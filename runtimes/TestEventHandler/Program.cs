﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestEventHandler
{
    class Program
    {
        static void Main(string[] args)
        {
            Grandmother grandmother = new Grandmother();

            Children children = new Children(grandmother);

            grandmother.IDide(new DateTime());
               
                //SampleEvent(this, new SampleEventArgs("Hello")

        }
    }

    class Grandmother
    {
        public Grandmother()
        {
            //Связала событие с процедурой погребения
            Death += new EventHandler(MyDeath);
        }

        private DateTime Age;

        public List<Children> Childrens { get; set; } 

        //Подписка на свое событие
        //Подготовила некую процедуру погребения в случае возникновения события
        private void MyDeath(object sender, EventArgs e)
        {
            //Юрист начинает процесс инициации завещания
            Console.WriteLine("Я все предусмотрела перед своей смертью.");
        }
        // Публикация события
        public event EventHandler Death;

        public void IDide(DateTime date)
        {
            if (DateTime.Now == date)
            {
                Death(this, new EventArgs());
            }
        }
    }

    class Children
    {
        private Grandmother _grandmother;

        public Children(Grandmother grandmother)
        {
            _grandmother = grandmother;

            _grandmother.Death += new EventHandler(DeathMyGrendmother);
        }
        //Подписка на  событие публикатора-бабушки
        //Подготовила некую процедуру погребения в случае возникновения события
        private void DeathMyGrendmother(object sender, EventArgs e)
        {
            //Подготовку к похоронам
            Console.WriteLine("Мы тоже готовы к  уходу бабушки.");
        }
    }
}

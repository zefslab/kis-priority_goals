﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using PriorityGoals.Api.Dpo;
using PriorityGoals.Api.Entity;

namespace PriorityGoal.WinForm.Mappings
{
    using PriorityGoals.Api.Dpo.Dream;
    using PriorityGoals.Api.Dpo.Finance;
    using PriorityGoals.Api.Dpo.Idea;
    using PriorityGoals.Api.Dpo.Interests;
    using PriorityGoals.Api.Dpo.Payment;
    using PriorityGoals.Api.Dpo.Principle;
    using PriorityGoals.Api.Dpo.Rest;
    using PriorityGoals.Api.Dpo.Result;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Service.Service;

    using Goal = PriorityGoals.Api.Entity.Goal;

    public class AutomapperWinFormMappings
    {
        private IServiceFactory _serviceFactory = ServiceFactory.InitializeServiceFactory();

        static public  void Initialize()
        {
            ///Определить все маппинги используемые на презентационном слое
            Mapper.Initialize(cfg => 
            {
                cfg.CreateMap<Issue, IssueDpo>()
                    .ForMember(d => d.Name, x => x.MapFrom(e => e.Name));
                cfg.CreateMap<IssueDetailDpo, Issue>()
                    .ForMember(i=>i.SubIssues, a=>a.Ignore())
                    .ForMember(x=>x.Ideas, i=>i.Ignore());
                cfg.CreateMap<Issue, IssueDetailDpo>();
                cfg.CreateMap<Plan, PlanDetailDpo>();
                cfg.CreateMap<PlanDetailDpo, Plan>()
                    .ForMember(x => x.Ideas, i => i.Ignore())
                    .ForMember(x=>x.Issues, i=>i.Ignore());
                cfg.CreateMap<Plan, PlanDpo>();
                //TODO доделать маппинги на все сущности ...
                cfg.CreateMap<Dream, DreamDpo>();
                cfg.CreateMap<DreamDpo, Dream>();
                cfg.CreateMap<Dream, DreamDetailDpo>();
                cfg.CreateMap<DreamDetailDpo, Dream>();
                cfg.CreateMap<Finance, FinanceDpo>();
                cfg.CreateMap<FinanceDpo, Finance>();
                cfg.CreateMap<Idea, IdeaDpo>();
                cfg.CreateMap<IdeaDpo, Idea>();

                cfg.CreateMap<Interest, InterestDetailDpo>();
                cfg.CreateMap<InterestDetailDpo, Interest>();
                cfg.CreateMap<Interest, InterestDpo>();
                cfg.CreateMap<InterestDpo, Interest>();


                cfg.CreateMap<Payment, PaymentDpo>();
                cfg.CreateMap<PaymentDpo, Payment>();
                cfg.CreateMap<Principle, PrincipleDpo>();
                cfg.CreateMap<PrincipleDpo, Principle>();
                cfg.CreateMap<Rest, RestDpo>();
                cfg.CreateMap<RestDpo, Rest>();
                cfg.CreateMap<Result, ResultDpo>();
                cfg.CreateMap<ResultDpo, Result>();
                cfg.CreateMap<Goal, GoalDetailDpo>();
                cfg.CreateMap<GoalDetailDpo, Goal>();
                cfg.CreateMap<Goal, GoalDpo>();
                cfg.CreateMap<GoalDpo, Goal>();
            });

        }
    }
}

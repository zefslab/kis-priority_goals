﻿using System;
using System.Windows.Forms;
using PriorityGoal.WinForm.Mappings;

namespace Goal.WinForm
{
    using System.Data.Entity.Migrations;
    using System.Diagnostics.Eventing.Reader;

    using Microsoft.Practices.Unity;

    using PriorityGoal.WinForm.Forms;

    using PriorityGoals.Api.Dao;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Api.Utility;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.Migrations;
    using PriorityGoals.Service;
    using PriorityGoals.Service.Service;

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Инициализируются маппинги Automapper
            AutomapperWinFormMappings.Initialize();
            //Регистрация типов в контейнере зависимостей
            IUnityContainer container = new UnityContainer();
            container.RegisterType(typeof(IIdeaCrudService), typeof(IdeaCrudService));
            container.RegisterType(typeof(IIssueCrudService), typeof(IssueCrudService));
            container.RegisterType(typeof(IBaseRepository<Plan>), typeof(PlanRepository));
            container.RegisterType(typeof(frmIssueList), typeof(frmIssueList));
            container.RegisterInstance(typeof(IUnityContainer), container);
            

            var migrator = new DbMigrator(new PriorityGoalMigrationConfiguration());
            migrator.Update();
            do
            {
                try
                {
                   // throw new Exception("Ошибка в рантайме");
                    Application.Run(container.BuildUp(new frmMainForm()));
                    break;
                }

                catch (Exception ex)
                {
                    new Log("Runtime").Error("Фатальная ошибка", ex);

                    throw;
                }
            }
            while (true);

        }
       
    }
}

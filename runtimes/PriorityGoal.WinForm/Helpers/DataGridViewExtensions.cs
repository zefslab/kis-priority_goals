﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoal.WinForm.Helpers
{
    using System.Windows.Forms;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Dao.DbRepositories;

    static class DataGridViewExtensions
    {
        public static Guid GetIdFromSelectedRow(this DataGridView grid, string idColumnName = "colId")
        {
            Guid id = Guid.Empty; 

            if (grid.SelectedRows.Count > 0)
            {
                id = Guid.Parse(grid.SelectedRows[0].Cells[idColumnName].Value.ToString());
            }
            else
            {
                MessageBox.Show("Не выбран элемент списка для удаления.", " ",
                                    MessageBoxButtons.OK, 
                                    MessageBoxIcon.Exclamation);
            }

            return id;
        }

        public static void DeleteSelectedItem(this DataGridView grid, ICrudService service, Action filterApply)
        {
            Guid id = Guid.Empty;
            try
            {
                id = grid.GetIdFromSelectedRow();
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException("В гриде колонка с Id  носит название отличное от ColId."
                                            + "Согласно соглашения у нее должно быть именно такое имя", ex);
            }

            if (id == Guid.Empty) return;

            if (MessageBox.Show("Действительно ли Вы желаете удалить запись?",
                "Удаление записи", MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                var result = service.Remove(id);

                if (result.Data)
                {
                    filterApply();
                }
                else
                {
                    if (result.Result == CrudResults.UnAccessibleDb)
                    {
                        MessageBox.Show("Проблемы с доступом к БД.",
                        "Удаление невозможно",
                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    if (result.Result == CrudResults.CannotDelete)
                        MessageBox.Show(result.Message,
                            "Удаление невозможно",
                            MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoal.WinForm.Helpers
{
    using System.Collections;
    using System.Windows.Forms;

    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Entity;

    static class ComboBoxExtensions 
    {
        /// <summary>
        /// Добавляет пустой элемент в комбобокс и устанавливает выбранное значение в списке
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cmb"></param>
        /// <param name="dataSource"></param>
        /// <param name="selectedValue"></param>
        /// <param name="needEmptyElement"></param>
        public static void SetDataSource<T>(this ComboBox cmb, IList<T> dataSource, Guid? selectedValue, bool needEmptyElement = true)
            where T : BaseDpo,  new ()
        {
            cmb.DataSource = null;

            if (needEmptyElement)
            {
                dataSource.Add(new T{ Id = Guid.Empty});
            }

            cmb.DataSource = dataSource.OrderBy(d => d.Id).ToList();

            cmb.SelectedValue = selectedValue ?? Guid.Empty;
        } 
    }
}

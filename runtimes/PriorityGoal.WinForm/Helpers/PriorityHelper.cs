﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoal.WinForm.Helpers
{
    using System.Windows.Forms;

    using PriorityGoals.Api.Constants;

    static class PriorityHelper
    {
        /// <summary>
        /// Возвращает приоритет, выбранный радиокнопками в  форме
        /// </summary>
        /// <param name="groupBox"></param>
        /// <returns></returns>
        public static Priorities GetPriorityFromRadioButton( this GroupBox groupBox)
        {
            return (Priorities) groupBox.Controls.OfType<RadioButton>()
                                    .OrderBy(r => r.Name)
                                    .Select(b => b.Checked)
                                    .ToList()
                                    .FindIndex(x => x);
        }
       
        /// <summary>
        /// Настраивается CheckListBox на содержимое четырех приоритетов.
        /// Рекомендуется использовать его в списочных формах, для настройки фильтра
        /// в обработчике загрузки формы 
        /// </summary>
        /// <param name="box"></param>
        public static void InitializePriorityCheckListBox(this CheckedListBox box)
        {
            box.DataSource = null;
            box.DisplayMember = "value";
            box.ValueMember = "key";
            box.DataSource = new List<KeyValuePair<int, string>>
                                     { new KeyValuePair<int, string>(0, "Важные и срочные(A)"),
                                       new KeyValuePair<int, string>(1, "Важные не срочные(B)"),
                                       new KeyValuePair<int, string>(2, "Не важные, но срочные(C)"),
                                       new KeyValuePair<int, string>(3, "Не важные и не срочные(D)")
                                      };
        }
        /// <summary>
        ///  Выделяет радиокнопку, соответсвующую переданному приоритету
        /// </summary>
        /// <param name="groupBox"></param>
        public static void SetPriotityRadioButton(this GroupBox groupBox, Priorities priority)
        {
            groupBox.Controls.OfType<RadioButton>()
                .OrderBy(r => r.Name)
                .ToArray()[(int)priority]
                .Checked = true;
        }
    }
}

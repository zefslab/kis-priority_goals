﻿using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Dpo.Interests;
    using PriorityGoals.Api.Dpo.Principle;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    public partial class frmInterest : Form
    {
        public InterestDetailDpo Interest { get; set; }

        private IList<PrincipleDpo> _principles;

        public frmInterest(ICrudService principleCrudService)
        {
            _principles = principleCrudService.Get<PrincipleDpo>(null).ToList();
            InitializeComponent();
        }

        private void frmInterest_Load(object sender, System.EventArgs e)
        {
            this.cmbPrinciples.SetDataSource(_principles, Interest.PrincipleId);

            this.txtName.Text = this.Interest.Name;
            this.txtDescryption.Text = this.Interest.Description;
            this.dtStartDate.Value = this.Interest.InterestDate;
            gbPriorities.SetPriotityRadioButton(this.Interest.Priority);
        }
          
        private void btnSave_Click(object sender, System.EventArgs e)
        {
            this.Interest.Name = this.txtName.Text;
            this.Interest.Description = this.txtDescryption.Text;
            this.Interest.InterestDate = this.dtStartDate.Value;
            if (this.cmbPrinciples.SelectedValue != null)
            {
                var principleId = Guid.Parse(this.cmbPrinciples.SelectedValue.ToString());
                if (principleId != Guid.Empty)
                {
                    this.Interest.PrincipleId = principleId;
                }
                else
                {
                    this.Interest.PrincipleId = null;
                }
            }
            
            this.Interest.Priority = gbPriorities.GetPriorityFromRadioButton();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        

       
    }
}

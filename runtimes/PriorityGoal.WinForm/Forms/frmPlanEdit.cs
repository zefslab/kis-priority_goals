﻿using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Dpo.Idea;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Extentions;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    public partial class frmPlanEdit : Form
    {
        public ServiceFactory ServiceFactory;

        public PlanDetailDpo Plan { get; set; }

        private IEnumerable<GoalDpo> _goals;

        private ICrudService _ideaService;
        IssueFilter filter = new IssueFilter();

        private ICrudService _issueService;


        public frmPlanEdit(IServiceFactory factory)
        {
            _ideaService = factory.GetService<Idea>();
            _issueService = factory.GetService<Issue>();
            _goals = factory.GetService<Goal>().Get<GoalDpo>(null).ToList();
            InitializeComponent();
        }
       
        private void frmPlan_Load(object sender, System.EventArgs e)
        {
            this.IdeaGrid.AutoGenerateColumns = false;
            this.cmbGoals.DataSource = null;
            this.cmbGoals.DataSource = this._goals;

            var statesList = StateListProvider.GetList();
            this.cmbPlanState.DataSource = null;
            this.cmbPlanState.DataSource = statesList;

            IList<KeyValuePair<int, string>> states = new List<KeyValuePair<int, string>>();
            var i = 1;
            foreach (States value in Enum.GetValues(typeof(States)))
            {
                states.Add(new KeyValuePair<int, string>(i, value.GetDisplayName()));
                i++;
            }
            states.Add(new KeyValuePair<int, string>(0, "---"));
            this.cmbIssuesState.DataSource = states.OrderBy(x => x.Key).ToList();
            chkListBoxPriority.InitializePriorityCheckListBox();
            this.txtName.Text = this.Plan.Name;
            this.txtDescryption.Text = this.Plan.Description;
            this.dpStartDate.Value = this.Plan.BeginDate;
            if (this.Plan.EndDate != null)
            {
                this.dpEndDate.Value = (DateTime)Plan.EndDate;
            }
            this.cmbPlanState.SelectedValue = this.Plan.State;
            this.cmbGoals.SelectedValue = this.Plan.GoalId;
            this.gbPriority.SetPriotityRadioButton(this.Plan.Priority);
            this.IdeaGridRefresh();
            this.IssueGridRefresh();

        }
        private void IdeaGridRefresh()
        {
            this.IdeaGrid.DataSource = null;
            this.IdeaGrid.DataSource = this.Plan.Ideas.Select(x => new
            {
                x.Id,
                x.Name
            }).ToList();

        }

        private void IssueGridRefresh()
        {
            this.filter = new IssueFilter();

            filter.PlanId = this.Plan.Id;
            var state = int.Parse(cmbIssuesState.SelectedValue.ToString());
            this.filter.State = state == 0 ? null : (States?)state;

           
            this.filter.Priorities = this.chkListBoxPriority.CheckedItems.OfType<KeyValuePair<int, string>>()
                    .Select(x => x.Key).ToList();
            

            /// Изменил ИЭФ 2016.08.34 Добавил условие проверки на точность
            this.Plan.Issues = this._issueService.Get<IssueDpo>(filter).ToList();

            gridIssues.DataSource = null;
            gridIssues.DataSource = this.Plan.Issues;
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            this.Plan.Name = this.txtName.Text;
            this.Plan.Description = this.txtDescryption.Text;
            this.Plan.BeginDate = this.dpStartDate.Value.Date;
            this.Plan.EndDate = this.dpEndDate.Value.Date;
            this.Plan.State = (States)this.cmbPlanState.SelectedValue;
            if (this.cmbGoals.SelectedValue != null)
            {
                this.Plan.GoalId = Guid.Parse(this.cmbGoals.SelectedValue.ToString());
            }
            else
            {
                MessageBox.Show(
                    "Нет привязки плана к цели. Вернитесь к форме создания плана и укажите цель",
                    "Отсутствует привязка",
                    MessageBoxButtons.OK);
                return;
            }
            this.Plan.Priority = this.gbPriority.GetPriorityFromRadioButton();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnEscape_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void btnIdeaAdd_Click(object sender, EventArgs e)
        {
            frmIdea frm = new frmIdea(ServiceFactory.InitializeServiceFactory());

            frm.Idea = new IdeaDpo { PlanId = this.Plan.Id };

            if (frm.ShowDialog() == DialogResult.OK)
            {
                //Сохранить данные в БД
                var idea = this._ideaService.Add(frm.Idea);
                //Обновить список в гриде
                if (idea != null)
                {
                    this.Plan.Ideas.Add(idea);

                    this.IdeaGridRefresh();
                }
            }
        }
        private void btnIdeaModifite_Click(object sender, EventArgs e)
        {
            if (this.IdeaGrid.SelectedRows.Count == 0)
            {
                MessageBox.Show(
                    "Не выбрано ни одной идеи",
                    "Ошибка выбора",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }
            var id = Guid.Parse(IdeaGrid.SelectedRows[0].Cells["colIdeaId"].Value.ToString());
            var idea = this.Plan.Ideas.FirstOrDefault(x => x.Id == id);
            var form = new frmIdea(ServiceFactory.InitializeServiceFactory()) { Idea = idea };

            if (form.ShowDialog() == DialogResult.OK)
            {
                //Сохранить данные в БД
                this._ideaService.Update(form.Idea);

                //Обновить список в гриде
                this.IdeaGridRefresh();
            }
        }
        private void btnIdeaDelete_Click(object sender, EventArgs e)
        {

            if (this.IdeaGrid.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите элемент списка для удаления.");
                return;
            }

            if (DialogResult.Yes == MessageBox.Show("Действительно ли Вы желаете удалить идею?", "Удаление идеи", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {

                var id = Guid.Parse(IdeaGrid.SelectedRows[0].Cells["colIdeaId"].Value.ToString());
                if (this._ideaService.Remove(id).Data)
                {
                    var idea = this.Plan.Ideas.FirstOrDefault(x => x.Id == id);
                    this.Plan.Ideas.Remove(idea);
                    this.IdeaGridRefresh();
                }

            }
        }

        private void cmbIssuesState_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.IssueGridRefresh();
        }

        private void chkListBoxPriority_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.IssueGridRefresh();
        }
        private void IdeaGrid_SelectionChanged(object sender, EventArgs e)
        {
            if (this.IdeaGrid.SelectedRows.Count == 0)
            {
                return;
            }
            Guid ideaId = IdeaGrid.GetIdFromSelectedRow("colIdeaId");
            var idea = this.Plan.Ideas.FirstOrDefault(x => x.Id == ideaId);

            this.txtIdeaName.Text = idea.Name;
            this.txtIdeaDescription.Text = idea.Description;

        }


    }
}

﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmTaskList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbTaskStates = new System.Windows.Forms.ComboBox();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbPlans = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbGoals = new System.Windows.Forms.ComboBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.gridTasks = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFinishedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSubtask = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIdeaName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTaskStates = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btnCreateTask = new System.Windows.Forms.Button();
            this.btnDeleteTask = new System.Windows.Forms.Button();
            this.tabTaskInfo = new System.Windows.Forms.TabControl();
            this.tabMainInfo = new System.Windows.Forms.TabPage();
            this.txtDescryption = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnModifiteTask = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTasks)).BeginInit();
            this.tabTaskInfo.SuspendLayout();
            this.tabMainInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.dateTimePicker2);
            this.splitContainer1.Panel1.Controls.Add(this.dateTimePicker1);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.cmbTaskStates);
            this.splitContainer1.Panel1.Controls.Add(this.checkedListBox1);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.cmbPlans);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.cmbGoals);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(978, 599);
            this.splitContainer1.SplitterDistance = 232;
            this.splitContainer1.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(15, 331);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Конец периода";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(15, 274);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Начало периода";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePicker2.Location = new System.Drawing.Point(15, 354);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 26);
            this.dateTimePicker2.TabIndex = 8;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateTimePicker1.Location = new System.Drawing.Point(15, 302);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 26);
            this.dateTimePicker1.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 221);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(211, 26);
            this.label3.TabIndex = 6;
            this.label3.Text = "Состояние задания";
            // 
            // cmbTaskStates
            // 
            this.cmbTaskStates.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTaskStates.FormattingEnabled = true;
            this.cmbTaskStates.Location = new System.Drawing.Point(12, 250);
            this.cmbTaskStates.Name = "cmbTaskStates";
            this.cmbTaskStates.Size = new System.Drawing.Size(204, 21);
            this.cmbTaskStates.TabIndex = 5;
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.checkedListBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "Важные и срочные (A)  ",
            "Важные не срочные (B)",
            "Не важные, но срочные (C)",
            "Не важные и не срочные (D)"});
            this.checkedListBox1.Location = new System.Drawing.Point(12, 142);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(213, 76);
            this.checkedListBox1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "Планы";
            // 
            // cmbPlans
            // 
            this.cmbPlans.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPlans.FormattingEnabled = true;
            this.cmbPlans.Location = new System.Drawing.Point(12, 98);
            this.cmbPlans.Name = "cmbPlans";
            this.cmbPlans.Size = new System.Drawing.Size(204, 21);
            this.cmbPlans.TabIndex = 2;
            this.cmbPlans.SelectedIndexChanged += new System.EventHandler(this.cmbPlans_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Цели";
            // 
            // cmbGoals
            // 
            this.cmbGoals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbGoals.DisplayMember = "Name";
            this.cmbGoals.FormattingEnabled = true;
            this.cmbGoals.Location = new System.Drawing.Point(12, 48);
            this.cmbGoals.Name = "cmbGoals";
            this.cmbGoals.Size = new System.Drawing.Size(204, 21);
            this.cmbGoals.TabIndex = 0;
            this.cmbGoals.ValueMember = "Id";
            this.cmbGoals.SelectedIndexChanged += new System.EventHandler(this.cmbGoals_SelectedIndexChanged);
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.gridTasks);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.btnCreateTask);
            this.splitContainer2.Panel2.Controls.Add(this.btnDeleteTask);
            this.splitContainer2.Panel2.Controls.Add(this.tabTaskInfo);
            this.splitContainer2.Panel2.Controls.Add(this.btnModifiteTask);
            this.splitContainer2.Size = new System.Drawing.Size(742, 599);
            this.splitContainer2.SplitterDistance = 311;
            this.splitContainer2.TabIndex = 0;
            // 
            // gridTasks
            // 
            this.gridTasks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTasks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colName,
            this.colType,
            this.colStartDate,
            this.colFinishedDate,
            this.colSubtask,
            this.colIdeaName,
            this.colTaskStates});
            this.gridTasks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTasks.Location = new System.Drawing.Point(0, 0);
            this.gridTasks.MultiSelect = false;
            this.gridTasks.Name = "gridTasks";
            this.gridTasks.RowHeadersVisible = false;
            this.gridTasks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridTasks.Size = new System.Drawing.Size(738, 307);
            this.gridTasks.TabIndex = 0;
            this.gridTasks.SelectionChanged += new System.EventHandler(this.gridTasks_SelectionChanged);
            // 
            // colId
            // 
            this.colId.DataPropertyName = "Id";
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.Visible = false;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Name";
            this.colName.HeaderText = "Название";
            this.colName.Name = "colName";
            // 
            // colType
            // 
            this.colType.HeaderText = "Тип задания";
            this.colType.Name = "colType";
            // 
            // colStartDate
            // 
            this.colStartDate.DataPropertyName = "StartDate";
            this.colStartDate.HeaderText = "Дата начала";
            this.colStartDate.Name = "colStartDate";
            // 
            // colFinishedDate
            // 
            this.colFinishedDate.HeaderText = "Дата окончания";
            this.colFinishedDate.Name = "colFinishedDate";
            // 
            // colSubtask
            // 
            this.colSubtask.HeaderText = "Вложенные задания";
            this.colSubtask.Name = "colSubtask";
            // 
            // colIdeaName
            // 
            this.colIdeaName.HeaderText = "Примечания";
            this.colIdeaName.Name = "colIdeaName";
            // 
            // colTaskStates
            // 
            this.colTaskStates.HeaderText = "Состояние задания";
            this.colTaskStates.Name = "colTaskStates";
            this.colTaskStates.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colTaskStates.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // btnCreateTask
            // 
            this.btnCreateTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCreateTask.Location = new System.Drawing.Point(3, 3);
            this.btnCreateTask.Name = "btnCreateTask";
            this.btnCreateTask.Size = new System.Drawing.Size(106, 29);
            this.btnCreateTask.TabIndex = 21;
            this.btnCreateTask.Text = "Создать";
            this.btnCreateTask.UseVisualStyleBackColor = true;
            this.btnCreateTask.Click += new System.EventHandler(this.btnCreateTask_Click);
            // 
            // btnDeleteTask
            // 
            this.btnDeleteTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDeleteTask.Location = new System.Drawing.Point(233, 4);
            this.btnDeleteTask.Name = "btnDeleteTask";
            this.btnDeleteTask.Size = new System.Drawing.Size(99, 28);
            this.btnDeleteTask.TabIndex = 8;
            this.btnDeleteTask.Text = "Удалить";
            this.btnDeleteTask.UseVisualStyleBackColor = true;
            this.btnDeleteTask.Click += new System.EventHandler(this.btnDeleteTask_Click);
            // 
            // tabTaskInfo
            // 
            this.tabTaskInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabTaskInfo.Controls.Add(this.tabMainInfo);
            this.tabTaskInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabTaskInfo.Location = new System.Drawing.Point(-2, 37);
            this.tabTaskInfo.Name = "tabTaskInfo";
            this.tabTaskInfo.SelectedIndex = 0;
            this.tabTaskInfo.Size = new System.Drawing.Size(750, 245);
            this.tabTaskInfo.TabIndex = 16;
            // 
            // tabMainInfo
            // 
            this.tabMainInfo.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabMainInfo.Controls.Add(this.txtDescryption);
            this.tabMainInfo.Controls.Add(this.label7);
            this.tabMainInfo.Location = new System.Drawing.Point(4, 34);
            this.tabMainInfo.Name = "tabMainInfo";
            this.tabMainInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainInfo.Size = new System.Drawing.Size(742, 207);
            this.tabMainInfo.TabIndex = 0;
            this.tabMainInfo.Text = "Основные сведения";
            // 
            // txtDescryption
            // 
            this.txtDescryption.Location = new System.Drawing.Point(4, 34);
            this.txtDescryption.Multiline = true;
            this.txtDescryption.Name = "txtDescryption";
            this.txtDescryption.Size = new System.Drawing.Size(722, 153);
            this.txtDescryption.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(5, 5);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(112, 26);
            this.label7.TabIndex = 2;
            this.label7.Text = "Описание";
            // 
            // btnModifiteTask
            // 
            this.btnModifiteTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnModifiteTask.Location = new System.Drawing.Point(115, 3);
            this.btnModifiteTask.Name = "btnModifiteTask";
            this.btnModifiteTask.Size = new System.Drawing.Size(112, 28);
            this.btnModifiteTask.TabIndex = 20;
            this.btnModifiteTask.Text = "Изменить";
            this.btnModifiteTask.UseVisualStyleBackColor = true;
            this.btnModifiteTask.Click += new System.EventHandler(this.btnModifiteTask_Click);
            // 
            // frmTaskList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(978, 599);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(994, 637);
            this.Name = "frmTaskList";
            this.Text = "Задачи для выполнения";
            this.Load += new System.EventHandler(this.frmTask_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTasks)).EndInit();
            this.tabTaskInfo.ResumeLayout(false);
            this.tabMainInfo.ResumeLayout(false);
            this.tabMainInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView gridTasks;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbTaskStates;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbPlans;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbGoals;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabControl tabTaskInfo;
        private System.Windows.Forms.TabPage tabMainInfo;
        private System.Windows.Forms.Button btnModifiteTask;
        private System.Windows.Forms.Button btnDeleteTask;
        private System.Windows.Forms.Button btnCreateTask;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFinishedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSubtask;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIdeaName;
        private System.Windows.Forms.DataGridViewComboBoxColumn colTaskStates;
        private System.Windows.Forms.TextBox txtDescryption;
    }
}
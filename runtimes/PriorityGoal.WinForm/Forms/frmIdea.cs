﻿using System;
using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using PriorityGoals.Api.Dpo.Idea;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;

    public partial class frmIdea : Form
    {
        public IdeaDpo Idea { get; set; }

        private ICrudService _service;

        public frmIdea(IServiceFactory factory)
        {

            InitializeComponent();
            _service = factory.GetService<Idea>();
        }

        private void frmIdea_Load(object sender, EventArgs e)
        {
            this.txtName.Text = this.Idea.Name;
            this.txtDescription.Text = this.Idea.Description;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Idea.Name = this.txtName.Text;
            this.Idea.Description = this.txtDescription.Text;

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            var response = MessageBox.Show(
                "Вы действительно желаете закрыть форму и отменить внесенные изменения?",
                "Отменить изменения",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (response == DialogResult.Yes )
            {
                this.Close();
            }


        }
    }
}

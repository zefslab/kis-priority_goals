﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmIssueCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.cmbState = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.cmbPlans = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.gbPriorities = new System.Windows.Forms.GroupBox();
            this.rdBtnTypeA = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeD = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeB = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeC = new System.Windows.Forms.RadioButton();
            this.txtIssueName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dpEndDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.lbCreateDate = new System.Windows.Forms.Label();
            this.dpStartDate = new System.Windows.Forms.DateTimePicker();
            this.txtDescryption = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbPriorities.SuspendLayout();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(252, 101);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(53, 17);
            this.label8.TabIndex = 61;
            this.label8.Text = "Статус";
            // 
            // cmbState
            // 
            this.cmbState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbState.DisplayMember = "Value";
            this.cmbState.FormattingEnabled = true;
            this.cmbState.Location = new System.Drawing.Point(256, 120);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(191, 21);
            this.cmbState.TabIndex = 60;
            this.cmbState.ValueMember = "Key";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCancel.Location = new System.Drawing.Point(258, 424);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(191, 31);
            this.btnCancel.TabIndex = 59;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCreate.Location = new System.Drawing.Point(61, 424);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(191, 31);
            this.btnCreate.TabIndex = 58;
            this.btnCreate.Text = "Создать";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // cmbPlans
            // 
            this.cmbPlans.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbPlans.DisplayMember = "Name";
            this.cmbPlans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbPlans.FormattingEnabled = true;
            this.cmbPlans.Location = new System.Drawing.Point(25, 74);
            this.cmbPlans.Name = "cmbPlans";
            this.cmbPlans.Size = new System.Drawing.Size(422, 21);
            this.cmbPlans.TabIndex = 57;
            this.cmbPlans.ValueMember = "Id";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(22, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(174, 17);
            this.label7.TabIndex = 56;
            this.label7.Text = "Принадлежность к плану\r\n";
            // 
            // gbPriorities
            // 
            this.gbPriorities.Controls.Add(this.rdBtnTypeA);
            this.gbPriorities.Controls.Add(this.rdBtnTypeD);
            this.gbPriorities.Controls.Add(this.rdBtnTypeB);
            this.gbPriorities.Controls.Add(this.rdBtnTypeC);
            this.gbPriorities.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbPriorities.Location = new System.Drawing.Point(19, 101);
            this.gbPriorities.Name = "gbPriorities";
            this.gbPriorities.Size = new System.Drawing.Size(227, 115);
            this.gbPriorities.TabIndex = 55;
            this.gbPriorities.TabStop = false;
            this.gbPriorities.Text = "Приоритет";
            // 
            // rdBtnTypeA
            // 
            this.rdBtnTypeA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeA.AutoSize = true;
            this.rdBtnTypeA.Location = new System.Drawing.Point(6, 19);
            this.rdBtnTypeA.Name = "rdBtnTypeA";
            this.rdBtnTypeA.Size = new System.Drawing.Size(182, 21);
            this.rdBtnTypeA.TabIndex = 9;
            this.rdBtnTypeA.TabStop = true;
            this.rdBtnTypeA.Text = "(A)  Важные и срочные ";
            this.rdBtnTypeA.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeD
            // 
            this.rdBtnTypeD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeD.AutoSize = true;
            this.rdBtnTypeD.Location = new System.Drawing.Point(6, 91);
            this.rdBtnTypeD.Name = "rdBtnTypeD";
            this.rdBtnTypeD.Size = new System.Drawing.Size(219, 21);
            this.rdBtnTypeD.TabIndex = 12;
            this.rdBtnTypeD.TabStop = true;
            this.rdBtnTypeD.Text = "(D) Не важные и не срочные ";
            this.rdBtnTypeD.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeB
            // 
            this.rdBtnTypeB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeB.AutoSize = true;
            this.rdBtnTypeB.Location = new System.Drawing.Point(6, 42);
            this.rdBtnTypeB.Name = "rdBtnTypeB";
            this.rdBtnTypeB.Size = new System.Drawing.Size(186, 21);
            this.rdBtnTypeB.TabIndex = 10;
            this.rdBtnTypeB.TabStop = true;
            this.rdBtnTypeB.Text = "(B) Важные не срочные ";
            this.rdBtnTypeB.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeC
            // 
            this.rdBtnTypeC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeC.AutoSize = true;
            this.rdBtnTypeC.Location = new System.Drawing.Point(6, 69);
            this.rdBtnTypeC.Name = "rdBtnTypeC";
            this.rdBtnTypeC.Size = new System.Drawing.Size(206, 21);
            this.rdBtnTypeC.TabIndex = 11;
            this.rdBtnTypeC.TabStop = true;
            this.rdBtnTypeC.Text = "(C) Не важные, но срочные";
            this.rdBtnTypeC.UseVisualStyleBackColor = true;
            // 
            // txtIssueName
            // 
            this.txtIssueName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtIssueName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtIssueName.Location = new System.Drawing.Point(25, 29);
            this.txtIssueName.Name = "txtIssueName";
            this.txtIssueName.Size = new System.Drawing.Size(422, 20);
            this.txtIssueName.TabIndex = 54;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(106, 17);
            this.label1.TabIndex = 53;
            this.label1.Text = "Наименование";
            // 
            // dpEndDate
            // 
            this.dpEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dpEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dpEndDate.Location = new System.Drawing.Point(255, 378);
            this.dpEndDate.MinDate = new System.DateTime(1800, 4, 25, 0, 0, 0, 0);
            this.dpEndDate.Name = "dpEndDate";
            this.dpEndDate.Size = new System.Drawing.Size(200, 23);
            this.dpEndDate.TabIndex = 52;
            this.dpEndDate.Value = new System.DateTime(2016, 4, 27, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(252, 358);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 17);
            this.label10.TabIndex = 51;
            this.label10.Text = "Дата окончания";
            // 
            // lbCreateDate
            // 
            this.lbCreateDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbCreateDate.AutoSize = true;
            this.lbCreateDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCreateDate.Location = new System.Drawing.Point(24, 358);
            this.lbCreateDate.Name = "lbCreateDate";
            this.lbCreateDate.Size = new System.Drawing.Size(94, 17);
            this.lbCreateDate.TabIndex = 50;
            this.lbCreateDate.Text = "Дата начала";
            // 
            // dpStartDate
            // 
            this.dpStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dpStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dpStartDate.Location = new System.Drawing.Point(24, 378);
            this.dpStartDate.MinDate = new System.DateTime(1800, 4, 25, 0, 0, 0, 0);
            this.dpStartDate.Name = "dpStartDate";
            this.dpStartDate.Size = new System.Drawing.Size(200, 23);
            this.dpStartDate.TabIndex = 49;
            // 
            // txtDescryption
            // 
            this.txtDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtDescryption.Location = new System.Drawing.Point(22, 239);
            this.txtDescryption.Multiline = true;
            this.txtDescryption.Name = "txtDescryption";
            this.txtDescryption.Size = new System.Drawing.Size(433, 116);
            this.txtDescryption.TabIndex = 48;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(19, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 17);
            this.label4.TabIndex = 47;
            this.label4.Text = "Описание ";
            // 
            // frmIssueCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(476, 474);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cmbState);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.cmbPlans);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.gbPriorities);
            this.Controls.Add(this.txtIssueName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dpEndDate);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbCreateDate);
            this.Controls.Add(this.dpStartDate);
            this.Controls.Add(this.txtDescryption);
            this.Controls.Add(this.label4);
            this.MinimumSize = new System.Drawing.Size(492, 513);
            this.Name = "frmIssueCreate";
            this.Text = "Создание задачи";
            this.Load += new System.EventHandler(this.frmIssue_Load);
            this.gbPriorities.ResumeLayout(false);
            this.gbPriorities.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbState;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.ComboBox cmbPlans;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbPriorities;
        private System.Windows.Forms.RadioButton rdBtnTypeA;
        private System.Windows.Forms.RadioButton rdBtnTypeD;
        private System.Windows.Forms.RadioButton rdBtnTypeB;
        private System.Windows.Forms.RadioButton rdBtnTypeC;
        private System.Windows.Forms.TextBox txtIssueName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dpEndDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbCreateDate;
        private System.Windows.Forms.DateTimePicker dpStartDate;
        private System.Windows.Forms.TextBox txtDescryption;
        private System.Windows.Forms.Label label4;
    }
}
﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.планыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.целиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.мечтыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.интересыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.принципыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.документацияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проверитьОбьновленияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(74, 20);
            this.toolStripMenuItem1.Text = "Задачи";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.btnTasks_Click);
            // 
            // планыToolStripMenuItem
            // 
            this.планыToolStripMenuItem.Name = "планыToolStripMenuItem";
            this.планыToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.планыToolStripMenuItem.Text = "Планы";
            this.планыToolStripMenuItem.Click += new System.EventHandler(this.btnPlans_Click);
            // 
            // целиToolStripMenuItem
            // 
            this.целиToolStripMenuItem.Name = "целиToolStripMenuItem";
            this.целиToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.целиToolStripMenuItem.Text = "Цели";
            this.целиToolStripMenuItem.Click += new System.EventHandler(this.btnGoals_Click);
            // 
            // мечтыToolStripMenuItem
            // 
            this.мечтыToolStripMenuItem.Name = "мечтыToolStripMenuItem";
            this.мечтыToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.мечтыToolStripMenuItem.Text = "Мечты";
            this.мечтыToolStripMenuItem.Click += new System.EventHandler(this.btnDreams_Click);
            // 
            // интересыToolStripMenuItem
            // 
            this.интересыToolStripMenuItem.Name = "интересыToolStripMenuItem";
            this.интересыToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.интересыToolStripMenuItem.Text = "Интересы";
            this.интересыToolStripMenuItem.Click += new System.EventHandler(this.btnInterests_Click);
            // 
            // принципыToolStripMenuItem
            // 
            this.принципыToolStripMenuItem.Name = "принципыToolStripMenuItem";
            this.принципыToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.принципыToolStripMenuItem.Text = "Принципы";
            this.принципыToolStripMenuItem.Click += new System.EventHandler(this.btnPrinciples_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.документацияToolStripMenuItem,
            this.проверитьОбьновленияToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // документацияToolStripMenuItem
            // 
            this.документацияToolStripMenuItem.Enabled = false;
            this.документацияToolStripMenuItem.Name = "документацияToolStripMenuItem";
            this.документацияToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.документацияToolStripMenuItem.Text = "Документация";
            // 
            // проверитьОбьновленияToolStripMenuItem
            // 
            this.проверитьОбьновленияToolStripMenuItem.Enabled = false;
            this.проверитьОбьновленияToolStripMenuItem.Name = "проверитьОбьновленияToolStripMenuItem";
            this.проверитьОбьновленияToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.проверитьОбьновленияToolStripMenuItem.Text = "Проверить обьновления";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.планыToolStripMenuItem,
            this.целиToolStripMenuItem,
            this.мечтыToolStripMenuItem,
            this.интересыToolStripMenuItem,
            this.принципыToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(463, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // frmMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BackgroundImage = global::PriorityGoal.WinForm.Resource.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(463, 579);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(479, 618);
            this.Name = "frmMainForm";
            this.Text = "Достижение целей";
            this.Load += new System.EventHandler(this.frmMainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem планыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem целиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem мечтыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem интересыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem принципыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem документацияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem проверитьОбьновленияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
    }
}
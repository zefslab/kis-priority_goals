﻿using System.Windows.Forms;
using PriorityGoals.Service.Service;

namespace PriorityGoal.WinForm.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Dpo.Dream;
    using PriorityGoals.Api.Dpo.Interests;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;

    public partial class frmGoal : Form
    {
        public ServiceFactory serviceFactory;

        public GoalDetailDpo Goal { get; set; }

        private IList<DreamDpo> _dreams;
       

        public frmGoal(IServiceFactory factory)
        {
            this._dreams = factory.GetService<Dream>().Get<DreamDpo>(null).ToList();

            InitializeComponent();
        }

        private void frmGoal_Load(object sender, EventArgs e)
        {
            this.cmbDreams.SetDataSource(this._dreams, this.Goal.DreamId);
           
            this.txtName.Text = this.Goal.Name;
            this.txtDescryption.Text = this.Goal.Description;
            this.dpStartDate.Value = this.Goal.GoalDate;
            gbGoals.SetPriotityRadioButton(this.Goal.Priority);
        }

        
        

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Goal.Name = this.txtName.Text;
            this.Goal.Description = this.txtDescryption.Text;
            this.Goal.GoalDate = this.dpStartDate.Value;
            if (this.cmbDreams.SelectedValue != null)
            {
                var dreamId = Guid.Parse(this.cmbDreams.SelectedValue.ToString());

                this.Goal.DreamId = dreamId == Guid.Empty?null : (Guid?)dreamId;
            }
            this.Goal.Priority = gbGoals.GetPriorityFromRadioButton();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}

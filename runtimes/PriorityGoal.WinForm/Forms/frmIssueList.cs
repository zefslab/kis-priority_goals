﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;


namespace PriorityGoal.WinForm.Forms
{
    using System.Diagnostics;
    using System.Security.Cryptography.X509Certificates;

    using Microsoft.Practices.Unity;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Extentions;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    public partial class frmIssueList : Form
    {
        [Dependency]
        public IIssueCrudService IssueCrudService { get; set; }
        
        ICrudService _goalService;

        ICrudService _planService;

        IssueFilter filter = new IssueFilter();

        private IList<IssueDpo> Issues; 

        /// <summary>
        /// признак того, что форма загружается в первый раз
        /// поэтому открывается пустое окно (не применяются фильтры).
        /// </summary>
        private bool IsFormNotLoaded = true;

        public frmIssueList()
        {
            IServiceFactory serviceFactory = ServiceFactory.InitializeServiceFactory();

            if (serviceFactory == null)
            {
                throw new ArgumentNullException(nameof(serviceFactory));
            }
            
            InitializeComponent();

            _goalService = serviceFactory.GetService<Goal>();

            _planService = serviceFactory.GetService<Plan>();

            this.Issues = new List<IssueDpo>();
        }

        private void frmIssue_Load(object sender, EventArgs e)
        {
            grid.AutoGenerateColumns = false;
            
            #region Настройка элементов фильтра
            var goals = this._goalService.Get<GoalDpo>(new GoalFilter()).ToList();
            goals.Add(new GoalDpo {Name = "", Id = Guid.Empty});
            
            cmbPlans.DisplayMember = "Name";
            cmbPlans.ValueMember = "Id";
            PlansFiltrate(new PlanFilter());

            IList<KeyValuePair<int, string>> states = new List<KeyValuePair<int, string>>();
            var i = 1;
            foreach (States value in Enum.GetValues(typeof(States)))
            {
                states.Add(new KeyValuePair<int, string>(i, value.GetDisplayName()));
                i++;
            }
            states.Add(new KeyValuePair<int, string>( 0, "---"));
            this.cmbStates.DataSource = states.OrderBy(x=>x.Key).ToList();
            this.cmbStates.SelectedIndexChanged += this.cmbStates_SelectedIndexChanged;

            chkListBoxPriority.InitializePriorityCheckListBox();
            this.chkListBoxPriority.SelectedIndexChanged += this.chkListBoxPriority_SelectedIndexChanged;

            #endregion

            IsFormNotLoaded = false;

            FilterApply();
        }

        private void PlansFiltrate(PlanFilter filter)
        {
            var plans = this._planService.Get<PlanDpo>(filter).ToList();
            plans.Add(new PlanDetailDpo { Name = "", Id = Guid.Empty });
            cmbPlans.DataSource = plans.OrderBy(p => p.Id).ToList();
        }
        /// <summary>
        /// Меняются настройка фильтра со списком планов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbPlans_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsFormNotLoaded) return;

            FilterApply();
        }

        /// <summary>
        /// Применяются настройки фильтров при перезагрузки списка задач в гриде
        /// Создал ЗЕФ 2016.04.12
        /// </summary>
        private void FilterApply()
        {
            this.filter = new IssueFilter();

            filter.PlanId = cmbPlans.SelectedValue as Guid?;
            var state = int.Parse(this.cmbStates.SelectedValue.ToString());
            this.filter.State = state == 0? null : (States?)state;

            this.filter.DataBegin = this.chbEnableBeginDate.Checked ? (DateTime?)this.dtBegin.Value.Date : null;
            this.filter.DataEnd = this.chbEnableEndDate.Checked ? (DateTime?)dtEnd.Value.Date : null;
            if (this.chbEnablePriority.Checked)
            {
                this.filter.Priorities = this.chkListBoxPriority.CheckedItems.OfType<KeyValuePair<int, string>>()
                    .Select(x => x.Key).ToList();
            }

            /// Изменил ИЭФ 2016.08.34 Добавил условие проверки на точность
            this.Issues = this.IssueCrudService.Get<IssueDpo>(filter).ToList();

            grid.DataSource = null;
            grid.DataSource = this.Issues;
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridissue_SelectionChanged(object sender, EventArgs e)
        {
            if (this.grid.SelectedRows.Count > 0)
            {
                var id = (Guid)grid.SelectedRows[0].Cells["colId"].Value;

                txtDescryption.Text = this.Issues.FirstOrDefault(x => x.Id == id).Description;
            }
        }

        private IssueDetailDpo getSelectedIssue()
        {
            if (grid.SelectedRows.Count == 0)
            {
                return null;
            }

            var id = (Guid)grid.SelectedRows[0].Cells["colId"].Value;

            var issue = this.IssueCrudService.Get<IssueDetailDpo>(id);

            return issue;
        }

        private void btnCreateIssue_Click(object sender, EventArgs e)
        {
            var form = new frmIssueCreate(ServiceFactory.InitializeServiceFactory());
            //В свойство формы передаем новый объект
            Guid? planId = Guid.Parse(this.cmbPlans.SelectedValue.ToString());
            form.Issue = new IssueDetailDpo
            {
                StartDate = DateTime.Now,
                PlanId = planId!=Guid.Empty? planId : null
            };

            if (form.ShowDialog() == DialogResult.OK )
            {
                //Сохранить данные в БД
                var issue = this.IssueCrudService.Add(form.Issue);
                //Обновить список в гриде
                if (issue != null && issue.Id != Guid.Empty)
                {
                    this.FilterApply();
                }
            }
            else
            {
#if DEBUG
                //Ничего не делаем
                Debug.Print("Пользователь отказался от сохранения");
#endif
            }

        }

        private void SaveAndRefreshGrid(IssueDpo task)
        {
           
        }

        private void btnModifiteIssue_Click(object sender, EventArgs e)
        {
            var id = this.grid.GetIdFromSelectedRow();
            if (id == Guid.Empty)
            {
                return;
            }

            var form = new frmIssueEdit(ServiceFactory.InitializeServiceFactory());

            form.Issue = this.IssueCrudService.Get<IssueDetailDpo>(id);

            if (form.ShowDialog() == DialogResult.OK)
            {
                //Сохранить данные в БД
                this.IssueCrudService.Update(form.Issue);
                
                //Обновить список в гриде
                FilterApply();
            }
        }
        private void btnModifiteIssue_Click(object sender, DataGridViewCellEventArgs e)
        {
            btnModifiteIssue_Click(sender, (EventArgs)e);
        }

        private void btnDeleteTask_Click(object sender, EventArgs e)
        {
            if (this.grid.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите элемент списка для удаления.");
                return;
            }

            if (DialogResult.Yes== MessageBox.Show(
                    "Действительно ли Вы желаете удалить задачу?",
                    "Удаление задачи",MessageBoxButtons.YesNo,MessageBoxIcon.Warning))
            {
                var issueId = this.grid.GetIdFromSelectedRow();
                var issue = this.IssueCrudService.Get<IssueDetailDpo>(issueId);
                if (issue.Ideas.Any() || issue.SubIssues.Any())
                {
                    if (DialogResult.No == MessageBox.Show(
                            "В удаляемой задаче есть связанные подзадачи или идеи. Продолжить удаление?",
                            "Удаление задачи",MessageBoxButtons.YesNo,MessageBoxIcon.Warning))
                    {
                        return;
                    }
                }

                this.IssueCrudService.Remove(issueId);
                this.FilterApply();
            }
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void chbEnablePriority_CheckedChanged(object sender, EventArgs e)
        {
            this.chkListBoxPriority.Enabled = this.chbEnablePriority.Checked;
            FilterApply();
        }

        private void chbEnableBeginDate_CheckedChanged(object sender, EventArgs e)
        {
            this.dtBegin.Enabled = this.chbEnableBeginDate.Checked;
            FilterApply();
        }

        private void dtEnd_ValueChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void chkListBoxPriority_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void dtBegin_ValueChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void chbEnableEndDate_CheckedChanged(object sender, EventArgs e)
        {
            this.dtEnd.Enabled = this.chbEnableEndDate.Checked;
            FilterApply();
        }

       
    }
}

﻿namespace PriorityGoal.WinForm.Forms
{
   public partial class frmIssueEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Идеи = new System.Windows.Forms.Label();
            this.txtIdeaDescription = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIdeaName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnIdeaDelete = new System.Windows.Forms.Button();
            this.IdeaGrid = new System.Windows.Forms.DataGridView();
            this.colIdeaId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIdeaName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnIdeaAdd = new System.Windows.Forms.Button();
            this.btnIdeaModifite = new System.Windows.Forms.Button();
            this.tabCreateTasks = new System.Windows.Forms.TabPage();
            this.btnEdit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbState = new System.Windows.Forms.ComboBox();
            this.cmbPlans = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.grid = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStates = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbPriorities = new System.Windows.Forms.GroupBox();
            this.rdBtnTypeA = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeD = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeB = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeC = new System.Windows.Forms.RadioButton();
            this.txtIssueName = new System.Windows.Forms.TextBox();
            this.txtDescryption = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtEndDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.lbCreateDate = new System.Windows.Forms.Label();
            this.dpStartDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.tabTask = new System.Windows.Forms.TabControl();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdeaGrid)).BeginInit();
            this.tabCreateTasks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.gbPriorities.SuspendLayout();
            this.tabTask.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage2.Controls.Add(this.Идеи);
            this.tabPage2.Controls.Add(this.txtIdeaDescription);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.txtIdeaName);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.btnIdeaDelete);
            this.tabPage2.Controls.Add(this.IdeaGrid);
            this.tabPage2.Controls.Add(this.btnIdeaAdd);
            this.tabPage2.Controls.Add(this.btnIdeaModifite);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1063, 459);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Идеи";
            // 
            // Идеи
            // 
            this.Идеи.AutoSize = true;
            this.Идеи.Location = new System.Drawing.Point(459, 4);
            this.Идеи.Name = "Идеи";
            this.Идеи.Size = new System.Drawing.Size(49, 20);
            this.Идеи.TabIndex = 58;
            this.Идеи.Text = "Идеи";
            // 
            // txtIdeaDescription
            // 
            this.txtIdeaDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtIdeaDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtIdeaDescription.Location = new System.Drawing.Point(9, 69);
            this.txtIdeaDescription.Multiline = true;
            this.txtIdeaDescription.Name = "txtIdeaDescription";
            this.txtIdeaDescription.ReadOnly = true;
            this.txtIdeaDescription.Size = new System.Drawing.Size(433, 384);
            this.txtIdeaDescription.TabIndex = 50;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(6, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 17);
            this.label5.TabIndex = 49;
            this.label5.Text = "Описание";
            // 
            // txtIdeaName
            // 
            this.txtIdeaName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtIdeaName.Location = new System.Drawing.Point(9, 23);
            this.txtIdeaName.Name = "txtIdeaName";
            this.txtIdeaName.ReadOnly = true;
            this.txtIdeaName.Size = new System.Drawing.Size(433, 23);
            this.txtIdeaName.TabIndex = 48;
            this.txtIdeaName.TextChanged += new System.EventHandler(this.txtIdeaName_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(6, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 17);
            this.label6.TabIndex = 47;
            this.label6.Text = "Название";
            // 
            // btnIdeaDelete
            // 
            this.btnIdeaDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIdeaDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIdeaDelete.Location = new System.Drawing.Point(955, 101);
            this.btnIdeaDelete.Name = "btnIdeaDelete";
            this.btnIdeaDelete.Size = new System.Drawing.Size(102, 28);
            this.btnIdeaDelete.TabIndex = 54;
            this.btnIdeaDelete.Text = "Удалить";
            this.btnIdeaDelete.UseVisualStyleBackColor = true;
            this.btnIdeaDelete.Click += new System.EventHandler(this.btnIdeaDelete_Click);
            // 
            // IdeaGrid
            // 
            this.IdeaGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IdeaGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.IdeaGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIdeaId,
            this.colIdeaName});
            this.IdeaGrid.Location = new System.Drawing.Point(462, 25);
            this.IdeaGrid.MultiSelect = false;
            this.IdeaGrid.Name = "IdeaGrid";
            this.IdeaGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.IdeaGrid.Size = new System.Drawing.Size(487, 428);
            this.IdeaGrid.TabIndex = 46;
            this.IdeaGrid.SelectionChanged += new System.EventHandler(this.IdeaGrid_SelectionChanged);
            // 
            // colIdeaId
            // 
            this.colIdeaId.DataPropertyName = "Id";
            this.colIdeaId.HeaderText = "Id";
            this.colIdeaId.Name = "colIdeaId";
            this.colIdeaId.ReadOnly = true;
            this.colIdeaId.Visible = false;
            // 
            // colIdeaName
            // 
            this.colIdeaName.DataPropertyName = "Name";
            this.colIdeaName.FillWeight = 450F;
            this.colIdeaName.HeaderText = "Имя";
            this.colIdeaName.Name = "colIdeaName";
            this.colIdeaName.ReadOnly = true;
            this.colIdeaName.Width = 450;
            // 
            // btnIdeaAdd
            // 
            this.btnIdeaAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIdeaAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIdeaAdd.Location = new System.Drawing.Point(955, 25);
            this.btnIdeaAdd.Name = "btnIdeaAdd";
            this.btnIdeaAdd.Size = new System.Drawing.Size(102, 28);
            this.btnIdeaAdd.TabIndex = 53;
            this.btnIdeaAdd.Text = "Добавить";
            this.btnIdeaAdd.UseVisualStyleBackColor = true;
            this.btnIdeaAdd.Click += new System.EventHandler(this.btnIdeaAdd_Click);
            // 
            // btnIdeaModifite
            // 
            this.btnIdeaModifite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIdeaModifite.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIdeaModifite.Location = new System.Drawing.Point(955, 63);
            this.btnIdeaModifite.Name = "btnIdeaModifite";
            this.btnIdeaModifite.Size = new System.Drawing.Size(102, 28);
            this.btnIdeaModifite.TabIndex = 55;
            this.btnIdeaModifite.Text = "Изменить";
            this.btnIdeaModifite.UseVisualStyleBackColor = true;
            this.btnIdeaModifite.Click += new System.EventHandler(this.btnIdeaModifite_Click);
            // 
            // tabCreateTasks
            // 
            this.tabCreateTasks.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabCreateTasks.Controls.Add(this.btnEdit);
            this.tabCreateTasks.Controls.Add(this.label8);
            this.tabCreateTasks.Controls.Add(this.cmbState);
            this.tabCreateTasks.Controls.Add(this.cmbPlans);
            this.tabCreateTasks.Controls.Add(this.label7);
            this.tabCreateTasks.Controls.Add(this.label2);
            this.tabCreateTasks.Controls.Add(this.btnAdd);
            this.tabCreateTasks.Controls.Add(this.btnDelete);
            this.tabCreateTasks.Controls.Add(this.grid);
            this.tabCreateTasks.Controls.Add(this.gbPriorities);
            this.tabCreateTasks.Controls.Add(this.txtIssueName);
            this.tabCreateTasks.Controls.Add(this.txtDescryption);
            this.tabCreateTasks.Controls.Add(this.label1);
            this.tabCreateTasks.Controls.Add(this.dtEndDate);
            this.tabCreateTasks.Controls.Add(this.label10);
            this.tabCreateTasks.Controls.Add(this.lbCreateDate);
            this.tabCreateTasks.Controls.Add(this.dpStartDate);
            this.tabCreateTasks.Controls.Add(this.label4);
            this.tabCreateTasks.Location = new System.Drawing.Point(4, 25);
            this.tabCreateTasks.Name = "tabCreateTasks";
            this.tabCreateTasks.Padding = new System.Windows.Forms.Padding(3);
            this.tabCreateTasks.Size = new System.Drawing.Size(1063, 459);
            this.tabCreateTasks.TabIndex = 0;
            this.tabCreateTasks.Text = "Задача";
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEdit.Location = new System.Drawing.Point(950, 77);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(102, 28);
            this.btnEdit.TabIndex = 47;
            this.btnEdit.Text = "Изменить";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(260, 97);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label8.Size = new System.Drawing.Size(53, 17);
            this.label8.TabIndex = 46;
            this.label8.Text = "Статус";
            // 
            // cmbState
            // 
            this.cmbState.DisplayMember = "Value";
            this.cmbState.FormattingEnabled = true;
            this.cmbState.Location = new System.Drawing.Point(261, 116);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(218, 24);
            this.cmbState.TabIndex = 45;
            this.cmbState.ValueMember = "Key";
            // 
            // cmbPlans
            // 
            this.cmbPlans.DisplayMember = "Name";
            this.cmbPlans.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbPlans.FormattingEnabled = true;
            this.cmbPlans.Location = new System.Drawing.Point(30, 70);
            this.cmbPlans.Name = "cmbPlans";
            this.cmbPlans.Size = new System.Drawing.Size(449, 21);
            this.cmbPlans.TabIndex = 41;
            this.cmbPlans.ValueMember = "Id";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(30, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(174, 17);
            this.label7.TabIndex = 40;
            this.label7.Text = "Принадлежность к плану\r\n";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(497, 6);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(125, 26);
            this.label2.TabIndex = 38;
            this.label2.Text = "Подзадачи";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAdd.Location = new System.Drawing.Point(950, 39);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(102, 28);
            this.btnAdd.TabIndex = 37;
            this.btnAdd.Text = "Добавить";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDelete.Location = new System.Drawing.Point(950, 115);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(102, 28);
            this.btnDelete.TabIndex = 36;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // grid
            // 
            this.grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colName,
            this.colStartDate,
            this.colStates,
            this.colPriority});
            this.grid.Location = new System.Drawing.Point(499, 35);
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(441, 418);
            this.grid.TabIndex = 35;
            // 
            // colId
            // 
            this.colId.DataPropertyName = "Id";
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Visible = false;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Name";
            this.colName.HeaderText = "Имя";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            // 
            // colStartDate
            // 
            this.colStartDate.DataPropertyName = "StartDate";
            this.colStartDate.HeaderText = "Дата начала";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.ReadOnly = true;
            // 
            // colStates
            // 
            this.colStates.DataPropertyName = "State";
            this.colStates.HeaderText = "Состояние";
            this.colStates.Name = "colStates";
            this.colStates.ReadOnly = true;
            // 
            // colPriority
            // 
            this.colPriority.DataPropertyName = "Priority";
            this.colPriority.HeaderText = "Приоритет";
            this.colPriority.Name = "colPriority";
            this.colPriority.ReadOnly = true;
            // 
            // gbPriorities
            // 
            this.gbPriorities.Controls.Add(this.rdBtnTypeA);
            this.gbPriorities.Controls.Add(this.rdBtnTypeD);
            this.gbPriorities.Controls.Add(this.rdBtnTypeB);
            this.gbPriorities.Controls.Add(this.rdBtnTypeC);
            this.gbPriorities.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbPriorities.Location = new System.Drawing.Point(27, 97);
            this.gbPriorities.Name = "gbPriorities";
            this.gbPriorities.Size = new System.Drawing.Size(227, 115);
            this.gbPriorities.TabIndex = 34;
            this.gbPriorities.TabStop = false;
            this.gbPriorities.Text = "Приоритет";
            // 
            // rdBtnTypeA
            // 
            this.rdBtnTypeA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeA.AutoSize = true;
            this.rdBtnTypeA.Location = new System.Drawing.Point(6, 19);
            this.rdBtnTypeA.Name = "rdBtnTypeA";
            this.rdBtnTypeA.Size = new System.Drawing.Size(182, 21);
            this.rdBtnTypeA.TabIndex = 9;
            this.rdBtnTypeA.TabStop = true;
            this.rdBtnTypeA.Text = "(A)  Важные и срочные ";
            this.rdBtnTypeA.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeD
            // 
            this.rdBtnTypeD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeD.AutoSize = true;
            this.rdBtnTypeD.Location = new System.Drawing.Point(6, 91);
            this.rdBtnTypeD.Name = "rdBtnTypeD";
            this.rdBtnTypeD.Size = new System.Drawing.Size(219, 21);
            this.rdBtnTypeD.TabIndex = 12;
            this.rdBtnTypeD.TabStop = true;
            this.rdBtnTypeD.Text = "(D) Не важные и не срочные ";
            this.rdBtnTypeD.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeB
            // 
            this.rdBtnTypeB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeB.AutoSize = true;
            this.rdBtnTypeB.Location = new System.Drawing.Point(6, 42);
            this.rdBtnTypeB.Name = "rdBtnTypeB";
            this.rdBtnTypeB.Size = new System.Drawing.Size(186, 21);
            this.rdBtnTypeB.TabIndex = 10;
            this.rdBtnTypeB.TabStop = true;
            this.rdBtnTypeB.Text = "(B) Важные не срочные ";
            this.rdBtnTypeB.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeC
            // 
            this.rdBtnTypeC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeC.AutoSize = true;
            this.rdBtnTypeC.Location = new System.Drawing.Point(6, 69);
            this.rdBtnTypeC.Name = "rdBtnTypeC";
            this.rdBtnTypeC.Size = new System.Drawing.Size(206, 21);
            this.rdBtnTypeC.TabIndex = 11;
            this.rdBtnTypeC.TabStop = true;
            this.rdBtnTypeC.Text = "(C) Не важные, но срочные";
            this.rdBtnTypeC.UseVisualStyleBackColor = true;
            // 
            // txtIssueName
            // 
            this.txtIssueName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtIssueName.Location = new System.Drawing.Point(30, 25);
            this.txtIssueName.Name = "txtIssueName";
            this.txtIssueName.Size = new System.Drawing.Size(457, 20);
            this.txtIssueName.TabIndex = 33;
            // 
            // txtDescryption
            // 
            this.txtDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDescryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtDescryption.Location = new System.Drawing.Point(27, 235);
            this.txtDescryption.Multiline = true;
            this.txtDescryption.Name = "txtDescryption";
            this.txtDescryption.Size = new System.Drawing.Size(454, 157);
            this.txtDescryption.TabIndex = 27;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(32, 5);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(106, 17);
            this.label1.TabIndex = 32;
            this.label1.Text = "Наименование";
            // 
            // dtEndDate
            // 
            this.dtEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dtEndDate.Enabled = false;
            this.dtEndDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtEndDate.Location = new System.Drawing.Point(261, 415);
            this.dtEndDate.MinDate = new System.DateTime(1800, 4, 25, 0, 0, 0, 0);
            this.dtEndDate.Name = "dtEndDate";
            this.dtEndDate.Size = new System.Drawing.Size(200, 23);
            this.dtEndDate.TabIndex = 31;
            this.dtEndDate.Value = new System.DateTime(2016, 4, 27, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(258, 395);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 17);
            this.label10.TabIndex = 30;
            this.label10.Text = "Дата окончания";
            // 
            // lbCreateDate
            // 
            this.lbCreateDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbCreateDate.AutoSize = true;
            this.lbCreateDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCreateDate.Location = new System.Drawing.Point(30, 395);
            this.lbCreateDate.Name = "lbCreateDate";
            this.lbCreateDate.Size = new System.Drawing.Size(94, 17);
            this.lbCreateDate.TabIndex = 29;
            this.lbCreateDate.Text = "Дата начала";
            // 
            // dpStartDate
            // 
            this.dpStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dpStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dpStartDate.Location = new System.Drawing.Point(30, 415);
            this.dpStartDate.MinDate = new System.DateTime(1800, 4, 25, 0, 0, 0, 0);
            this.dpStartDate.Name = "dpStartDate";
            this.dpStartDate.Size = new System.Drawing.Size(200, 23);
            this.dpStartDate.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(27, 215);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 17);
            this.label4.TabIndex = 26;
            this.label4.Text = "Описание ";
            // 
            // tabTask
            // 
            this.tabTask.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabTask.Controls.Add(this.tabCreateTasks);
            this.tabTask.Controls.Add(this.tabPage2);
            this.tabTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabTask.Location = new System.Drawing.Point(0, 0);
            this.tabTask.Name = "tabTask";
            this.tabTask.SelectedIndex = 0;
            this.tabTask.Size = new System.Drawing.Size(1071, 488);
            this.tabTask.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCancel.Location = new System.Drawing.Point(864, 494);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(191, 31);
            this.btnCancel.TabIndex = 59;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(667, 494);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(191, 31);
            this.btnSave.TabIndex = 58;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmIssueEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1068, 539);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tabTask);
            this.MinimumSize = new System.Drawing.Size(1084, 578);
            this.Name = "frmIssueEdit";
            this.Text = "Задача";
            this.Load += new System.EventHandler(this.frmIssue_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdeaGrid)).EndInit();
            this.tabCreateTasks.ResumeLayout(false);
            this.tabCreateTasks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.gbPriorities.ResumeLayout(false);
            this.gbPriorities.PerformLayout();
            this.tabTask.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtIdeaDescription;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIdeaName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnIdeaDelete;
        private System.Windows.Forms.DataGridView IdeaGrid;
        private System.Windows.Forms.Button btnIdeaAdd;
        private System.Windows.Forms.Button btnIdeaModifite;
        private System.Windows.Forms.TabPage tabCreateTasks;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbState;
        private System.Windows.Forms.ComboBox cmbPlans;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStates;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPriority;
        private System.Windows.Forms.GroupBox gbPriorities;
        private System.Windows.Forms.RadioButton rdBtnTypeA;
        private System.Windows.Forms.RadioButton rdBtnTypeD;
        private System.Windows.Forms.RadioButton rdBtnTypeB;
        private System.Windows.Forms.RadioButton rdBtnTypeC;
        private System.Windows.Forms.TextBox txtIssueName;
        private System.Windows.Forms.TextBox txtDescryption;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtEndDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbCreateDate;
        private System.Windows.Forms.DateTimePicker dpStartDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabTask;
        private System.Windows.Forms.Label Идеи;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIdeaId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIdeaName;
    }
}
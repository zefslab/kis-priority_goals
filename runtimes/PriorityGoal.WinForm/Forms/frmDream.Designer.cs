﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmDream
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEscapeDream = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lbCreateDate = new System.Windows.Forms.Label();
            this.dpStartDate = new System.Windows.Forms.DateTimePicker();
            this.txtDescryption = new System.Windows.Forms.TextBox();
            this.rdBtnTypeADream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeDDream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeBDream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeCDream = new System.Windows.Forms.RadioButton();
            this.cmbInterests = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbDreamDescryption = new System.Windows.Forms.Label();
            this.gbDreams = new System.Windows.Forms.GroupBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lbDreamName = new System.Windows.Forms.Label();
            this.gbDreams.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEscapeDream
            // 
            this.btnEscapeDream.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEscapeDream.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEscapeDream.Location = new System.Drawing.Point(595, 348);
            this.btnEscapeDream.Name = "btnEscapeDream";
            this.btnEscapeDream.Size = new System.Drawing.Size(191, 30);
            this.btnEscapeDream.TabIndex = 59;
            this.btnEscapeDream.Text = "Отмена";
            this.btnEscapeDream.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(395, 348);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(191, 30);
            this.btnSave.TabIndex = 58;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lbCreateDate
            // 
            this.lbCreateDate.AutoSize = true;
            this.lbCreateDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCreateDate.Location = new System.Drawing.Point(24, 251);
            this.lbCreateDate.Name = "lbCreateDate";
            this.lbCreateDate.Size = new System.Drawing.Size(124, 20);
            this.lbCreateDate.TabIndex = 66;
            this.lbCreateDate.Text = "Дата создания";
            // 
            // dpStartDate
            // 
            this.dpStartDate.Location = new System.Drawing.Point(22, 275);
            this.dpStartDate.Name = "dpStartDate";
            this.dpStartDate.Size = new System.Drawing.Size(200, 20);
            this.dpStartDate.TabIndex = 65;
            // 
            // txtDescryption
            // 
            this.txtDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtDescryption.Location = new System.Drawing.Point(257, 88);
            this.txtDescryption.Multiline = true;
            this.txtDescryption.Name = "txtDescryption";
            this.txtDescryption.Size = new System.Drawing.Size(538, 253);
            this.txtDescryption.TabIndex = 63;
            // 
            // rdBtnTypeADream
            // 
            this.rdBtnTypeADream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeADream.AutoSize = true;
            this.rdBtnTypeADream.Location = new System.Drawing.Point(6, 19);
            this.rdBtnTypeADream.Name = "rdBtnTypeADream";
            this.rdBtnTypeADream.Size = new System.Drawing.Size(182, 21);
            this.rdBtnTypeADream.TabIndex = 9;
            this.rdBtnTypeADream.TabStop = true;
            this.rdBtnTypeADream.Text = "(A)  Важные и срочные ";
            this.rdBtnTypeADream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeDDream
            // 
            this.rdBtnTypeDDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeDDream.AutoSize = true;
            this.rdBtnTypeDDream.Location = new System.Drawing.Point(6, 91);
            this.rdBtnTypeDDream.Name = "rdBtnTypeDDream";
            this.rdBtnTypeDDream.Size = new System.Drawing.Size(219, 21);
            this.rdBtnTypeDDream.TabIndex = 12;
            this.rdBtnTypeDDream.TabStop = true;
            this.rdBtnTypeDDream.Text = "(D) Не важные и не срочные ";
            this.rdBtnTypeDDream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeBDream
            // 
            this.rdBtnTypeBDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeBDream.AutoSize = true;
            this.rdBtnTypeBDream.Location = new System.Drawing.Point(6, 43);
            this.rdBtnTypeBDream.Name = "rdBtnTypeBDream";
            this.rdBtnTypeBDream.Size = new System.Drawing.Size(186, 21);
            this.rdBtnTypeBDream.TabIndex = 10;
            this.rdBtnTypeBDream.TabStop = true;
            this.rdBtnTypeBDream.Text = "(B) Важные не срочные ";
            this.rdBtnTypeBDream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeCDream
            // 
            this.rdBtnTypeCDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeCDream.AutoSize = true;
            this.rdBtnTypeCDream.Location = new System.Drawing.Point(6, 67);
            this.rdBtnTypeCDream.Name = "rdBtnTypeCDream";
            this.rdBtnTypeCDream.Size = new System.Drawing.Size(206, 21);
            this.rdBtnTypeCDream.TabIndex = 11;
            this.rdBtnTypeCDream.TabStop = true;
            this.rdBtnTypeCDream.Text = "(C) Не важные, но срочные";
            this.rdBtnTypeCDream.UseVisualStyleBackColor = true;
            // 
            // cmbInterests
            // 
            this.cmbInterests.DisplayMember = "Name";
            this.cmbInterests.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbInterests.FormattingEnabled = true;
            this.cmbInterests.Location = new System.Drawing.Point(14, 88);
            this.cmbInterests.Name = "cmbInterests";
            this.cmbInterests.Size = new System.Drawing.Size(233, 24);
            this.cmbInterests.TabIndex = 68;
            this.cmbInterests.ValueMember = "Id";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(15, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 67;
            this.label1.Text = "Интересы";
            // 
            // lbDreamDescryption
            // 
            this.lbDreamDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDreamDescryption.AutoSize = true;
            this.lbDreamDescryption.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lbDreamDescryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamDescryption.Location = new System.Drawing.Point(254, 68);
            this.lbDreamDescryption.Name = "lbDreamDescryption";
            this.lbDreamDescryption.Size = new System.Drawing.Size(78, 17);
            this.lbDreamDescryption.TabIndex = 64;
            this.lbDreamDescryption.Text = "Описание ";
            // 
            // gbDreams
            // 
            this.gbDreams.Controls.Add(this.rdBtnTypeADream);
            this.gbDreams.Controls.Add(this.rdBtnTypeDDream);
            this.gbDreams.Controls.Add(this.rdBtnTypeBDream);
            this.gbDreams.Controls.Add(this.rdBtnTypeCDream);
            this.gbDreams.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbDreams.Location = new System.Drawing.Point(22, 126);
            this.gbDreams.Name = "gbDreams";
            this.gbDreams.Size = new System.Drawing.Size(232, 117);
            this.gbDreams.TabIndex = 62;
            this.gbDreams.TabStop = false;
            this.gbDreams.Text = "Приоритет";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtName.Location = new System.Drawing.Point(15, 31);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(780, 23);
            this.txtName.TabIndex = 61;
            // 
            // lbDreamName
            // 
            this.lbDreamName.AutoSize = true;
            this.lbDreamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamName.Location = new System.Drawing.Point(12, 11);
            this.lbDreamName.Name = "lbDreamName";
            this.lbDreamName.Size = new System.Drawing.Size(72, 17);
            this.lbDreamName.TabIndex = 60;
            this.lbDreamName.Text = "Название";
            // 
            // frmDream
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(807, 390);
            this.Controls.Add(this.btnEscapeDream);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lbCreateDate);
            this.Controls.Add(this.dpStartDate);
            this.Controls.Add(this.txtDescryption);
            this.Controls.Add(this.cmbInterests);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbDreamDescryption);
            this.Controls.Add(this.gbDreams);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lbDreamName);
            this.MinimumSize = new System.Drawing.Size(823, 429);
            this.Name = "frmDream";
            this.Text = "Мечта";
            this.Load += new System.EventHandler(this.frmDream_Load);
            this.gbDreams.ResumeLayout(false);
            this.gbDreams.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEscapeDream;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lbCreateDate;
        private System.Windows.Forms.DateTimePicker dpStartDate;
        private System.Windows.Forms.TextBox txtDescryption;
        private System.Windows.Forms.RadioButton rdBtnTypeADream;
        private System.Windows.Forms.RadioButton rdBtnTypeDDream;
        private System.Windows.Forms.RadioButton rdBtnTypeBDream;
        private System.Windows.Forms.RadioButton rdBtnTypeCDream;
        private System.Windows.Forms.ComboBox cmbInterests;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbDreamDescryption;
        private System.Windows.Forms.GroupBox gbDreams;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lbDreamName;
    }
}
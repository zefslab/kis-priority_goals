﻿using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PriorityGoal.WinForm.Helpers;
using PriorityGoals.Api.Dpo.Interests;
using PriorityGoals.Dao.DbRepositories;
using PriorityGoals.Service.Service;

namespace PriorityGoal.WinForm.Forms
{
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;

    public partial class frmInterestList : Form
    {
        ICrudService _service;
        IList<InterestDpo> _interests;

        public frmInterestList(ICrudService service)
        {
            this._service = service;
          
            InitializeComponent();
        }
        private void frmInterestList_Load(object sender, System.EventArgs e)
        {
            this.grid.AutoGenerateColumns = false;

            this.refreshGrid();
        }

        private void refreshGrid()
        {
            /// Изменил ИЭФ 2016.08.34 Добавил условие проверки на точность
            this._interests = this._service.Get<InterestDpo>(null).ToList();
            this.grid.DataSource = null;
            this.grid.DataSource = this._interests;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            var form = new frmInterest(ServiceFactory.InitializeServiceFactory().GetService<Principle>());
            //В свойство формы передаем новый объект
            form.Interest = new InterestDetailDpo() {InterestDate = DateTime.Now};

            var answer = form.ShowDialog();

            if (answer == DialogResult.OK)
            {
                //Сохранить данные в БД
                var interest = this._service.Add(form.Interest);
                //Обновить список в гриде
                if (interest != null && interest.Id != Guid.Empty)
                {
                    this.refreshGrid();
                }
            }
            else
            {
#if DEBUG
                //Ничего не делаем
                Debug.Print("Пользователь отказался от сохранения");
#endif
            }

        }

        private void btnModifite_Click(object sender, EventArgs e)
        {
            var frm = new frmInterest(ServiceFactory.InitializeServiceFactory().GetService<Principle>());
           
            var id = this.grid.GetIdFromSelectedRow();
            if (id == Guid.Empty)
            {
                return;
            }
                
            frm.Interest = this._service.Get<InterestDetailDpo>(id);

            if (frm.ShowDialog() == DialogResult.OK)
            {
                this._service.Update(frm.Interest);
                this.refreshGrid();
            }
            else
            {
#if DEBUG
                //Ничего не делаем
                Debug.Print("Пользователь отказался от сохранения");
#endif
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.grid.DeleteSelectedItem(this._service, this.refreshGrid);
        }

        private void grid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            this.txtDescryption.Text = this.grid.SelectedRows[0].Cells["colDescription"].Value.ToString();
        }
    }
}

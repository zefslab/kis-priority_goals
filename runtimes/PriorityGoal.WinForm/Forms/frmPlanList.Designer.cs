﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmPlanList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chbEnablePriority = new System.Windows.Forms.CheckBox();
            this.chkListBoxPriority = new System.Windows.Forms.CheckedListBox();
            this.chbEnableEndDate = new System.Windows.Forms.CheckBox();
            this.chbEnableBeginDate = new System.Windows.Forms.CheckBox();
            this.dtEnd = new System.Windows.Forms.DateTimePicker();
            this.dtBegin = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbPlanState = new System.Windows.Forms.ComboBox();
            this.lbPlanState = new System.Windows.Forms.Label();
            this.cmbGoals = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.grid = new System.Windows.Forms.DataGridView();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModifite = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBeginDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colEndDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPlanState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chbEnablePriority);
            this.splitContainer1.Panel1.Controls.Add(this.chkListBoxPriority);
            this.splitContainer1.Panel1.Controls.Add(this.chbEnableEndDate);
            this.splitContainer1.Panel1.Controls.Add(this.chbEnableBeginDate);
            this.splitContainer1.Panel1.Controls.Add(this.dtEnd);
            this.splitContainer1.Panel1.Controls.Add(this.dtBegin);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.cmbPlanState);
            this.splitContainer1.Panel1.Controls.Add(this.lbPlanState);
            this.splitContainer1.Panel1.Controls.Add(this.cmbGoals);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1038, 598);
            this.splitContainer1.SplitterDistance = 292;
            this.splitContainer1.TabIndex = 1;
            // 
            // chbEnablePriority
            // 
            this.chbEnablePriority.AutoSize = true;
            this.chbEnablePriority.Location = new System.Drawing.Point(239, 106);
            this.chbEnablePriority.Name = "chbEnablePriority";
            this.chbEnablePriority.Size = new System.Drawing.Size(15, 14);
            this.chbEnablePriority.TabIndex = 25;
            this.chbEnablePriority.UseVisualStyleBackColor = true;
            this.chbEnablePriority.CheckedChanged += new System.EventHandler(this.chbEnablePriority_CheckedChanged);
            // 
            // chkListBoxPriority
            // 
            this.chkListBoxPriority.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.chkListBoxPriority.CheckOnClick = true;
            this.chkListBoxPriority.Enabled = false;
            this.chkListBoxPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkListBoxPriority.FormattingEnabled = true;
            this.chkListBoxPriority.Items.AddRange(new object[] {
            "Важные и срочные ",
            "Важные не срочные ",
            "Не важные, но срочные ",
            "Не важные и не срочные "});
            this.chkListBoxPriority.Location = new System.Drawing.Point(11, 106);
            this.chkListBoxPriority.Name = "chkListBoxPriority";
            this.chkListBoxPriority.Size = new System.Drawing.Size(222, 76);
            this.chkListBoxPriority.TabIndex = 24;
            // 
            // chbEnableEndDate
            // 
            this.chbEnableEndDate.AutoSize = true;
            this.chbEnableEndDate.Location = new System.Drawing.Point(240, 257);
            this.chbEnableEndDate.Name = "chbEnableEndDate";
            this.chbEnableEndDate.Size = new System.Drawing.Size(15, 14);
            this.chbEnableEndDate.TabIndex = 22;
            this.chbEnableEndDate.UseVisualStyleBackColor = true;
            this.chbEnableEndDate.CheckedChanged += new System.EventHandler(this.chbEnableEndDate_CheckedChanged);
            // 
            // chbEnableBeginDate
            // 
            this.chbEnableBeginDate.AutoSize = true;
            this.chbEnableBeginDate.Location = new System.Drawing.Point(239, 208);
            this.chbEnableBeginDate.Name = "chbEnableBeginDate";
            this.chbEnableBeginDate.Size = new System.Drawing.Size(15, 14);
            this.chbEnableBeginDate.TabIndex = 21;
            this.chbEnableBeginDate.UseVisualStyleBackColor = true;
            this.chbEnableBeginDate.CheckedChanged += new System.EventHandler(this.chbEnableBeginDate_CheckedChanged);
            // 
            // dtEnd
            // 
            this.dtEnd.Enabled = false;
            this.dtEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtEnd.Location = new System.Drawing.Point(12, 257);
            this.dtEnd.Name = "dtEnd";
            this.dtEnd.Size = new System.Drawing.Size(222, 23);
            this.dtEnd.TabIndex = 8;
            this.dtEnd.ValueChanged += new System.EventHandler(this.dtEnd_ValueChanged);
            // 
            // dtBegin
            // 
            this.dtBegin.Enabled = false;
            this.dtBegin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtBegin.Location = new System.Drawing.Point(12, 208);
            this.dtBegin.Name = "dtBegin";
            this.dtBegin.Size = new System.Drawing.Size(222, 23);
            this.dtBegin.TabIndex = 7;
            this.dtBegin.ValueChanged += new System.EventHandler(this.dtBegin_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(13, 234);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Конец периода";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(13, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Начало периода";
            // 
            // cmbPlanState
            // 
            this.cmbPlanState.DisplayMember = "Value";
            this.cmbPlanState.FormattingEnabled = true;
            this.cmbPlanState.Location = new System.Drawing.Point(12, 79);
            this.cmbPlanState.Name = "cmbPlanState";
            this.cmbPlanState.Size = new System.Drawing.Size(222, 21);
            this.cmbPlanState.TabIndex = 4;
            this.cmbPlanState.ValueMember = "Key";
            // 
            // lbPlanState
            // 
            this.lbPlanState.AutoSize = true;
            this.lbPlanState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbPlanState.Location = new System.Drawing.Point(13, 56);
            this.lbPlanState.Name = "lbPlanState";
            this.lbPlanState.Size = new System.Drawing.Size(141, 20);
            this.lbPlanState.TabIndex = 3;
            this.lbPlanState.Text = "Состояние плана";
            // 
            // cmbGoals
            // 
            this.cmbGoals.FormattingEnabled = true;
            this.cmbGoals.Location = new System.Drawing.Point(12, 32);
            this.cmbGoals.Name = "cmbGoals";
            this.cmbGoals.Size = new System.Drawing.Size(232, 21);
            this.cmbGoals.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Цели";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.grid);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.txtDescription);
            this.splitContainer2.Panel2.Controls.Add(this.label2);
            this.splitContainer2.Panel2.Controls.Add(this.btnDelete);
            this.splitContainer2.Panel2.Controls.Add(this.btnModifite);
            this.splitContainer2.Panel2.Controls.Add(this.btnCreate);
            this.splitContainer2.Size = new System.Drawing.Size(742, 598);
            this.splitContainer2.SplitterDistance = 377;
            this.splitContainer2.TabIndex = 0;
            // 
            // grid
            // 
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colName,
            this.colPriority,
            this.colBeginDate,
            this.colEndDate,
            this.colPlanState});
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.Name = "grid";
            this.grid.ReadOnly = true;
            this.grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid.Size = new System.Drawing.Size(742, 377);
            this.grid.TabIndex = 0;
            this.grid.SelectionChanged += new System.EventHandler(this.grid_SelectionChanged);
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription.Location = new System.Drawing.Point(3, 54);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new System.Drawing.Size(733, 160);
            this.txtDescription.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Описание";
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnDelete.Location = new System.Drawing.Point(219, 6);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(102, 28);
            this.btnDelete.TabIndex = 6;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModifite
            // 
            this.btnModifite.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnModifite.Location = new System.Drawing.Point(111, 6);
            this.btnModifite.Name = "btnModifite";
            this.btnModifite.Size = new System.Drawing.Size(102, 28);
            this.btnModifite.TabIndex = 3;
            this.btnModifite.Text = "Изменить";
            this.btnModifite.UseVisualStyleBackColor = true;
            this.btnModifite.Click += new System.EventHandler(this.btnModifite_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCreate.Location = new System.Drawing.Point(3, 6);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(102, 28);
            this.btnCreate.TabIndex = 1;
            this.btnCreate.Text = "Создать";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // colId
            // 
            this.colId.DataPropertyName = "Id";
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Visible = false;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Name";
            this.colName.FillWeight = 300F;
            this.colName.HeaderText = "Название";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 300;
            // 
            // colPriority
            // 
            this.colPriority.DataPropertyName = "Priority";
            this.colPriority.HeaderText = "Приоритет";
            this.colPriority.Name = "colPriority";
            this.colPriority.ReadOnly = true;
            // 
            // colBeginDate
            // 
            this.colBeginDate.DataPropertyName = "BeginDate";
            this.colBeginDate.HeaderText = "Дата начала";
            this.colBeginDate.Name = "colBeginDate";
            this.colBeginDate.ReadOnly = true;
            // 
            // colEndDate
            // 
            this.colEndDate.DataPropertyName = "EndDate";
            this.colEndDate.HeaderText = "Дата окончания";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.ReadOnly = true;
            // 
            // colPlanState
            // 
            this.colPlanState.DataPropertyName = "DisplayState";
            this.colPlanState.HeaderText = "Состояние плана";
            this.colPlanState.Name = "colPlanState";
            this.colPlanState.ReadOnly = true;
            // 
            // frmPlanList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1038, 598);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(1054, 637);
            this.Name = "frmPlanList";
            this.Text = "Планы";
            this.Load += new System.EventHandler(this.frmPlanList_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.ComboBox cmbGoals;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtEnd;
        private System.Windows.Forms.DateTimePicker dtBegin;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbPlanState;
        private System.Windows.Forms.Label lbPlanState;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModifite;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chbEnablePriority;
        private System.Windows.Forms.CheckedListBox chkListBoxPriority;
        private System.Windows.Forms.CheckBox chbEnableEndDate;
        private System.Windows.Forms.CheckBox chbEnableBeginDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBeginDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colEndDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPlanState;
    }
}
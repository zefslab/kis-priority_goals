﻿using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Dpo.Dream;
    using PriorityGoals.Api.Dpo.Interests;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    public partial class frmDreamList : Form
    {
        ICrudService _service;

        ICrudService _interestService;

        DreamFilter filter = new DreamFilter();

        IList<DreamDpo> _dreams; 

        /// <summary>
        /// признак того, что форма загружается в первый раз
        /// поэтому открывается пустое окно (не применяются фильтры).
        /// </summary>
        private bool IsFormNotLoaded = true;
        public frmDreamList(IServiceFactory serviceFactory)
        {
            if (serviceFactory == null)
            {
                throw new ArgumentNullException(nameof(serviceFactory));
            }

            InitializeComponent();

            this._service = serviceFactory.GetService<Dream>();
            this._interestService = serviceFactory.GetService<Interest>();
        }

        private void frmDreamList_Load(object sender, EventArgs e)
        {
            this.grid.AutoGenerateColumns = false;

            #region Производим настройку элементов фильтра

            chkListBoxPriority.InitializePriorityCheckListBox();

            this.chkListBoxPriority.SelectedIndexChanged += this.chkListBoxPriority_SelectedIndexChanged;
            var interests = this._interestService.Get<InterestDpo>(new InterestFilter()).ToList();
            interests.Add(new InterestDpo {Name = "---", Id = Guid.Empty});
            this.cmbInterests.DataSource = interests.OrderBy(i => i.Id).ToList();

            this.cmbInterests.DisplayMember = "Name";
            this.cmbInterests.ValueMember = "Id";

          
            #endregion

            IsFormNotLoaded = false;

            FilterApply();
        }
        private void refreshGrid()
        {

            /// Изменил ИЭФ 2016.08.34 Добавил условие проверки на точность
            this._dreams = this._service.Get<DreamDpo>(null).ToList();

            this.grid.DataSource = null;
            this.grid.DataSource = this._dreams;
        }

        /// <summary>
        /// Меняются настройка фильтра со списком планов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbInterests_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsFormNotLoaded) return;

            FilterApply();
        }

        /// <summary>
        /// Применяются настройки фильтров при перезагрузки списка задач в гриде
        /// Создал ЗЕФ 2016.04.12
        /// </summary>
        private void FilterApply()
        {
            this.filter = new DreamFilter();
            filter.InterestId = cmbInterests.SelectedValue as Guid?;

            if (this.chbEnablePriority.Checked)
            {
                this.filter.Priorities = this.chkListBoxPriority.CheckedItems.OfType<KeyValuePair<int, string>>()
                    .Select(x => x.Key).ToList();
            }

            /// Изменил ЗЕФ 2016.08.34 Добавил условие проверки на точность
            _dreams = this._service.Get<DreamDpo>(filter).ToList();

            this.grid.DataSource = null;
            this.grid.DataSource = this._dreams;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            var form = new frmDream(ServiceFactory.InitializeServiceFactory());
            //В свойство формы передаем новый объект
            form.Dream = new DreamDetailDpo() ;

            var answer = form.ShowDialog();

            if (answer == DialogResult.OK)
            {
                //Сохранить данные в БД
                var dream = this._service.Add(form.Dream);
                //Обновить список в гриде
                if (dream != null && dream.Id != Guid.Empty)
                {
                    this.FilterApply();
                }
            }
            else
            {
#if DEBUG
                //Ничего не делаем
                Debug.Print("Пользователь отказался от сохранения");
#endif
            }

        }

        private void btnModifiteDream_Click(object sender, EventArgs e)
        {

            var form = new frmDream(ServiceFactory.InitializeServiceFactory());
           
                var id = this.grid.GetIdFromSelectedRow();
                if (id == Guid.Empty)
                {
                    return;
                }
                    
                var dream = this._service.Get<DreamDetailDpo>(id);
                form.Dream = dream;

                if (form.ShowDialog() == DialogResult.OK)
                {
                    this._service.Update(form.Dream);
                    this.FilterApply();
                }
            
        }

        private void grid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var dream = _dreams.FirstOrDefault(
                d => d.Id == Guid.Parse(this.grid.SelectedRows[0].Cells["colId"].Value.ToString()));
            this.txtDescription.Text = dream.Description;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.grid.DeleteSelectedItem(this._service, this.FilterApply);
        }

        private void chbEnablePriority_CheckedChanged(object sender, EventArgs e)
        {
            this.chkListBoxPriority.Enabled = this.chbEnablePriority.Checked;
            FilterApply();
        }

        private void chkListBoxPriority_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterApply();
        }
    }
}

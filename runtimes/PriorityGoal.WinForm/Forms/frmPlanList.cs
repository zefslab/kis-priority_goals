﻿using System;
using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    public partial class frmPlanList : Form
    {
        ICrudService _service;

        ICrudService _goalService;

        PlanFilter filter = new PlanFilter();

        IList<PlanDpo> _plans;

        public frmPlanList(IServiceFactory serviceFactory)
        {
            if (serviceFactory == null)
            {
                throw new ArgumentNullException(nameof(serviceFactory));
            }
            this._service = serviceFactory.GetService<Plan>();
            this._goalService = serviceFactory.GetService<Goal>();

            InitializeComponent();
        }

        private void frmPlanList_Load(object sender, EventArgs e)
        {
            this.grid.AutoGenerateColumns = false;

            #region Производим настройку элементов фильтра

            this.cmbGoals.DataSource = null;
            var goals = this._goalService.Get<GoalDpo>(null).ToList();
            goals.Add(new GoalDpo { Name = "---", Id = Guid.Empty });
            this.cmbGoals.DisplayMember = "Name";
            this.cmbGoals.ValueMember = "Id";
            this.cmbGoals.DataSource = goals.OrderBy(g => g.Name).ToList();
            this.cmbGoals.SelectedIndexChanged += this.cmbGoals_SelectedIndexChanged_1;

            this.cmbPlanState.DataSource = null;
            var list = StateListProvider.GetList();
            list.Add(new KeyValuePair<States, string>(0, "---"));
            this.cmbPlanState.DataSource = list.OrderBy(x=>x.Value).ToList();
            this.cmbPlanState.SelectedIndexChanged += this.cmbPlanState_SelectedIndexChanged;

            chkListBoxPriority.InitializePriorityCheckListBox();
           
            this.chkListBoxPriority.SelectedIndexChanged += this.chkListBoxPriority_SelectedIndexChanged;


            #endregion

            IsFormNotLoaded = false;
            FilterApply();
        }
        /// <summary>
        /// признак того, что форма загружается в первый раз
        /// поэтому открывается пустое окно (не применяются фильтры).
        /// </summary>
        private bool IsFormNotLoaded = true;

        /// <summary>
        /// Применяются настройки фильтров при перезагрузке списка задач в гриде
        /// Создал ЗЕФ 2016.04.12
        /// </summary>
        private void FilterApply()

        {
            filter = new PlanFilter();
            var goalId = (Guid)this.cmbGoals.SelectedValue;
            this.filter.GoalId = goalId != Guid.Empty? (Guid?)goalId : null ;

            var state = (States?)cmbPlanState.SelectedValue;
            filter.State = state != 0 ? state : null;

            this.filter.DataBegin = this.chbEnableBeginDate.Checked ? (DateTime?)this.dtBegin.Value.Date : null;
            this.filter.DataEnd = this.chbEnableEndDate.Checked ? (DateTime?)dtEnd.Value.Date : null;
            if (this.chbEnablePriority.Checked)
            {
                this.filter.Priorities = this.chkListBoxPriority.CheckedItems.OfType<KeyValuePair<int, string>>()
                    .Select(x=>x.Key).ToList();

            }
            
            /// Изменил ИЭФ 2016.08.34 Добавил условие проверки на точность
            this._plans = this._service.Get<PlanDpo>(filter).ToList();

            this.grid.DataSource = null;
            this.grid.DataSource = this._plans;
        }
      

        private void btnCreate_Click(object sender, EventArgs e)
        {
            var form = new frmPlanCreate(ServiceFactory.InitializeServiceFactory());

            //Передаем новый объект в свойство формы

            form.Plan = new PlanDetailDpo() {BeginDate = DateTime.Now, State = States.New};

            if (form.ShowDialog()== DialogResult.OK)
            {
                var plan = this._service.Add(form.Plan);

                if (plan !=null && plan.Id !=Guid.Empty)
                {
                    this.FilterApply();
                }
#if DEBUG
                else
                {
                    Debug.Print("Пользователь отказался от сохранения");
                }
#endif
            }
        }

        private void btnModifite_Click(object sender, EventArgs e)
        {
            var id = this.grid.GetIdFromSelectedRow();
            if (id == Guid.Empty) return;

            var form = new frmPlanEdit(ServiceFactory.InitializeServiceFactory());

            form.Plan = this._service.Get<PlanDetailDpo>(id);

            if (form.ShowDialog() == DialogResult.OK)
            {
                var plan = _service.Update(form.Plan);

                if (plan != null)
                {
                    this.FilterApply();
                }
                else
                {
                    {
#if DEBUG
                        //Ничего не делаем
                        Debug.Print("Пользователь отказался от сохранения");
#endif
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.grid.DeleteSelectedItem(this._service, this.FilterApply);
        }

        private void cmbGoals_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void dtBegin_ValueChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void dtEnd_ValueChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void cmbPlanState_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void chbEnableBeginDate_CheckedChanged(object sender, EventArgs e)
        {
            this.dtBegin.Enabled = this.chbEnableBeginDate.Checked;
            FilterApply();
        }

        private void chbEnableEndDate_CheckedChanged(object sender, EventArgs e)
        {
            this.dtEnd.Enabled = this.chbEnableEndDate.Checked;
            FilterApply();
        }

        private void chbEnablePriority_CheckedChanged(object sender, EventArgs e)
        {
            this.chkListBoxPriority.Enabled = this.chbEnablePriority.Checked;
            FilterApply();
        }

        private void chkListBoxPriority_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        

        

        private void grid_SelectionChanged(object sender, EventArgs e)
        {
            if (this.grid.SelectedRows.Count > 0)
            {
                var id = this.grid.GetIdFromSelectedRow();
                this.txtDescription.Text = this._service.Get<PlanDpo>(id).Description;
            }
        }
    }
}

﻿using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using PriorityGoals.Api.Dpo.Principle;

    public partial class frmPriciple : Form
    {
        public PrincipleDpo Principle { get; set; }
        
        public frmPriciple()
        {
            InitializeComponent();
        }

        private void frmPriciple_Load(object sender, System.EventArgs e)
        {
            this.txtPrincipleName.Text =this.Principle.Name;
            this.txtPrincipleDescription.Text = this.Principle.Description;
        }

        private void btnCreate_Click(object sender, System.EventArgs e)
        {
            this.Principle.Name = this.txtPrincipleName.Text;
            this.Principle.Description = this.txtPrincipleDescription.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();

        }
    }
}

﻿using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo.Principle;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;

    public partial class frmPrincipleList : Form
    {
        ICrudService _service;

        IList<PrincipleDpo> _principles;

        public frmPrincipleList(IServiceFactory serviceFactory)
        {
            if (serviceFactory == null)
            {
                throw new ArgumentNullException(nameof(serviceFactory));
            }
            this._service = serviceFactory.GetService<Principle>();

            InitializeComponent();
        }

        private void btnModifitePrinciple_Click(object sender, System.EventArgs e)
        {
            var id = this.principleGrid.GetIdFromSelectedRow();
            if (id == Guid.Empty) return;

            var form = new frmPriciple();
            //В свойство формы передаем новый объект
            form.Principle = this._principles.FirstOrDefault(x=>x.Id == id);

            var answer = form.ShowDialog();

            if (answer == DialogResult.OK)
            {
                //Сохранить данные в БД
                var principle = this._service.Update(form.Principle);
                //Обновить список в гриде
                if (principle != null && principle.Id != Guid.Empty)
                {
                    this.principleGrid.DataSource = null;
                    this.principleGrid.DataSource = this._principles;
                }
            }
            else
            {
#if DEBUG
                //Ничего не делаем
                Debug.Print("Пользователь отказался от сохранения");
#endif
            }
        }

        private void frmPrincipleList_Load(object sender, System.EventArgs e)
        {
            principleGrid.AutoGenerateColumns = false;
            this.filterApply();
        }

        private void filterApply()
        {

            /// Изменил ИЭФ 2016.08.34 Добавил условие проверки на точность
            this._principles = this._service.Get<PrincipleDpo>(null).ToList();

            this.principleGrid.DataSource = null;
            this.principleGrid.DataSource = this._principles;
        }

        private void btnCreatePrinciple_Click(object sender, EventArgs e)
        {
            var form = new frmPriciple();
            //В свойство формы передаем новый объект
            form.Principle = new PrincipleDpo();

            var answer = form.ShowDialog();

            if (answer == DialogResult.OK)
            {
                //Сохранить данные в БД
                var principle = this._service.Add(form.Principle);
                //Обновить список в гриде
                if (principle != null && principle.Id != Guid.Empty)
                {
                    this._principles.Add(principle);
                    this.principleGrid.DataSource = null;
                    this.principleGrid.DataSource = this._principles;
                }
            }
            else
            {
#if DEBUG
                //Ничего не делаем
                Debug.Print("Пользователь отказался от сохранения");
#endif
            }
        }

        private void principleGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var id = Guid.Parse(this.principleGrid.SelectedRows[0].Cells["colId"].Value.ToString());

            var principle = this._principles.First(p => p.Id == id);

            this.txtDescryption.Text = principle.Description;
        }

        private void btnDeletePrinciple_Click(object sender, EventArgs e)
        {
            this.principleGrid.DeleteSelectedItem(this._service, this.filterApply);
        }
    }
}

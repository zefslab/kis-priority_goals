﻿using System;
using System.Windows.Forms;

namespace PriorityGoal.WinForm
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    using PriorityGoal.WinForm.Forms;
    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Dpo.Dream;
    using PriorityGoals.Api.Dpo.Interests;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    public partial class frmGoalList : Form
    {
        ICrudService _service;

        ICrudService _dreamService;

        GoalFilter filter = new GoalFilter();

        IList<GoalDpo> _goals;

        /// <summary>
        /// признак того, что форма загружается в первый раз
        /// поэтому открывается пустое окно (не применяются фильтры).
        /// </summary>
        private bool IsFormNotLoaded = true;

        public frmGoalList(IServiceFactory factory)
        {
            if (factory == null)
            {
                throw new ArgumentNullException(nameof(factory));
            }
            InitializeComponent();

            this._service = factory.GetService<Goal>();
            this._dreamService = factory.GetService<Dream>();
        }

        private void frmGoals_Load(object sender, EventArgs e)
        {
            this.grid.AutoGenerateColumns = false;


            #region Производим настройку элементов фильтра

            var dreams = this._dreamService.Get<DreamDpo>(new DreamFilter()).ToList();
            dreams.Add(new DreamDpo() { Id = Guid.Empty, Name = "---" });
            this.cmbDreams.DataSource = dreams.OrderBy(d => d.Id).ToList();
            this.cmbDreams.DisplayMember = "Name";
            this.cmbDreams.ValueMember = "Id";

            chkListBoxPriority.InitializePriorityCheckListBox();

            #endregion

            this.dtBegin.Enabled = this.chbEnableBeginDate.Checked;
            this.dtEnd.Enabled = this.chbEnableEndDate.Checked;
            this.chkListBoxPriority.Enabled = this.chbEnablePriority.Checked;
            this.IsFormNotLoaded = false;

            FilterApply();
            
           
        }

        private void refreshGrid()
        {
            this._goals = this._service.Get<GoalDpo>(this.filter).ToList();

            this.grid.DataSource = null;
            this.grid.DataSource = this._goals;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            var form = new frmGoal(ServiceFactory.InitializeServiceFactory());
            //В свойство формы передаем новый объект
            form.Goal = new GoalDetailDpo() { GoalDate = DateTime.Now };

            if (form.ShowDialog() == DialogResult.OK)
            {
                //Сохранить данные в БД
                var goal = this._service.Add(form.Goal);
                //Обновить список в гриде
                if (goal != null && goal.Id != Guid.Empty)
                {
                    this.refreshGrid();
                }
            }
        }

      

        private void FilterApply()
        {
            filter = new GoalFilter();

            var id = this.cmbDreams.SelectedValue as Guid?;
            if (id != Guid.Empty)
            {
                this.filter.DreamId = id;
            }
            if (this.dtBegin.Enabled)
            {
                this.filter.DateBegin = this.dtBegin.Value;
            }
            if (this.dtEnd.Enabled)
            {
                this.filter.DateEnd = this.dtEnd.Value;
            }
            this.grid.DataSource = null;

            this._goals = this._service.Get<GoalDpo>(filter).ToList();

            this.grid.DataSource = this._goals;
        }

        private void chbEnableBeginDate_CheckedChanged(object sender, EventArgs e)
        {
            this.dtBegin.Enabled = this.chbEnableBeginDate.Checked;
        }

        private void chbEnableEndDate_CheckedChanged(object sender, EventArgs e)
        {
            this.dtEnd.Enabled = this.chbEnableEndDate.Checked;
        }

        private void cmbDreams_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void dtBegin_ValueChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

        private void dtEnd_ValueChanged(object sender, EventArgs e)
        {
            this.FilterApply();
        }

       
        private void btnModifite_Click(object sender, EventArgs e)
        {
           
                var id = this.grid.GetIdFromSelectedRow();

                if (id == Guid.Empty)
                {
                    return;
                }

                var form = new frmGoal(ServiceFactory.InitializeServiceFactory());
                 form.Goal = this._service.Get<GoalDetailDpo>(id);

                if (form.ShowDialog() == DialogResult.OK)
                {
                    //Обновить данные в БД
                    var goal = this._service.Update(form.Goal);
                    //Обновить список в гриде
                    if (goal != null)
                    {
                        this.refreshGrid();
                    }
                }
            

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.grid.DeleteSelectedItem(this._service, this.refreshGrid);
        }

        private void grid_SelectionChanged(object sender, EventArgs e)
        {
            if (this.grid.SelectedRows.Count > 0)
            {
                var id = Guid.Parse(this.grid.SelectedRows[0].Cells["colId"].Value.ToString());

                var goal = this._goals.FirstOrDefault(g => g.Id == id);

                this.txtDescription.Text = goal.Description;
            }

        }

        private void chbEnablePriority_CheckedChanged(object sender, EventArgs e)
        {
            
            this.chkListBoxPriority.Enabled = this.chbEnablePriority.Checked;
        }

        private void chListBoxPriority_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}



        

        


﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmInterest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbPrinciples = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbCreateDate = new System.Windows.Forms.Label();
            this.dtStartDate = new System.Windows.Forms.DateTimePicker();
            this.lbDreamDescryption = new System.Windows.Forms.Label();
            this.txtDescryption = new System.Windows.Forms.TextBox();
            this.gbPriorities = new System.Windows.Forms.GroupBox();
            this.rbtnTypeA = new System.Windows.Forms.RadioButton();
            this.rbtnTypeD = new System.Windows.Forms.RadioButton();
            this.rbtnTypeB = new System.Windows.Forms.RadioButton();
            this.rbtnTypeC = new System.Windows.Forms.RadioButton();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lbDreamName = new System.Windows.Forms.Label();
            this.btnCreateInterest = new System.Windows.Forms.Button();
            this.btnEscapeInterest = new System.Windows.Forms.Button();
            this.gbPriorities.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbPrinciples
            // 
            this.cmbPrinciples.DisplayMember = "Name";
            this.cmbPrinciples.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cmbPrinciples.FormattingEnabled = true;
            this.cmbPrinciples.Location = new System.Drawing.Point(15, 75);
            this.cmbPrinciples.Name = "cmbPrinciples";
            this.cmbPrinciples.Size = new System.Drawing.Size(232, 24);
            this.cmbPrinciples.TabIndex = 77;
            this.cmbPrinciples.ValueMember = "Id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(22, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 76;
            this.label4.Text = "Принцип";
            // 
            // lbCreateDate
            // 
            this.lbCreateDate.AutoSize = true;
            this.lbCreateDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCreateDate.Location = new System.Drawing.Point(15, 267);
            this.lbCreateDate.Name = "lbCreateDate";
            this.lbCreateDate.Size = new System.Drawing.Size(124, 20);
            this.lbCreateDate.TabIndex = 75;
            this.lbCreateDate.Text = "Дата создания";
            // 
            // dtStartDate
            // 
            this.dtStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dtStartDate.Location = new System.Drawing.Point(19, 299);
            this.dtStartDate.Name = "dtStartDate";
            this.dtStartDate.Size = new System.Drawing.Size(200, 23);
            this.dtStartDate.TabIndex = 74;
            // 
            // lbDreamDescryption
            // 
            this.lbDreamDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDreamDescryption.AutoSize = true;
            this.lbDreamDescryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamDescryption.Location = new System.Drawing.Point(254, 55);
            this.lbDreamDescryption.Name = "lbDreamDescryption";
            this.lbDreamDescryption.Size = new System.Drawing.Size(78, 17);
            this.lbDreamDescryption.TabIndex = 73;
            this.lbDreamDescryption.Text = "Описание ";
            // 
            // txtDescryption
            // 
            this.txtDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtDescryption.Location = new System.Drawing.Point(257, 75);
            this.txtDescryption.Multiline = true;
            this.txtDescryption.Name = "txtDescryption";
            this.txtDescryption.Size = new System.Drawing.Size(535, 257);
            this.txtDescryption.TabIndex = 72;
            // 
            // gbPriorities
            // 
            this.gbPriorities.Controls.Add(this.rbtnTypeA);
            this.gbPriorities.Controls.Add(this.rbtnTypeD);
            this.gbPriorities.Controls.Add(this.rbtnTypeB);
            this.gbPriorities.Controls.Add(this.rbtnTypeC);
            this.gbPriorities.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbPriorities.Location = new System.Drawing.Point(19, 115);
            this.gbPriorities.Name = "gbPriorities";
            this.gbPriorities.Size = new System.Drawing.Size(232, 113);
            this.gbPriorities.TabIndex = 71;
            this.gbPriorities.TabStop = false;
            this.gbPriorities.Text = "Приоритеты";
            // 
            // rbtnTypeA
            // 
            this.rbtnTypeA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtnTypeA.AutoSize = true;
            this.rbtnTypeA.Location = new System.Drawing.Point(6, 20);
            this.rbtnTypeA.Name = "rbtnTypeA";
            this.rbtnTypeA.Size = new System.Drawing.Size(182, 21);
            this.rbtnTypeA.TabIndex = 9;
            this.rbtnTypeA.TabStop = true;
            this.rbtnTypeA.Text = "(A)  Важные и срочные ";
            this.rbtnTypeA.UseVisualStyleBackColor = true;
            // 
            // rbtnTypeD
            // 
            this.rbtnTypeD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtnTypeD.AutoSize = true;
            this.rbtnTypeD.Location = new System.Drawing.Point(6, 91);
            this.rbtnTypeD.Name = "rbtnTypeD";
            this.rbtnTypeD.Size = new System.Drawing.Size(219, 21);
            this.rbtnTypeD.TabIndex = 12;
            this.rbtnTypeD.TabStop = true;
            this.rbtnTypeD.Text = "(D) Не важные и не срочные ";
            this.rbtnTypeD.UseVisualStyleBackColor = true;
            // 
            // rbtnTypeB
            // 
            this.rbtnTypeB.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtnTypeB.AutoSize = true;
            this.rbtnTypeB.Location = new System.Drawing.Point(6, 43);
            this.rbtnTypeB.Name = "rbtnTypeB";
            this.rbtnTypeB.Size = new System.Drawing.Size(186, 21);
            this.rbtnTypeB.TabIndex = 10;
            this.rbtnTypeB.TabStop = true;
            this.rbtnTypeB.Text = "(B) Важные не срочные ";
            this.rbtnTypeB.UseVisualStyleBackColor = true;
            // 
            // rbtnTypeC
            // 
            this.rbtnTypeC.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rbtnTypeC.AutoSize = true;
            this.rbtnTypeC.Location = new System.Drawing.Point(6, 67);
            this.rbtnTypeC.Name = "rbtnTypeC";
            this.rbtnTypeC.Size = new System.Drawing.Size(206, 21);
            this.rbtnTypeC.TabIndex = 11;
            this.rbtnTypeC.TabStop = true;
            this.rbtnTypeC.Text = "(C) Не важные, но срочные";
            this.rbtnTypeC.UseVisualStyleBackColor = true;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtName.Location = new System.Drawing.Point(15, 29);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(777, 23);
            this.txtName.TabIndex = 70;
            // 
            // lbDreamName
            // 
            this.lbDreamName.AutoSize = true;
            this.lbDreamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamName.Location = new System.Drawing.Point(22, 9);
            this.lbDreamName.Name = "lbDreamName";
            this.lbDreamName.Size = new System.Drawing.Size(72, 17);
            this.lbDreamName.TabIndex = 69;
            this.lbDreamName.Text = "Название";
            // 
            // btnCreateInterest
            // 
            this.btnCreateInterest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreateInterest.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCreateInterest.Location = new System.Drawing.Point(401, 347);
            this.btnCreateInterest.Name = "btnCreateInterest";
            this.btnCreateInterest.Size = new System.Drawing.Size(191, 31);
            this.btnCreateInterest.TabIndex = 67;
            this.btnCreateInterest.Text = "Сохранить";
            this.btnCreateInterest.UseVisualStyleBackColor = true;
            this.btnCreateInterest.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEscapeInterest
            // 
            this.btnEscapeInterest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEscapeInterest.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEscapeInterest.Location = new System.Drawing.Point(601, 347);
            this.btnEscapeInterest.Name = "btnEscapeInterest";
            this.btnEscapeInterest.Size = new System.Drawing.Size(191, 31);
            this.btnEscapeInterest.TabIndex = 68;
            this.btnEscapeInterest.Text = "Отмена";
            this.btnEscapeInterest.UseVisualStyleBackColor = true;
            this.btnEscapeInterest.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmInterest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(807, 390);
            this.Controls.Add(this.cmbPrinciples);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbCreateDate);
            this.Controls.Add(this.dtStartDate);
            this.Controls.Add(this.lbDreamDescryption);
            this.Controls.Add(this.txtDescryption);
            this.Controls.Add(this.gbPriorities);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lbDreamName);
            this.Controls.Add(this.btnCreateInterest);
            this.Controls.Add(this.btnEscapeInterest);
            this.MinimumSize = new System.Drawing.Size(823, 429);
            this.Name = "frmInterest";
            this.Text = "Интерес";
            this.Load += new System.EventHandler(this.frmInterest_Load);
            this.gbPriorities.ResumeLayout(false);
            this.gbPriorities.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbPrinciples;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbCreateDate;
        private System.Windows.Forms.DateTimePicker dtStartDate;
        private System.Windows.Forms.Label lbDreamDescryption;
        private System.Windows.Forms.TextBox txtDescryption;
        private System.Windows.Forms.GroupBox gbPriorities;
        private System.Windows.Forms.RadioButton rbtnTypeA;
        private System.Windows.Forms.RadioButton rbtnTypeD;
        private System.Windows.Forms.RadioButton rbtnTypeB;
        private System.Windows.Forms.RadioButton rbtnTypeC;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lbDreamName;
        private System.Windows.Forms.Button btnCreateInterest;
        private System.Windows.Forms.Button btnEscapeInterest;
    }
}
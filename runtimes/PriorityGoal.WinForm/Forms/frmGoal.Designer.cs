﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmGoal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtName = new System.Windows.Forms.TextBox();
            this.lbDreamName = new System.Windows.Forms.Label();
            this.cmbDreams = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbCreateDate = new System.Windows.Forms.Label();
            this.dpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lbDreamDescryption = new System.Windows.Forms.Label();
            this.txtDescryption = new System.Windows.Forms.TextBox();
            this.gbGoals = new System.Windows.Forms.GroupBox();
            this.rdBtnTypeADream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeDDream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeBDream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeCDream = new System.Windows.Forms.RadioButton();
            this.btnEscape = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbGoals.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Location = new System.Drawing.Point(9, 29);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(779, 20);
            this.txtName.TabIndex = 77;
            // 
            // lbDreamName
            // 
            this.lbDreamName.AutoSize = true;
            this.lbDreamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamName.Location = new System.Drawing.Point(12, 9);
            this.lbDreamName.Name = "lbDreamName";
            this.lbDreamName.Size = new System.Drawing.Size(72, 17);
            this.lbDreamName.TabIndex = 76;
            this.lbDreamName.Text = "Название";
            // 
            // cmbDreams
            // 
            this.cmbDreams.DisplayMember = "Name";
            this.cmbDreams.FormattingEnabled = true;
            this.cmbDreams.Location = new System.Drawing.Point(9, 72);
            this.cmbDreams.Name = "cmbDreams";
            this.cmbDreams.Size = new System.Drawing.Size(232, 21);
            this.cmbDreams.TabIndex = 75;
            this.cmbDreams.ValueMember = "Id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(15, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 17);
            this.label4.TabIndex = 74;
            this.label4.Text = "Мечты";
            // 
            // lbCreateDate
            // 
            this.lbCreateDate.AutoSize = true;
            this.lbCreateDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCreateDate.Location = new System.Drawing.Point(14, 242);
            this.lbCreateDate.Name = "lbCreateDate";
            this.lbCreateDate.Size = new System.Drawing.Size(124, 20);
            this.lbCreateDate.TabIndex = 73;
            this.lbCreateDate.Text = "Дата создания";
            // 
            // dpStartDate
            // 
            this.dpStartDate.Location = new System.Drawing.Point(18, 276);
            this.dpStartDate.Name = "dpStartDate";
            this.dpStartDate.Size = new System.Drawing.Size(200, 20);
            this.dpStartDate.TabIndex = 72;
            // 
            // lbDreamDescryption
            // 
            this.lbDreamDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDreamDescryption.AutoSize = true;
            this.lbDreamDescryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamDescryption.Location = new System.Drawing.Point(247, 52);
            this.lbDreamDescryption.Name = "lbDreamDescryption";
            this.lbDreamDescryption.Size = new System.Drawing.Size(78, 17);
            this.lbDreamDescryption.TabIndex = 71;
            this.lbDreamDescryption.Text = "Описание ";
            // 
            // txtDescryption
            // 
            this.txtDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescryption.Location = new System.Drawing.Point(250, 72);
            this.txtDescryption.Multiline = true;
            this.txtDescryption.Name = "txtDescryption";
            this.txtDescryption.Size = new System.Drawing.Size(538, 269);
            this.txtDescryption.TabIndex = 70;
            // 
            // gbGoals
            // 
            this.gbGoals.Controls.Add(this.rdBtnTypeADream);
            this.gbGoals.Controls.Add(this.rdBtnTypeDDream);
            this.gbGoals.Controls.Add(this.rdBtnTypeBDream);
            this.gbGoals.Controls.Add(this.rdBtnTypeCDream);
            this.gbGoals.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbGoals.Location = new System.Drawing.Point(12, 112);
            this.gbGoals.Name = "gbGoals";
            this.gbGoals.Size = new System.Drawing.Size(232, 113);
            this.gbGoals.TabIndex = 69;
            this.gbGoals.TabStop = false;
            this.gbGoals.Text = "Приоритет";
            // 
            // rdBtnTypeADream
            // 
            this.rdBtnTypeADream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeADream.AutoSize = true;
            this.rdBtnTypeADream.Location = new System.Drawing.Point(6, 19);
            this.rdBtnTypeADream.Name = "rdBtnTypeADream";
            this.rdBtnTypeADream.Size = new System.Drawing.Size(182, 21);
            this.rdBtnTypeADream.TabIndex = 9;
            this.rdBtnTypeADream.TabStop = true;
            this.rdBtnTypeADream.Text = "(A)  Важные и срочные ";
            this.rdBtnTypeADream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeDDream
            // 
            this.rdBtnTypeDDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeDDream.AutoSize = true;
            this.rdBtnTypeDDream.Location = new System.Drawing.Point(6, 91);
            this.rdBtnTypeDDream.Name = "rdBtnTypeDDream";
            this.rdBtnTypeDDream.Size = new System.Drawing.Size(219, 21);
            this.rdBtnTypeDDream.TabIndex = 12;
            this.rdBtnTypeDDream.TabStop = true;
            this.rdBtnTypeDDream.Text = "(D) Не важные и не срочные ";
            this.rdBtnTypeDDream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeBDream
            // 
            this.rdBtnTypeBDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeBDream.AutoSize = true;
            this.rdBtnTypeBDream.Location = new System.Drawing.Point(6, 43);
            this.rdBtnTypeBDream.Name = "rdBtnTypeBDream";
            this.rdBtnTypeBDream.Size = new System.Drawing.Size(186, 21);
            this.rdBtnTypeBDream.TabIndex = 10;
            this.rdBtnTypeBDream.TabStop = true;
            this.rdBtnTypeBDream.Text = "(B) Важные не срочные ";
            this.rdBtnTypeBDream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeCDream
            // 
            this.rdBtnTypeCDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeCDream.AutoSize = true;
            this.rdBtnTypeCDream.Location = new System.Drawing.Point(6, 67);
            this.rdBtnTypeCDream.Name = "rdBtnTypeCDream";
            this.rdBtnTypeCDream.Size = new System.Drawing.Size(206, 21);
            this.rdBtnTypeCDream.TabIndex = 11;
            this.rdBtnTypeCDream.TabStop = true;
            this.rdBtnTypeCDream.Text = "(C) Не важные, но срочные";
            this.rdBtnTypeCDream.UseVisualStyleBackColor = true;
            // 
            // btnEscape
            // 
            this.btnEscape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEscape.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEscape.Location = new System.Drawing.Point(592, 347);
            this.btnEscape.Name = "btnEscape";
            this.btnEscape.Size = new System.Drawing.Size(191, 31);
            this.btnEscape.TabIndex = 68;
            this.btnEscape.Text = "Отмена";
            this.btnEscape.UseVisualStyleBackColor = true;
            this.btnEscape.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(395, 347);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(191, 31);
            this.btnSave.TabIndex = 67;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // frmGoal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(807, 390);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lbDreamName);
            this.Controls.Add(this.cmbDreams);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbCreateDate);
            this.Controls.Add(this.dpStartDate);
            this.Controls.Add(this.lbDreamDescryption);
            this.Controls.Add(this.txtDescryption);
            this.Controls.Add(this.gbGoals);
            this.Controls.Add(this.btnEscape);
            this.Controls.Add(this.btnSave);
            this.MinimumSize = new System.Drawing.Size(823, 429);
            this.Name = "frmGoal";
            this.Text = "Цель";
            this.Load += new System.EventHandler(this.frmGoal_Load);
            this.gbGoals.ResumeLayout(false);
            this.gbGoals.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lbDreamName;
        private System.Windows.Forms.ComboBox cmbDreams;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbCreateDate;
        private System.Windows.Forms.DateTimePicker dpStartDate;
        private System.Windows.Forms.Label lbDreamDescryption;
        private System.Windows.Forms.TextBox txtDescryption;
        private System.Windows.Forms.GroupBox gbGoals;
        private System.Windows.Forms.RadioButton rdBtnTypeADream;
        private System.Windows.Forms.RadioButton rdBtnTypeDDream;
        private System.Windows.Forms.RadioButton rdBtnTypeBDream;
        private System.Windows.Forms.RadioButton rdBtnTypeCDream;
        private System.Windows.Forms.Button btnEscape;
        private System.Windows.Forms.Button btnSave;
    }
}
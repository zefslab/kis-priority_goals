﻿using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;

    using PriorityGoals.Service.Service;

    public partial class frmPlanCreate : Form
    {
        public ServiceFactory ServiceFactory;

        public PlanDetailDpo Plan { get; set; }

        private IEnumerable<GoalDpo> _goals;

        
        
        public frmPlanCreate(IServiceFactory factory)
        {
            _goals = factory.GetService<Goal>().Get<GoalDpo>(null).ToList();
            InitializeComponent();
           
        }
        

        private void frmPlan_Load(object sender, System.EventArgs e)
        {
            this.cmbGoals.DataSource = null;
            this.cmbGoals.DataSource = this._goals;

            var statesList = StateListProvider.GetList();
            this.cmbPlanState.DataSource = null;
            this.cmbPlanState.DataSource = statesList;

            this.txtName.Text = this.Plan.Name;
            this.txtDescryption.Text = this.Plan.Description;
            this.dpStartDate.Value = this.Plan.BeginDate;
            if (this.Plan.EndDate != null)
            {
                this.dpEndDate.Value = (DateTime)Plan.EndDate;
            }
            this.cmbPlanState.SelectedValue = this.Plan.State;
            this.cmbGoals.SelectedValue = this.Plan.GoalId;
            this.gbPriority.SetPriotityRadioButton(this.Plan.Priority);

        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            this.Plan.Name = this.txtName.Text;
            this.Plan.Description = this.txtDescryption.Text;
            this.Plan.BeginDate = this.dpStartDate.Value.Date;
            this.Plan.EndDate = this.dpEndDate.Value.Date;
            this.Plan.State = (States)this.cmbPlanState.SelectedValue;
            if (this.cmbGoals.SelectedValue != null)
            {
                this.Plan.GoalId = Guid.Parse(this.cmbGoals.SelectedValue.ToString());
            }
            else
            {
                MessageBox.Show(
                    "Нет привязки плана к цели. Вернитесь к форме создания плана и укажите цель",
                    "Отсутствует привязка",
                    MessageBoxButtons.OK);
                return;
            }
            this.Plan.Priority = this.gbPriority.GetPriorityFromRadioButton();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnEscape_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}

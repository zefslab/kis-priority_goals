﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace PriorityGoal.WinForm.Forms
{


    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Service;

    public partial class frmTaskList : Form
    {
        TaskService service = new TaskService();

        GoalService goalService = new GoalService();

        PlanService planService = new PlanService();

        TaskFilter filter = new TaskFilter();

        private List<TaskDpo> tasks; 

        /// <summary>
        /// признак того, что форма загружается в первый раз
        /// поэтому открывается пустое окно (не применяются фильтры).
        /// </summary>
        private bool IsFormNotLoaded = true;

        public frmTaskList()
        {
            InitializeComponent();

            tasks = new List<TaskDpo>();
        }

        private void frmTask_Load(object sender, EventArgs e)
        {
            gridTasks.AutoGenerateColumns = false;
            
            #region Настройка элементов фильтра
            var goals = goalService.Get(new GoalFilter()).ToList();
            goals.Add(new GoalDpo {Name = "", Id = Guid.Empty});
            cmbGoals.DataSource = goals.OrderBy(g=> g.Id).ToList();

            cmbPlans.DisplayMember = "Name";
            cmbPlans.ValueMember = "Id";

            var plans = planService.Get(new PlanFilter()).ToList();
            plans.Add(new PlanDpo { Name = "", Id = Guid.Empty });
            cmbPlans.DataSource = plans.OrderBy(p => p.Id).ToList();
            #endregion

            IsFormNotLoaded = false;
        }
        /// <summary>
        /// Меняются настройка фильтра со списком планов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbPlans_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (IsFormNotLoaded) return;

            FilterApply();
        }

        /// <summary>
        /// Применяются настройки фильтров при перезагрузки списка задач в гриде
        /// Создал ЗЕФ 2016.04.12
        /// </summary>
        private void FilterApply()
        {
            filter.PlanId = cmbPlans.SelectedValue as Guid?;
            filter.GoalId = cmbGoals.SelectedValue as Guid?;
            
            /// Изменил ИЭФ 2016.08.34 Добавил условие проверки на точность
            tasks = service.Get(filter).ToList();

            gridTasks.DataSource = null;
            gridTasks.DataSource = tasks;
        }

        private void cmbGoals_SelectedIndexChanged(object sender, EventArgs e)
        {
            FilterApply();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridTasks_SelectionChanged(object sender, EventArgs e)
        {
            var task = this.getSelectedTask();
            if (task != null)
            {
                txtDescryption.Text = task.Description;
            }
        }

        private TaskDpo getSelectedTask()
        {
            object sender;

            if (gridTasks.SelectedRows.Count == 0)
            {
                return null;
            }

            var id = (Guid)gridTasks.SelectedRows[0].Cells["colId"].Value;

            var task = tasks.FirstOrDefault(t => t.Id == id);

            return task;
        }

        private void btnCreateTask_Click(object sender, EventArgs e)
        {
            var form = new frmTask();
            //В свойство формы передаем новый объект
            form.Task = new TaskDpo()
                {
                  StartDate = DateTime.Now
                };

            var answer = form.ShowDialog();

            if (answer == DialogResult.OK )
            {
                //Сохранить данные в БД
                var task = service.Add(form.Task);
                //Обновить список в гриде
                if (task != null && task.Id != Guid.Empty)
                {
                    tasks.Add(task);
                    gridTasks.DataSource = null;
                    gridTasks.DataSource = tasks;
                }
            }
            else
            {
                //Ничего не делаем
                Console.WriteLine("Пользователь отказался от сохранения");
            }
           
        }

        private void SaveAndRefreshGrid(TaskDpo task)
        {
           
        }

        private void btnModifiteTask_Click(object sender, EventArgs e)
        {
            var task = getSelectedTask();
            var form = new frmTask { Task = task };

            if (form.ShowDialog() == DialogResult.OK)
            {
                //Сохранить данные в БД
                service.Update(form.Task);
                
                //Обновить список в гриде
                FilterApply();
            }
        }

        private void btnDeleteTask_Click(object sender, EventArgs e)
        {
            var id = (Guid)gridTasks.SelectedRows[0]?.Cells["colId"].Value;
            if (service.Remove(id))
            {
                FilterApply();
            }
            else
            {
                MessageBox.Show("По какой-то причине не удалось удалить задачу");
            }
        }
    }
}

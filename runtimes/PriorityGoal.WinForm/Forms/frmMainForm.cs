﻿using System;
using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System.Collections;
    using System.Collections.Generic;

    using Microsoft.Practices.Unity;

    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Dpo.Interests;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    public partial class frmMainForm : Form
    {
        IServiceFactory serviceFactory = ServiceFactory.InitializeServiceFactory();

        [Dependency]
        public frmIssueList FrmIssueList { get; set; }

        public frmMainForm()
        {
            InitializeComponent();
        }
        private void frmMainForm_Load(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// Вызов окна GoalTask из основной формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTasks_Click(object sender, EventArgs e)
        {
            this.FrmIssueList.Show();
        }



        private void btnDreams_Click(object sender, EventArgs e)
        {
            var form = new frmDreamList(this.serviceFactory);

            form.Show();
        }

        private void btnGoals_Click(object sender, EventArgs e)
        {
                var form = new frmGoalList(serviceFactory);

                form.Show();
        }
       
        private void btnPlans_Click(object sender, EventArgs e)
        {
            var form = new frmPlanList(this.serviceFactory);

            form.Show();
        }

        private void btnPrinciples_Click(object sender, EventArgs e)
        {
            var form = new frmPrincipleList(this.serviceFactory);

            form.Show();
        }

        private void btnInterests_Click(object sender, EventArgs e)
        {
            var formInterest = new frmInterestList(this.serviceFactory.GetService<Interest>());
            formInterest.Show();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }

    public class FakeBaseService<TEntity, TDao> : ICrudService
    {
        IList list;

        public virtual TDpo Add<TDpo>(TDpo dpo) where TDpo : BaseDpo, new()
        {
            dpo.Id = Guid.Parse("070edd2b-9712-49d4-8372-60d6ee4615d4");
            
            return dpo;
        }

        public virtual TDpo Update<TDpo>(TDpo dpo) where TDpo : BaseDpo
        {
            return dpo;
        }

        public virtual  IEnumerable<TDpo> Get<TDpo>(BaseFilter filter) where TDpo : BaseDpo, new()
        {
             list = new TDpo[]
                           {
                               new TDpo()
                                   {
                                       Id = Guid.Parse("070edd2b-9712-49d4-8372-60d6ee4615d4"),
                                       Description = "dfsdfsdfs"
                                   },
                               new TDpo()
                                   {
                                       Id = Guid.Parse("070edd2b-9712-49d4-8372-60d6ee4615d4"),
                                       Description = "fdsdfsdf"
                                   },
                               new TDpo()
                                   {
                                       Id = Guid.Parse("070edd2b-9712-49d4-8372-60d6ee4615d4"),
                                       Description = "dfsdfsdfsdfsdfsdfsdf"
                                   }
                           };
            return list as IEnumerable<TDpo>;
        }

        public virtual TDpo Get<TDpo>(Guid id) where TDpo : BaseDpo, new()
        {
            return new TDpo();
        }

        public virtual CrudResult<bool> Remove(Guid id)
        {
            return new CrudResult<bool>() {Data = true};
        }
    }
    public class FakeCrudService : FakeBaseService<Interest, string>
    {
      
    }
   
}

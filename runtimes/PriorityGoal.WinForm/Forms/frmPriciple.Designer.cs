﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmPriciple
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lbDreamDescryption = new System.Windows.Forms.Label();
            this.txtPrincipleDescription = new System.Windows.Forms.TextBox();
            this.txtPrincipleName = new System.Windows.Forms.TextBox();
            this.lbDreamName = new System.Windows.Forms.Label();
            this.btnEscapeDream = new System.Windows.Forms.Button();
            this.btnEventCreateDream = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lbDreamDescryption);
            this.splitContainer1.Panel1.Controls.Add(this.txtPrincipleDescription);
            this.splitContainer1.Panel1.Controls.Add(this.txtPrincipleName);
            this.splitContainer1.Panel1.Controls.Add(this.lbDreamName);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnEscapeDream);
            this.splitContainer1.Panel2.Controls.Add(this.btnEventCreateDream);
            this.splitContainer1.Size = new System.Drawing.Size(807, 475);
            this.splitContainer1.SplitterDistance = 417;
            this.splitContainer1.TabIndex = 0;
            // 
            // lbDreamDescryption
            // 
            this.lbDreamDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDreamDescryption.AutoSize = true;
            this.lbDreamDescryption.Location = new System.Drawing.Point(3, 55);
            this.lbDreamDescryption.Name = "lbDreamDescryption";
            this.lbDreamDescryption.Size = new System.Drawing.Size(60, 13);
            this.lbDreamDescryption.TabIndex = 62;
            this.lbDreamDescryption.Text = "Описание ";
            // 
            // txtPrincipleDescription
            // 
            this.txtPrincipleDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrincipleDescription.Location = new System.Drawing.Point(6, 75);
            this.txtPrincipleDescription.Multiline = true;
            this.txtPrincipleDescription.Name = "txtPrincipleDescription";
            this.txtPrincipleDescription.Size = new System.Drawing.Size(789, 339);
            this.txtPrincipleDescription.TabIndex = 61;
            // 
            // txtPrincipleName
            // 
            this.txtPrincipleName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPrincipleName.Location = new System.Drawing.Point(6, 29);
            this.txtPrincipleName.Name = "txtPrincipleName";
            this.txtPrincipleName.Size = new System.Drawing.Size(792, 20);
            this.txtPrincipleName.TabIndex = 60;
            // 
            // lbDreamName
            // 
            this.lbDreamName.AutoSize = true;
            this.lbDreamName.Location = new System.Drawing.Point(3, 9);
            this.lbDreamName.Name = "lbDreamName";
            this.lbDreamName.Size = new System.Drawing.Size(57, 13);
            this.lbDreamName.TabIndex = 59;
            this.lbDreamName.Text = "Название";
            // 
            // btnEscapeDream
            // 
            this.btnEscapeDream.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEscapeDream.Location = new System.Drawing.Point(583, 10);
            this.btnEscapeDream.Name = "btnEscapeDream";
            this.btnEscapeDream.Size = new System.Drawing.Size(191, 31);
            this.btnEscapeDream.TabIndex = 23;
            this.btnEscapeDream.Text = "Отмена";
            this.btnEscapeDream.UseVisualStyleBackColor = true;
            this.btnEscapeDream.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnEventCreateDream
            // 
            this.btnEventCreateDream.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEventCreateDream.Location = new System.Drawing.Point(383, 11);
            this.btnEventCreateDream.Name = "btnEventCreateDream";
            this.btnEventCreateDream.Size = new System.Drawing.Size(191, 31);
            this.btnEventCreateDream.TabIndex = 22;
            this.btnEventCreateDream.Text = "Сохранить";
            this.btnEventCreateDream.UseVisualStyleBackColor = true;
            this.btnEventCreateDream.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // frmPriciple
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(807, 475);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(823, 429);
            this.Name = "frmPriciple";
            this.Text = "Принцип";
            this.Load += new System.EventHandler(this.frmPriciple_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnEscapeDream;
        private System.Windows.Forms.Button btnEventCreateDream;
        private System.Windows.Forms.Label lbDreamDescryption;
        private System.Windows.Forms.TextBox txtPrincipleDescription;
        private System.Windows.Forms.TextBox txtPrincipleName;
        private System.Windows.Forms.Label lbDreamName;
    }
}
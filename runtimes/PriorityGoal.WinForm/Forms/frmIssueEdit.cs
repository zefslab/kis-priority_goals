﻿using System;
using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System.Collections.Generic;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Dpo.Idea;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Extentions;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    public partial class frmIssueEdit : Form
    {
        public IssueDetailDpo Issue { get; set; }


        private IList<PlanDpo> _plans;

        private ICrudService _service;
        private ICrudService _ideaService;

        public frmIssueEdit(IServiceFactory factory)
        {
            InitializeComponent();
            _service = factory.GetService<Issue>();
            _ideaService = factory.GetService<Idea>();
            _plans = factory.GetService<Plan>().Get<PlanDpo>(null).ToList();
        }
        private void frmIssue_Load(object sender, EventArgs e)
        {
            this.grid.AutoGenerateColumns = false;
            this.IdeaGrid.AutoGenerateColumns = false;
            this.cmbPlans.SetDataSource(this._plans, this.Issue.PlanId);

            IList<KeyValuePair<int, string>> states = new List<KeyValuePair<int, string>>();
            var i = 1;
            foreach (States value in Enum.GetValues(typeof(States)))
            {
                states.Add(new KeyValuePair<int, string>(i, value.GetDisplayName()));
                i++;
            }

            this.cmbState.DataSource = states;




            this.txtIssueName.Text = Issue.Name;
            this.txtDescryption.Text = Issue.Description;
            this.dpStartDate.Value = Issue.StartDate;

            if (this.Issue.PlanId != null)
            {
                this.cmbPlans.SelectedValue = this.Issue.PlanId;
            }

            gbPriorities.SetPriotityRadioButton(this.Issue.Priority);

            this.GridRefresh();
            this.IdeaGridRefresh();
        }






        //private void btnGoalDialog_Click(object sender, EventArgs e)
        //{
        //    var frmGoal = new frmGoal();

        //    frmGoal.ShowDialog();
        //}
        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Issue.Name = this.txtIssueName.Text;
            this.Issue.Description = this.txtDescryption.Text;
            this.Issue.StartDate = this.dpStartDate.Value;
            this.Issue.EndDate = this.dtEndDate.Value;
            if (this.cmbPlans.SelectedValue != null)
            {
                this.Issue.PlanId = Guid.Parse(this.cmbPlans.SelectedValue.ToString());
            }
            this.Issue.Priority = gbPriorities.GetPriorityFromRadioButton();


            int stateInt;
            var parseSaccessful = int.TryParse(this.cmbState.SelectedValue.ToString(), out stateInt);
            if (parseSaccessful & new[] { 1, 2, 3, 4 }.Contains(stateInt))
            {
                this.Issue.State = (States)stateInt;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        /// <summary>
        /// Добавляется подзадача в задачу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmIssueCreate frm = new frmIssueCreate(ServiceFactory.InitializeServiceFactory());

            frm.Issue = new IssueDetailDpo
            {
                ParentId = Issue.Id,
                StartDate = DateTime.Now,
                PlanId = Issue.PlanId != Guid.Empty ? Issue.PlanId : null
            };
            if (frm.ShowDialog() == DialogResult.OK)
            {
                //Сохранить данные в БД
                var issue = this._service.Add(frm.Issue);
                //Обновить список в гриде
                if (issue != null && issue.Id != Guid.Empty)
                {
                    this.Issue.SubIssues.Add(issue);
                    this.GridRefresh();
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var issue = getSelectedIssue();
            if (issue == null)
            {
                MessageBox.Show(
                    "Не выбрано ни одной задачи",
                    "Ошибка выбора",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }
            var form = new frmIssueEdit(ServiceFactory.InitializeServiceFactory()) { Issue = issue };

            if (form.ShowDialog() == DialogResult.OK)
            {
                //Сохранить данные в БД
                this._service.Update(form.Issue);

                //Обновить список в гриде
                this.GridRefresh();
            }
        }
        private IssueDetailDpo getSelectedIssue()
        {
            if (grid.SelectedRows.Count == 0)
            {
                return null;
            }

            var id = (Guid)grid.SelectedRows[0].Cells["colId"].Value;

            var issue = this.Issue.SubIssues.FirstOrDefault(t => t.Id == id);

            return issue;
        }

        private void GridRefresh()
        {
            this.grid.DataSource = null;
            this.grid.DataSource = this.Issue.SubIssues.Select(x => new
            {
                x.Id,
                x.Name,
                x.StartDate,
                x.State,
                x.Priority
             }).ToList();
        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show(
                "Вы действительно желаете отменить изменения?",
                "Отмена изменений",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Warning))
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }


        }
        private void btnIdeaAdd_Click(object sender, EventArgs e)
        {
            frmIdea frm = new frmIdea(ServiceFactory.InitializeServiceFactory());

            frm.Idea = new IdeaDpo { IssueId = this.Issue.Id };

            if (frm.ShowDialog() == DialogResult.OK)
            {
                //Сохранить данные в БД
                var idea = this._ideaService.Add(frm.Idea);
                //Обновить список в гриде
                if (idea != null)
                {
                    this.Issue.Ideas.Add(idea);

                    this.IdeaGridRefresh();
                }
            }
        }

        private void IdeaGridRefresh()
        {
            this.IdeaGrid.DataSource = null;
            this.IdeaGrid.DataSource = Issue.Ideas.Select(x => new
            {
                x.Id,
                x.Name
            }).ToList(); ;

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.grid.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите элемент списка для удаления.");
                return;
            }

            if (DialogResult.Yes == MessageBox.Show("Действительно ли Вы желаете удалить задачу?", "Удаление объекта", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {

                var id = Guid.Parse(grid.SelectedRows[0].Cells["colId"].Value.ToString());
                if (_service.Remove(id).Data)
                {
                    var issue = this.Issue.SubIssues.FirstOrDefault(x => x.Id == id);
                    this.Issue.SubIssues.Remove(issue);
                    this.GridRefresh();
                }

            }
        }

        private void btnIdeaModifite_Click(object sender, EventArgs e)
        {
            if (this.IdeaGrid.SelectedRows.Count == 0)
            {
                MessageBox.Show(
                    "Не выбрано ни одной идеи",
                    "Ошибка выбора",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }
            var id = Guid.Parse(IdeaGrid.SelectedRows[0].Cells["colIdeaId"].Value.ToString());
            var idea = this.Issue.Ideas.FirstOrDefault(x => x.Id == id);
            var form = new frmIdea(ServiceFactory.InitializeServiceFactory()) { Idea = idea };

            if (form.ShowDialog() == DialogResult.OK)
            {
                //Сохранить данные в БД
                this._ideaService.Update(form.Idea);

                //Обновить список в гриде
                this.IdeaGridRefresh();
            }
        }

        private void btnIdeaDelete_Click(object sender, EventArgs e)
        {

            if (this.IdeaGrid.SelectedRows.Count == 0)
            {
                MessageBox.Show("Выберите элемент списка для удаления.");
                return;
            }

            if (DialogResult.Yes == MessageBox.Show("Действительно ли Вы желаете удалить идею?", "Удаление идеи", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
            {

                var id = Guid.Parse(IdeaGrid.SelectedRows[0].Cells["colIdeaId"].Value.ToString());
                if (this._ideaService.Remove(id).Data)
                {
                    var idea = this.Issue.Ideas.FirstOrDefault(x => x.Id == id);
                    this.Issue.Ideas.Remove(idea);
                    this.IdeaGridRefresh();
                }

            }
        }

        private void IdeaGrid_SelectionChanged(object sender, EventArgs e)
        {
            if (this.IdeaGrid.SelectedRows.Count == 0)
            {
                return;
            }
            Guid ideaId = Guid.Parse(this.IdeaGrid.SelectedRows[0].Cells["colIdeaId"].Value.ToString());
            var idea = this.Issue.Ideas.FirstOrDefault(x => x.Id == ideaId);

            this.txtIdeaName.Text = idea.Name;
            this.txtIdeaDescription.Text = idea.Description;

        }

        private void txtIdeaName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

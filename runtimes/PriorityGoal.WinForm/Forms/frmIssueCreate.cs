﻿using System;
using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System.Collections.Generic;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Extentions;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    public partial class frmIssueCreate : Form
    {
        public IssueDetailDpo Issue { get; set; }

        private IList<PlanDpo> _plans;

        

       

        public frmIssueCreate(IServiceFactory factory)
        {
            InitializeComponent();
            _plans = factory.GetService<Plan>().Get<PlanDpo>(null).ToList();
        }
        private void frmIssue_Load(object sender, EventArgs e)
        {

            this.cmbPlans.SetDataSource(this._plans, this.Issue.PlanId, needEmptyElement:false);
            
            IList<KeyValuePair<int, string>> states = new List<KeyValuePair<int, string>>();
            var i = 1;
            foreach (States value in Enum.GetValues(typeof(States)))
            {
                states.Add(new KeyValuePair<int, string>(i, value.GetDisplayName()));
                i++;
            }
            
             this.cmbState.DataSource = states;

             this.txtIssueName.Text = Issue.Name;
             this.txtDescryption.Text = Issue.Description;
             this.dpStartDate.Value = Issue.StartDate;
            if (Issue.EndDate != null)
            {
                this.dpEndDate.Value = (DateTime)Issue.EndDate;
            }
           

            gbPriorities.SetPriotityRadioButton(this.Issue.Priority);

        }




        private void btnCreate_Click(object sender, EventArgs e)
        {
            this.Issue.Name = this.txtIssueName.Text;
            this.Issue.Description = this.txtDescryption.Text;
            this.Issue.StartDate = this.dpStartDate.Value;
            this.Issue.EndDate = this.dpEndDate.Value;
            if (this.cmbPlans.SelectedValue == null)
            {
               var answer = MessageBox.Show(
                    "Не указан план в задаче. Уверены, что желаете создать задачу без привязки к плану?",
                    "Задача без плана",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);
                if (answer == DialogResult.No)
                {
                    return;    
                }
            }
            else
            {
                this.Issue.PlanId = Guid.Parse(this.cmbPlans.SelectedValue.ToString());
            }
            
            this.Issue.Priority = gbPriorities.GetPriorityFromRadioButton();
            int stateInt;
            var parseSaccessful = int.TryParse(this.cmbState.SelectedValue.ToString(),out stateInt);
            if (parseSaccessful & new []{1,2,3,4}.Contains(stateInt))
            {
                this.Issue.State = (States)stateInt;
            }

            this.DialogResult = DialogResult.OK;

            this.Close();
        }
      

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

            this.Close();
        }

        
    }
}

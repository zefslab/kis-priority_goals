﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmPlanEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.tabMain = new System.Windows.Forms.TabPage();
            this.cmbPlanState = new System.Windows.Forms.ComboBox();
            this.cmbGoals = new System.Windows.Forms.ComboBox();
            this.lbPlanState = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dpEndDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.lbCreateDate = new System.Windows.Forms.Label();
            this.dpStartDate = new System.Windows.Forms.DateTimePicker();
            this.lbDreamDescryption = new System.Windows.Forms.Label();
            this.txtDescryption = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.gbPriority = new System.Windows.Forms.GroupBox();
            this.rdBtnTypeADream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeDDream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeBDream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeCDream = new System.Windows.Forms.RadioButton();
            this.lbDreamName = new System.Windows.Forms.Label();
            this.tabIssues = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chkListBoxPriority = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbIssuesState = new System.Windows.Forms.ComboBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.gridIssues = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStartDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colFinishedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTaskStates = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDescryptionIssue = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabIdeas = new System.Windows.Forms.TabPage();
            this.Идеи = new System.Windows.Forms.Label();
            this.txtIdeaDescription = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtIdeaName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnIdeaDelete = new System.Windows.Forms.Button();
            this.IdeaGrid = new System.Windows.Forms.DataGridView();
            this.colIdeaId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIdeaName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnIdeaAdd = new System.Windows.Forms.Button();
            this.btnIdeaModifite = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEscape = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.TabControl.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.gbPriority.SuspendLayout();
            this.tabIssues.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridIssues)).BeginInit();
            this.tabIdeas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdeaGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.TabControl);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnSave);
            this.splitContainer1.Panel2.Controls.Add(this.btnEscape);
            this.splitContainer1.Size = new System.Drawing.Size(1232, 551);
            this.splitContainer1.SplitterDistance = 487;
            this.splitContainer1.TabIndex = 0;
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.tabMain);
            this.TabControl.Controls.Add(this.tabIssues);
            this.TabControl.Controls.Add(this.tabIdeas);
            this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl.Location = new System.Drawing.Point(0, 0);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(1232, 487);
            this.TabControl.TabIndex = 0;
            // 
            // tabMain
            // 
            this.tabMain.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabMain.Controls.Add(this.cmbPlanState);
            this.tabMain.Controls.Add(this.cmbGoals);
            this.tabMain.Controls.Add(this.lbPlanState);
            this.tabMain.Controls.Add(this.label4);
            this.tabMain.Controls.Add(this.dpEndDate);
            this.tabMain.Controls.Add(this.label10);
            this.tabMain.Controls.Add(this.lbCreateDate);
            this.tabMain.Controls.Add(this.dpStartDate);
            this.tabMain.Controls.Add(this.lbDreamDescryption);
            this.tabMain.Controls.Add(this.txtDescryption);
            this.tabMain.Controls.Add(this.txtName);
            this.tabMain.Controls.Add(this.gbPriority);
            this.tabMain.Controls.Add(this.lbDreamName);
            this.tabMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabMain.Location = new System.Drawing.Point(4, 22);
            this.tabMain.Name = "tabMain";
            this.tabMain.Padding = new System.Windows.Forms.Padding(3);
            this.tabMain.Size = new System.Drawing.Size(1224, 461);
            this.tabMain.TabIndex = 0;
            this.tabMain.Text = "Основные данные";
            // 
            // cmbPlanState
            // 
            this.cmbPlanState.DisplayMember = "Value";
            this.cmbPlanState.FormattingEnabled = true;
            this.cmbPlanState.Location = new System.Drawing.Point(14, 117);
            this.cmbPlanState.Name = "cmbPlanState";
            this.cmbPlanState.Size = new System.Drawing.Size(222, 24);
            this.cmbPlanState.TabIndex = 6;
            this.cmbPlanState.ValueMember = "Key";
            // 
            // cmbGoals
            // 
            this.cmbGoals.DisplayMember = "Name";
            this.cmbGoals.FormattingEnabled = true;
            this.cmbGoals.Location = new System.Drawing.Point(14, 71);
            this.cmbGoals.Name = "cmbGoals";
            this.cmbGoals.Size = new System.Drawing.Size(226, 24);
            this.cmbGoals.TabIndex = 57;
            this.cmbGoals.ValueMember = "Id";
            // 
            // lbPlanState
            // 
            this.lbPlanState.AutoSize = true;
            this.lbPlanState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbPlanState.Location = new System.Drawing.Point(13, 98);
            this.lbPlanState.Name = "lbPlanState";
            this.lbPlanState.Size = new System.Drawing.Size(121, 16);
            this.lbPlanState.TabIndex = 5;
            this.lbPlanState.Text = "Состояние плана";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(13, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 17);
            this.label4.TabIndex = 56;
            this.label4.Text = "Цели";
            // 
            // dpEndDate
            // 
            this.dpEndDate.Location = new System.Drawing.Point(12, 348);
            this.dpEndDate.Name = "dpEndDate";
            this.dpEndDate.Size = new System.Drawing.Size(200, 23);
            this.dpEndDate.TabIndex = 53;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(16, 325);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 20);
            this.label10.TabIndex = 52;
            this.label10.Text = "Дата окончания";
            // 
            // lbCreateDate
            // 
            this.lbCreateDate.AutoSize = true;
            this.lbCreateDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCreateDate.Location = new System.Drawing.Point(16, 276);
            this.lbCreateDate.Name = "lbCreateDate";
            this.lbCreateDate.Size = new System.Drawing.Size(124, 20);
            this.lbCreateDate.TabIndex = 51;
            this.lbCreateDate.Text = "Дата создания";
            // 
            // dpStartDate
            // 
            this.dpStartDate.Location = new System.Drawing.Point(14, 299);
            this.dpStartDate.Name = "dpStartDate";
            this.dpStartDate.Size = new System.Drawing.Size(200, 23);
            this.dpStartDate.TabIndex = 50;
            // 
            // lbDreamDescryption
            // 
            this.lbDreamDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDreamDescryption.AutoSize = true;
            this.lbDreamDescryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamDescryption.Location = new System.Drawing.Point(263, 51);
            this.lbDreamDescryption.Name = "lbDreamDescryption";
            this.lbDreamDescryption.Size = new System.Drawing.Size(78, 17);
            this.lbDreamDescryption.TabIndex = 49;
            this.lbDreamDescryption.Text = "Описание ";
            // 
            // txtDescryption
            // 
            this.txtDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescryption.Location = new System.Drawing.Point(251, 71);
            this.txtDescryption.Multiline = true;
            this.txtDescryption.Name = "txtDescryption";
            this.txtDescryption.Size = new System.Drawing.Size(965, 384);
            this.txtDescryption.TabIndex = 48;
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Location = new System.Drawing.Point(16, 25);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(1200, 23);
            this.txtName.TabIndex = 46;
            // 
            // gbPriority
            // 
            this.gbPriority.Controls.Add(this.rdBtnTypeADream);
            this.gbPriority.Controls.Add(this.rdBtnTypeDDream);
            this.gbPriority.Controls.Add(this.rdBtnTypeBDream);
            this.gbPriority.Controls.Add(this.rdBtnTypeCDream);
            this.gbPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbPriority.Location = new System.Drawing.Point(14, 148);
            this.gbPriority.Name = "gbPriority";
            this.gbPriority.Size = new System.Drawing.Size(229, 113);
            this.gbPriority.TabIndex = 47;
            this.gbPriority.TabStop = false;
            this.gbPriority.Text = "Приоритет";
            // 
            // rdBtnTypeADream
            // 
            this.rdBtnTypeADream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeADream.AutoSize = true;
            this.rdBtnTypeADream.Location = new System.Drawing.Point(6, 19);
            this.rdBtnTypeADream.Name = "rdBtnTypeADream";
            this.rdBtnTypeADream.Size = new System.Drawing.Size(182, 21);
            this.rdBtnTypeADream.TabIndex = 9;
            this.rdBtnTypeADream.TabStop = true;
            this.rdBtnTypeADream.Text = "(A)  Важные и срочные ";
            this.rdBtnTypeADream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeDDream
            // 
            this.rdBtnTypeDDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeDDream.AutoSize = true;
            this.rdBtnTypeDDream.Location = new System.Drawing.Point(6, 91);
            this.rdBtnTypeDDream.Name = "rdBtnTypeDDream";
            this.rdBtnTypeDDream.Size = new System.Drawing.Size(219, 21);
            this.rdBtnTypeDDream.TabIndex = 12;
            this.rdBtnTypeDDream.TabStop = true;
            this.rdBtnTypeDDream.Text = "(D) Не важные и не срочные ";
            this.rdBtnTypeDDream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeBDream
            // 
            this.rdBtnTypeBDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeBDream.AutoSize = true;
            this.rdBtnTypeBDream.Location = new System.Drawing.Point(6, 43);
            this.rdBtnTypeBDream.Name = "rdBtnTypeBDream";
            this.rdBtnTypeBDream.Size = new System.Drawing.Size(186, 21);
            this.rdBtnTypeBDream.TabIndex = 10;
            this.rdBtnTypeBDream.TabStop = true;
            this.rdBtnTypeBDream.Text = "(B) Важные не срочные ";
            this.rdBtnTypeBDream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeCDream
            // 
            this.rdBtnTypeCDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeCDream.AutoSize = true;
            this.rdBtnTypeCDream.Location = new System.Drawing.Point(6, 67);
            this.rdBtnTypeCDream.Name = "rdBtnTypeCDream";
            this.rdBtnTypeCDream.Size = new System.Drawing.Size(206, 21);
            this.rdBtnTypeCDream.TabIndex = 11;
            this.rdBtnTypeCDream.TabStop = true;
            this.rdBtnTypeCDream.Text = "(C) Не важные, но срочные";
            this.rdBtnTypeCDream.UseVisualStyleBackColor = true;
            // 
            // lbDreamName
            // 
            this.lbDreamName.AutoSize = true;
            this.lbDreamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamName.Location = new System.Drawing.Point(13, 5);
            this.lbDreamName.Name = "lbDreamName";
            this.lbDreamName.Size = new System.Drawing.Size(72, 17);
            this.lbDreamName.TabIndex = 45;
            this.lbDreamName.Text = "Название";
            // 
            // tabIssues
            // 
            this.tabIssues.Controls.Add(this.splitContainer2);
            this.tabIssues.Location = new System.Drawing.Point(4, 22);
            this.tabIssues.Name = "tabIssues";
            this.tabIssues.Padding = new System.Windows.Forms.Padding(3);
            this.tabIssues.Size = new System.Drawing.Size(1224, 461);
            this.tabIssues.TabIndex = 2;
            this.tabIssues.Text = "Задачи";
            this.tabIssues.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.chkListBoxPriority);
            this.splitContainer2.Panel1.Controls.Add(this.label3);
            this.splitContainer2.Panel1.Controls.Add(this.cmbIssuesState);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(1218, 455);
            this.splitContainer2.SplitterDistance = 258;
            this.splitContainer2.TabIndex = 0;
            // 
            // chkListBoxPriority
            // 
            this.chkListBoxPriority.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.chkListBoxPriority.CheckOnClick = true;
            this.chkListBoxPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chkListBoxPriority.FormattingEnabled = true;
            this.chkListBoxPriority.Items.AddRange(new object[] {
            "Важные и ...",
            "Важные не...",
            "Не важные, ...",
            "Не важные и не..."});
            this.chkListBoxPriority.Location = new System.Drawing.Point(9, 83);
            this.chkListBoxPriority.Name = "chkListBoxPriority";
            this.chkListBoxPriority.Size = new System.Drawing.Size(222, 76);
            this.chkListBoxPriority.TabIndex = 39;
            this.chkListBoxPriority.SelectedIndexChanged += new System.EventHandler(this.chkListBoxPriority_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(10, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 20);
            this.label3.TabIndex = 36;
            this.label3.Text = "Состояние задач";
            // 
            // cmbIssuesState
            // 
            this.cmbIssuesState.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbIssuesState.DisplayMember = "Value";
            this.cmbIssuesState.FormattingEnabled = true;
            this.cmbIssuesState.Location = new System.Drawing.Point(9, 39);
            this.cmbIssuesState.Name = "cmbIssuesState";
            this.cmbIssuesState.Size = new System.Drawing.Size(226, 21);
            this.cmbIssuesState.TabIndex = 35;
            this.cmbIssuesState.ValueMember = "Key";
            this.cmbIssuesState.SelectedIndexChanged += new System.EventHandler(this.cmbIssuesState_SelectedIndexChanged);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.gridIssues);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.txtDescryptionIssue);
            this.splitContainer3.Panel2.Controls.Add(this.label7);
            this.splitContainer3.Size = new System.Drawing.Size(956, 455);
            this.splitContainer3.SplitterDistance = 199;
            this.splitContainer3.TabIndex = 0;
            // 
            // gridIssues
            // 
            this.gridIssues.AllowUserToAddRows = false;
            this.gridIssues.AllowUserToDeleteRows = false;
            this.gridIssues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridIssues.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colName,
            this.colType,
            this.colStartDate,
            this.colFinishedDate,
            this.colTaskStates});
            this.gridIssues.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridIssues.Location = new System.Drawing.Point(0, 0);
            this.gridIssues.MultiSelect = false;
            this.gridIssues.Name = "gridIssues";
            this.gridIssues.ReadOnly = true;
            this.gridIssues.RowHeadersVisible = false;
            this.gridIssues.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridIssues.Size = new System.Drawing.Size(956, 199);
            this.gridIssues.TabIndex = 1;
            // 
            // colId
            // 
            this.colId.DataPropertyName = "Id";
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Visible = false;
            // 
            // colName
            // 
            this.colName.DataPropertyName = "Name";
            this.colName.FillWeight = 550F;
            this.colName.HeaderText = "Название";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            this.colName.Width = 550;
            // 
            // colType
            // 
            this.colType.DataPropertyName = "Priority";
            this.colType.HeaderText = "Приоритет";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            // 
            // colStartDate
            // 
            this.colStartDate.DataPropertyName = "StartDate";
            this.colStartDate.HeaderText = "Дата начала";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.ReadOnly = true;
            // 
            // colFinishedDate
            // 
            this.colFinishedDate.DataPropertyName = "EndDate";
            this.colFinishedDate.HeaderText = "Дата окончания";
            this.colFinishedDate.Name = "colFinishedDate";
            this.colFinishedDate.ReadOnly = true;
            // 
            // colTaskStates
            // 
            this.colTaskStates.DataPropertyName = "State";
            this.colTaskStates.HeaderText = "Состояние";
            this.colTaskStates.Name = "colTaskStates";
            this.colTaskStates.ReadOnly = true;
            this.colTaskStates.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // txtDescryptionIssue
            // 
            this.txtDescryptionIssue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescryptionIssue.Location = new System.Drawing.Point(7, 23);
            this.txtDescryptionIssue.Multiline = true;
            this.txtDescryptionIssue.Name = "txtDescryptionIssue";
            this.txtDescryptionIssue.ReadOnly = true;
            this.txtDescryptionIssue.Size = new System.Drawing.Size(944, 226);
            this.txtDescryptionIssue.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 20);
            this.label7.TabIndex = 24;
            this.label7.Text = "Описание";
            // 
            // tabIdeas
            // 
            this.tabIdeas.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabIdeas.Controls.Add(this.Идеи);
            this.tabIdeas.Controls.Add(this.txtIdeaDescription);
            this.tabIdeas.Controls.Add(this.label5);
            this.tabIdeas.Controls.Add(this.txtIdeaName);
            this.tabIdeas.Controls.Add(this.label6);
            this.tabIdeas.Controls.Add(this.btnIdeaDelete);
            this.tabIdeas.Controls.Add(this.IdeaGrid);
            this.tabIdeas.Controls.Add(this.btnIdeaAdd);
            this.tabIdeas.Controls.Add(this.btnIdeaModifite);
            this.tabIdeas.Location = new System.Drawing.Point(4, 22);
            this.tabIdeas.Name = "tabIdeas";
            this.tabIdeas.Padding = new System.Windows.Forms.Padding(3);
            this.tabIdeas.Size = new System.Drawing.Size(1224, 461);
            this.tabIdeas.TabIndex = 1;
            this.tabIdeas.Text = "Идеи";
            // 
            // Идеи
            // 
            this.Идеи.AutoSize = true;
            this.Идеи.Location = new System.Drawing.Point(465, 4);
            this.Идеи.Name = "Идеи";
            this.Идеи.Size = new System.Drawing.Size(33, 13);
            this.Идеи.TabIndex = 67;
            this.Идеи.Text = "Идеи";
            // 
            // txtIdeaDescription
            // 
            this.txtIdeaDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtIdeaDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtIdeaDescription.Location = new System.Drawing.Point(17, 69);
            this.txtIdeaDescription.Multiline = true;
            this.txtIdeaDescription.Name = "txtIdeaDescription";
            this.txtIdeaDescription.ReadOnly = true;
            this.txtIdeaDescription.Size = new System.Drawing.Size(432, 371);
            this.txtIdeaDescription.TabIndex = 63;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(13, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 17);
            this.label5.TabIndex = 62;
            this.label5.Text = "Описание";
            // 
            // txtIdeaName
            // 
            this.txtIdeaName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtIdeaName.Location = new System.Drawing.Point(17, 23);
            this.txtIdeaName.Name = "txtIdeaName";
            this.txtIdeaName.ReadOnly = true;
            this.txtIdeaName.Size = new System.Drawing.Size(433, 23);
            this.txtIdeaName.TabIndex = 61;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(13, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 17);
            this.label6.TabIndex = 60;
            this.label6.Text = "Название";
            // 
            // btnIdeaDelete
            // 
            this.btnIdeaDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIdeaDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIdeaDelete.Location = new System.Drawing.Point(1115, 102);
            this.btnIdeaDelete.Name = "btnIdeaDelete";
            this.btnIdeaDelete.Size = new System.Drawing.Size(102, 28);
            this.btnIdeaDelete.TabIndex = 65;
            this.btnIdeaDelete.Text = "Удалить";
            this.btnIdeaDelete.UseVisualStyleBackColor = true;
            this.btnIdeaDelete.Click += new System.EventHandler(this.btnIdeaDelete_Click);
            // 
            // IdeaGrid
            // 
            this.IdeaGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.IdeaGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.IdeaGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colIdeaId,
            this.colIdeaName});
            this.IdeaGrid.Location = new System.Drawing.Point(471, 27);
            this.IdeaGrid.MultiSelect = false;
            this.IdeaGrid.Name = "IdeaGrid";
            this.IdeaGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.IdeaGrid.Size = new System.Drawing.Size(638, 413);
            this.IdeaGrid.TabIndex = 59;
            this.IdeaGrid.SelectionChanged += new System.EventHandler(this.IdeaGrid_SelectionChanged);
            // 
            // colIdeaId
            // 
            this.colIdeaId.DataPropertyName = "Id";
            this.colIdeaId.HeaderText = "Id";
            this.colIdeaId.Name = "colIdeaId";
            this.colIdeaId.ReadOnly = true;
            this.colIdeaId.Visible = false;
            // 
            // colIdeaName
            // 
            this.colIdeaName.DataPropertyName = "Name";
            this.colIdeaName.FillWeight = 550F;
            this.colIdeaName.HeaderText = "Название";
            this.colIdeaName.Name = "colIdeaName";
            this.colIdeaName.ReadOnly = true;
            this.colIdeaName.Width = 550;
            // 
            // btnIdeaAdd
            // 
            this.btnIdeaAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIdeaAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIdeaAdd.Location = new System.Drawing.Point(1115, 26);
            this.btnIdeaAdd.Name = "btnIdeaAdd";
            this.btnIdeaAdd.Size = new System.Drawing.Size(102, 28);
            this.btnIdeaAdd.TabIndex = 64;
            this.btnIdeaAdd.Text = "Добавить";
            this.btnIdeaAdd.UseVisualStyleBackColor = true;
            this.btnIdeaAdd.Click += new System.EventHandler(this.btnIdeaAdd_Click);
            // 
            // btnIdeaModifite
            // 
            this.btnIdeaModifite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIdeaModifite.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnIdeaModifite.Location = new System.Drawing.Point(1115, 64);
            this.btnIdeaModifite.Name = "btnIdeaModifite";
            this.btnIdeaModifite.Size = new System.Drawing.Size(102, 28);
            this.btnIdeaModifite.TabIndex = 66;
            this.btnIdeaModifite.Text = "Изменить";
            this.btnIdeaModifite.UseVisualStyleBackColor = true;
            this.btnIdeaModifite.Click += new System.EventHandler(this.btnIdeaModifite_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(840, 26);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(191, 31);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEscape
            // 
            this.btnEscape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEscape.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEscape.Location = new System.Drawing.Point(1037, 26);
            this.btnEscape.Name = "btnEscape";
            this.btnEscape.Size = new System.Drawing.Size(191, 31);
            this.btnEscape.TabIndex = 23;
            this.btnEscape.Text = "Отмена";
            this.btnEscape.UseVisualStyleBackColor = true;
            this.btnEscape.Click += new System.EventHandler(this.btnEscape_Click);
            // 
            // frmPlanEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1232, 551);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(823, 429);
            this.Name = "frmPlanEdit";
            this.Text = "План";
            this.Load += new System.EventHandler(this.frmPlan_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.TabControl.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.tabMain.PerformLayout();
            this.gbPriority.ResumeLayout(false);
            this.gbPriority.PerformLayout();
            this.tabIssues.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridIssues)).EndInit();
            this.tabIdeas.ResumeLayout(false);
            this.tabIdeas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IdeaGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnEscape;
        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage tabMain;
        private System.Windows.Forms.ComboBox cmbPlanState;
        private System.Windows.Forms.ComboBox cmbGoals;
        private System.Windows.Forms.Label lbPlanState;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dpEndDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbCreateDate;
        private System.Windows.Forms.DateTimePicker dpStartDate;
        private System.Windows.Forms.Label lbDreamDescryption;
        private System.Windows.Forms.TextBox txtDescryption;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.GroupBox gbPriority;
        private System.Windows.Forms.RadioButton rdBtnTypeADream;
        private System.Windows.Forms.RadioButton rdBtnTypeDDream;
        private System.Windows.Forms.RadioButton rdBtnTypeBDream;
        private System.Windows.Forms.RadioButton rdBtnTypeCDream;
        private System.Windows.Forms.Label lbDreamName;
        private System.Windows.Forms.TabPage tabIdeas;
        private System.Windows.Forms.Label Идеи;
        private System.Windows.Forms.TextBox txtIdeaDescription;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtIdeaName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnIdeaDelete;
        private System.Windows.Forms.DataGridView IdeaGrid;
        private System.Windows.Forms.Button btnIdeaAdd;
        private System.Windows.Forms.Button btnIdeaModifite;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TabPage tabIssues;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.CheckedListBox chkListBoxPriority;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbIssuesState;
        private System.Windows.Forms.DataGridView gridIssues;
        private System.Windows.Forms.TextBox txtDescryptionIssue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStartDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colFinishedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTaskStates;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIdeaId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIdeaName;
    }
}
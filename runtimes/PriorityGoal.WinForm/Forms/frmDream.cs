﻿using System.Windows.Forms;

namespace PriorityGoal.WinForm.Forms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using PriorityGoal.WinForm.Helpers;

    using PriorityGoals.Api.Dpo.Interests;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;
    public partial class frmDream : Form
    { 
        public DreamDetailDpo Dream { get; set; }

        private IList<InterestDpo> _interests;

       

        public frmDream(IServiceFactory factory)
        {
            this._interests = factory.GetService<Interest>().Get<InterestDpo>(null).ToList();
            InitializeComponent();
        }

        private void frmDream_Load(object sender, System.EventArgs e)
        {
            this.cmbInterests.SetDataSource(_interests, this.Dream.InterestId);

            this.txtName.Text = this.Dream.Name;
            this.txtDescryption.Text = this.Dream.Description;
            this.dpStartDate.Value = this.Dream.DreamDate;
            gbDreams.SetPriotityRadioButton(Dream.Priority);
        }

        
        
        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Dream.Name = this.txtName.Text;
            this.Dream.Description = this.txtDescryption.Text;
            this.Dream.DreamDate = this.dpStartDate.Value;
            if (this.cmbInterests.SelectedValue != null)
            {
                var interestId = Guid.Parse(this.cmbInterests.SelectedValue.ToString());
                if (interestId != Guid.Empty)
                {
                    this.Dream.InterestId = interestId;
                }
                else
                {
                    this.Dream.InterestId = null;
                }
            }

            this.Dream.Priority = gbDreams.GetPriorityFromRadioButton();

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnEscapeDream_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}

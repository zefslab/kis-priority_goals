﻿namespace PriorityGoal.WinForm.Forms
{
    partial class frmPlanCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new System.Windows.Forms.Button();
            this.btnEscape = new System.Windows.Forms.Button();
            this.dpEndDate = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.lbCreateDate = new System.Windows.Forms.Label();
            this.dpStartDate = new System.Windows.Forms.DateTimePicker();
            this.txtDescryption = new System.Windows.Forms.TextBox();
            this.rdBtnTypeDDream = new System.Windows.Forms.RadioButton();
            this.cmbPlanState = new System.Windows.Forms.ComboBox();
            this.cmbGoals = new System.Windows.Forms.ComboBox();
            this.lbPlanState = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbDreamDescryption = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.gbPriority = new System.Windows.Forms.GroupBox();
            this.rdBtnTypeADream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeBDream = new System.Windows.Forms.RadioButton();
            this.rdBtnTypeCDream = new System.Windows.Forms.RadioButton();
            this.lbDreamName = new System.Windows.Forms.Label();
            this.gbPriority.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSave.Location = new System.Drawing.Point(394, 378);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(194, 31);
            this.btnSave.TabIndex = 72;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnEscape
            // 
            this.btnEscape.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEscape.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnEscape.Location = new System.Drawing.Point(594, 378);
            this.btnEscape.Name = "btnEscape";
            this.btnEscape.Size = new System.Drawing.Size(194, 31);
            this.btnEscape.TabIndex = 71;
            this.btnEscape.Text = "Отмена";
            this.btnEscape.UseVisualStyleBackColor = true;
            this.btnEscape.Click += new System.EventHandler(this.btnEscape_Click);
            // 
            // dpEndDate
            // 
            this.dpEndDate.Location = new System.Drawing.Point(13, 330);
            this.dpEndDate.Name = "dpEndDate";
            this.dpEndDate.Size = new System.Drawing.Size(200, 20);
            this.dpEndDate.TabIndex = 83;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(17, 307);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 20);
            this.label10.TabIndex = 82;
            this.label10.Text = "Дата окончания";
            // 
            // lbCreateDate
            // 
            this.lbCreateDate.AutoSize = true;
            this.lbCreateDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbCreateDate.Location = new System.Drawing.Point(17, 261);
            this.lbCreateDate.Name = "lbCreateDate";
            this.lbCreateDate.Size = new System.Drawing.Size(124, 20);
            this.lbCreateDate.TabIndex = 81;
            this.lbCreateDate.Text = "Дата создания";
            // 
            // dpStartDate
            // 
            this.dpStartDate.Location = new System.Drawing.Point(15, 284);
            this.dpStartDate.Name = "dpStartDate";
            this.dpStartDate.Size = new System.Drawing.Size(200, 20);
            this.dpStartDate.TabIndex = 80;
            // 
            // txtDescryption
            // 
            this.txtDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescryption.Location = new System.Drawing.Point(250, 72);
            this.txtDescryption.Multiline = true;
            this.txtDescryption.Name = "txtDescryption";
            this.txtDescryption.Size = new System.Drawing.Size(538, 289);
            this.txtDescryption.TabIndex = 78;
            // 
            // rdBtnTypeDDream
            // 
            this.rdBtnTypeDDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeDDream.AutoSize = true;
            this.rdBtnTypeDDream.Location = new System.Drawing.Point(6, 91);
            this.rdBtnTypeDDream.Name = "rdBtnTypeDDream";
            this.rdBtnTypeDDream.Size = new System.Drawing.Size(219, 21);
            this.rdBtnTypeDDream.TabIndex = 12;
            this.rdBtnTypeDDream.TabStop = true;
            this.rdBtnTypeDDream.Text = "(D) Не важные и не срочные ";
            this.rdBtnTypeDDream.UseVisualStyleBackColor = true;
            // 
            // cmbPlanState
            // 
            this.cmbPlanState.DisplayMember = "Value";
            this.cmbPlanState.FormattingEnabled = true;
            this.cmbPlanState.Location = new System.Drawing.Point(13, 113);
            this.cmbPlanState.Name = "cmbPlanState";
            this.cmbPlanState.Size = new System.Drawing.Size(222, 21);
            this.cmbPlanState.TabIndex = 74;
            this.cmbPlanState.ValueMember = "Key";
            // 
            // cmbGoals
            // 
            this.cmbGoals.DisplayMember = "Name";
            this.cmbGoals.FormattingEnabled = true;
            this.cmbGoals.Location = new System.Drawing.Point(13, 69);
            this.cmbGoals.Name = "cmbGoals";
            this.cmbGoals.Size = new System.Drawing.Size(226, 21);
            this.cmbGoals.TabIndex = 85;
            this.cmbGoals.ValueMember = "Id";
            // 
            // lbPlanState
            // 
            this.lbPlanState.AutoSize = true;
            this.lbPlanState.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbPlanState.Location = new System.Drawing.Point(12, 94);
            this.lbPlanState.Name = "lbPlanState";
            this.lbPlanState.Size = new System.Drawing.Size(121, 16);
            this.lbPlanState.TabIndex = 73;
            this.lbPlanState.Text = "Состояние плана";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 17);
            this.label4.TabIndex = 84;
            this.label4.Text = "Цели";
            // 
            // lbDreamDescryption
            // 
            this.lbDreamDescryption.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDreamDescryption.AutoSize = true;
            this.lbDreamDescryption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamDescryption.Location = new System.Drawing.Point(247, 52);
            this.lbDreamDescryption.Name = "lbDreamDescryption";
            this.lbDreamDescryption.Size = new System.Drawing.Size(78, 17);
            this.lbDreamDescryption.TabIndex = 79;
            this.lbDreamDescryption.Text = "Описание ";
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.Location = new System.Drawing.Point(15, 29);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(770, 20);
            this.txtName.TabIndex = 76;
            // 
            // gbPriority
            // 
            this.gbPriority.Controls.Add(this.rdBtnTypeADream);
            this.gbPriority.Controls.Add(this.rdBtnTypeDDream);
            this.gbPriority.Controls.Add(this.rdBtnTypeBDream);
            this.gbPriority.Controls.Add(this.rdBtnTypeCDream);
            this.gbPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gbPriority.Location = new System.Drawing.Point(13, 144);
            this.gbPriority.Name = "gbPriority";
            this.gbPriority.Size = new System.Drawing.Size(229, 113);
            this.gbPriority.TabIndex = 77;
            this.gbPriority.TabStop = false;
            this.gbPriority.Text = "Приоритет";
            // 
            // rdBtnTypeADream
            // 
            this.rdBtnTypeADream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeADream.AutoSize = true;
            this.rdBtnTypeADream.Location = new System.Drawing.Point(6, 19);
            this.rdBtnTypeADream.Name = "rdBtnTypeADream";
            this.rdBtnTypeADream.Size = new System.Drawing.Size(182, 21);
            this.rdBtnTypeADream.TabIndex = 9;
            this.rdBtnTypeADream.TabStop = true;
            this.rdBtnTypeADream.Text = "(A)  Важные и срочные ";
            this.rdBtnTypeADream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeBDream
            // 
            this.rdBtnTypeBDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeBDream.AutoSize = true;
            this.rdBtnTypeBDream.Location = new System.Drawing.Point(6, 43);
            this.rdBtnTypeBDream.Name = "rdBtnTypeBDream";
            this.rdBtnTypeBDream.Size = new System.Drawing.Size(186, 21);
            this.rdBtnTypeBDream.TabIndex = 10;
            this.rdBtnTypeBDream.TabStop = true;
            this.rdBtnTypeBDream.Text = "(B) Важные не срочные ";
            this.rdBtnTypeBDream.UseVisualStyleBackColor = true;
            // 
            // rdBtnTypeCDream
            // 
            this.rdBtnTypeCDream.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdBtnTypeCDream.AutoSize = true;
            this.rdBtnTypeCDream.Location = new System.Drawing.Point(6, 67);
            this.rdBtnTypeCDream.Name = "rdBtnTypeCDream";
            this.rdBtnTypeCDream.Size = new System.Drawing.Size(206, 21);
            this.rdBtnTypeCDream.TabIndex = 11;
            this.rdBtnTypeCDream.TabStop = true;
            this.rdBtnTypeCDream.Text = "(C) Не важные, но срочные";
            this.rdBtnTypeCDream.UseVisualStyleBackColor = true;
            // 
            // lbDreamName
            // 
            this.lbDreamName.AutoSize = true;
            this.lbDreamName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDreamName.Location = new System.Drawing.Point(12, 9);
            this.lbDreamName.Name = "lbDreamName";
            this.lbDreamName.Size = new System.Drawing.Size(72, 17);
            this.lbDreamName.TabIndex = 75;
            this.lbDreamName.Text = "Название";
            // 
            // frmPlanCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(807, 421);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnEscape);
            this.Controls.Add(this.dpEndDate);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lbCreateDate);
            this.Controls.Add(this.dpStartDate);
            this.Controls.Add(this.txtDescryption);
            this.Controls.Add(this.cmbPlanState);
            this.Controls.Add(this.cmbGoals);
            this.Controls.Add(this.lbPlanState);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbDreamDescryption);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.gbPriority);
            this.Controls.Add(this.lbDreamName);
            this.MinimumSize = new System.Drawing.Size(823, 446);
            this.Name = "frmPlanCreate";
            this.Text = "План";
            this.Load += new System.EventHandler(this.frmPlan_Load);
            this.gbPriority.ResumeLayout(false);
            this.gbPriority.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnEscape;
        private System.Windows.Forms.DateTimePicker dpEndDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbCreateDate;
        private System.Windows.Forms.DateTimePicker dpStartDate;
        private System.Windows.Forms.TextBox txtDescryption;
        private System.Windows.Forms.RadioButton rdBtnTypeDDream;
        private System.Windows.Forms.ComboBox cmbPlanState;
        private System.Windows.Forms.ComboBox cmbGoals;
        private System.Windows.Forms.Label lbPlanState;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbDreamDescryption;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.GroupBox gbPriority;
        private System.Windows.Forms.RadioButton rdBtnTypeADream;
        private System.Windows.Forms.RadioButton rdBtnTypeBDream;
        private System.Windows.Forms.RadioButton rdBtnTypeCDream;
        private System.Windows.Forms.Label lbDreamName;
    }
}
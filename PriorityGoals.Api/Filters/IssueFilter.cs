﻿using System;
using System.Collections.Generic;

namespace PriorityGoals.Api.Filters
{
    using PriorityGoals.Api.Constants;

    public class IssueFilter : BaseFilter
    {
       
        public Guid? PlanId { get; set; }

        public IList<int> Priorities { get; set; }

        public DateTime? DataBegin { get; set; }

        public DateTime? DataEnd { get; set; }

        public States? State { get; set; }

        public bool IsLoadSubissues { get; set; }
    }
}

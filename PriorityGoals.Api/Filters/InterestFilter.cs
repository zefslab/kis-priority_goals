﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Filters
{
   public class InterestFilter : BaseFilter
    {
        public Guid? PrincipleId { get; set; }
    }
}

﻿namespace PriorityGoals.Api.Filters
{
    using System;

    using PriorityGoals.Api.Constants;

    public class GoalFilter : BaseFilter
    {
       public Guid? DreamId { get; set; }

        public Priorities? Priority { get; set; }

        public DateTime? DateBegin { get; set; }

        public DateTime? DateEnd { get; set; }
    }
}

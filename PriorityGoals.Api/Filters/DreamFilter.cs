﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Filters
{
   public class DreamFilter : BaseFilter
    {
        public Guid? InterestId { get; set; }

        public IList<int> Priorities { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Filters
{
    using PriorityGoals.Api.Constants;

    public class TaskFilter : BaseFilter
    {
        public Guid? GoalId { get; set; }

        public Guid? PlanId { get; set; }

        public List<TaskStates> State { get; set; }


    }
}

﻿namespace PriorityGoals.Api.Filters
{
    using System;
    using System.Collections.Generic;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Extentions;

    public class PlanFilter : BaseFilter
    {
        public Guid? GoalId { get; set; }

        public IList<int> Priorities { get; set; }

        public DateTime? DataBegin { get; set; }

        public DateTime? DataEnd { get; set; }

        public States? State { get; set; }



    }
}

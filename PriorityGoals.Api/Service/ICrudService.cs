﻿using System;
using System.Collections.Generic;
using PriorityGoals.Api.Entity;

namespace PriorityGoals.Dao.DbRepositories
{
    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;

    public interface ICrudService
    {
        TDpo Add<TDpo>(TDpo dpo) 
            where TDpo : BaseDpo, new ();

        TDpo Update<TDpo>(TDpo dpo)
           where TDpo : BaseDpo;

        IEnumerable<TDpo> Get<TDpo>(BaseFilter filter)
            where TDpo : BaseDpo, new ();

        TDpo Get<TDpo>(Guid id) where TDpo : BaseDpo, new ();

        CrudResult<bool> Remove(Guid id);
    }
}

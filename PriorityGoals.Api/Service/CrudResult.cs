﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Service
{
    using PriorityGoals.Api.Constants;

    public class CrudResult<T>
    {
        public T Data { get; set; }

        public CrudResults Result { get; set; }

        public string Message { get; set; }

        public IDictionary<string, object> Params { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Service
{
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Dao.DbRepositories;

    /// <summary>
    /// Фабрика по изготовлению объектов сервисов crud операций
    /// </summary>
    public interface IServiceFactory
    {
        ICrudService GetService<TEntity>() where TEntity : BaseEntity;
    }
}

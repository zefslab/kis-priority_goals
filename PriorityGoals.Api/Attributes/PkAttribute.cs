﻿using System;

namespace PriorityGoals.Api.Attributes
{
    /// <summary>
    /// Отмечаются свойства являющиеся первичным ключом
    /// </summary>
    public class PkAttribute : Attribute
    {
    }
}

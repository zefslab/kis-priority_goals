﻿using System;

namespace PriorityGoals.Api.Attributes
{

    /// <summary>
    /// Отмечаются свойства являющиеся внешним ключом
    /// </summary>
    public class FkAttribute : Attribute
    {
    }
}

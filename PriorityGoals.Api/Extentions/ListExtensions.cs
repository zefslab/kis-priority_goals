﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PriorityGoals.Api.Extentions
{
    public static class ListExtensions
    {
        /// <summary>
        /// Если список null то возвращается пустой список
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> list)
        {
            if (list == null) return new Collection<T>();

            return list;
        }
    }
}

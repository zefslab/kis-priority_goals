﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Extentions
{
    using System.ComponentModel.DataAnnotations;

    public static class EnumExstensions
    {
        /// <summary>
        /// Получение из атрибута описания элемента списка
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDisplayName(this Enum value)
        {
            //На основе знаяения элемента перечисления получает тип перечисления
            var type = value.GetType();
            //Находим имя элемента перечисления
            var name = Enum.GetName(type, value);

            if (name == null)
            {
                return null;
            }
            //Получаем поле с найденныи именем
            var field = type.GetField(name);
            if (field == null)
            {
                return null;
            }
            //У поля ищется соответсвующий атирбут и возвращается значение его свойства Name
            var attr = Attribute.GetCustomAttribute(field, typeof(DisplayAttribute)) as DisplayAttribute;
            return attr != null ? attr.Name : null;
        }
    }
}

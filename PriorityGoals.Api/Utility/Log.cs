﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace PriorityGoals.Api.Utility
{
    public interface ILog
    {
        void Info(string message);

        void Error(string message, Exception ex);

        void Fatal(string message, Exception ex, params object[] param);
    }

    public class Log : ILog
    {
        ILogger logger;

        public Log(string name)
        {
            logger = LogManager.GetLogger(name);
        }

        public void Info(string message)
        {
            this.logger.Info(message);
        }

        public void Error(string message, Exception ex)
        {
            this.logger.Error(ex, message);
        }

        public void Fatal(string message, Exception ex, params object[] param)
        {
            this.logger.Fatal(ex, message);
        }
    }

    public class EnothLog : ILog
    {
        ILogger logger;

        public EnothLog(string name)
        {
            logger = LogManager.GetLogger(name);
        }
        public void Info(string message)
        {
            throw new NotImplementedException();
        }

        public void Error(string message, Exception ex)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message, Exception ex, params object[] param)
        {
            throw new NotImplementedException();
        }
    }
}

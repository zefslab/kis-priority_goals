﻿using PriorityGoals.Api.Entity;

namespace PriorityGoals.Api.Service
{
    using PriorityGoals.Api.Dpo;

    /// <summary>
    /// Инструментарий мапинга entiy < _ > dbo
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TDbo"></typeparam>
    public abstract class Mapper<TEntity, TDpo>
         where TEntity : BaseEntity, new()
         where TDpo : BaseDpo,  new()
    {
        /// <summary>
        /// Мапит entity в  dbo объект
        /// Используется в методе Create() базового репозитория
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="dbo"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual TDpo MapEntityToDpo(TEntity entity )
        {
            var dpo = new TDpo();

            return _mapEntityToDpo(entity, dpo);
        }

        private static TDpo _mapEntityToDpo(TEntity entity, TDpo dpo)
        {
            dpo.Id = entity.Id;
            dpo.Description = entity.Description;


            return dpo;
        }
        public virtual TEntity MapDpoToEntity(TDpo dpo)
        {
            var entity = new TEntity();
            entity.Id = dpo.Id;
            entity.Description = dpo.Description;
            

            return entity;
        }
        public virtual TEntity MapDpoToEntity(TDpo dpo, TEntity entity)
        {
            entity.Id = dpo.Id;
            entity.Description = dpo.Description;
            
            return entity;
        }



    }
}

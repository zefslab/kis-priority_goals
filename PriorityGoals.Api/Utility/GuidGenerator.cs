﻿using System;

namespace PriorityGoals.Api.Utility
{
    /// <summary>
    /// Генератор уникальных Guid значений в его приметивной реализации. Пока.
    /// </summary>
    public class GuidGenerator
    {
        public static Guid Get()
        {
            return Guid.NewGuid();
        }
    }
}

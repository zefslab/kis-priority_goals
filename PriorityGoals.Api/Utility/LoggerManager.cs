﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;


namespace PriorityGoals.Api.Utility
{
    public interface ILoggerManager
    {
        ILogger GetLogger(string name);
    }

    public class LoggerManager : ILoggerManager
    {
       private ILogger _logger;

        public ILogger GetLogger(string name)
       {
           if (_logger == null)
           {
               _logger = new MyLog(); 
           }

           return _logger;
       }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Entity
{


    /// <summary>
    /// Типы задач. Индекс соответсвует приоритету.
    /// </summary>
    public enum TaskTypes
        {
            A = 1,
            B = 2,
            C = 3,
            D = 4
        }
    
}

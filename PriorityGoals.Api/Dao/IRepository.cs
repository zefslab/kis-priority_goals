using System;
using System.Collections.Generic;
using PriorityGoals.Api.Constants;
using PriorityGoals.Api.Entity;

namespace PriorityGoals.Api.Dao
{
    using System.Linq;

    /// <summary>
    /// ��������� �������� �����������
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> 
        where TEntity : BaseEntity, new()
    {
        TEntity Create(TEntity entity);
        TEntity Read(Guid id);
        IQueryable<TEntity> Read(params SelectCriterion [] criterions);
        TEntity Update(TEntity entity);
        bool Delete(Guid id, bool isPermanently = false);
        bool Restore(Guid id);
        void SubmitChanges();
    }
}
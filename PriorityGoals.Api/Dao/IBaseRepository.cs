using System;
using PriorityGoals.Api.Constants;
using PriorityGoals.Api.Entity;

namespace PriorityGoals.Api.Dao
{
    using System.Linq;

    /// <summary>
    /// ��������� �������� �����������
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IBaseRepository<TEntity> 
        where TEntity : BaseEntity, new()
    {
        TEntity Create(TEntity entity, bool needSaveToDb = true);
        TEntity Read(Guid id);
        IQueryable<TEntity> Read(params SelectCriterion [] criterions);
        TEntity Update(TEntity entity);
        bool Delete(Guid id, bool isPermanently = false);
        bool Restore(Guid id);
        void SubmitChanges();
    }
}
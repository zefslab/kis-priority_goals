﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Dpo
{
    public class TaskDpo : BaseDpo
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }

        public DateTime StartDate { get; set; }
        
        public DateTime? FinishDate { get; set; }

    }
}

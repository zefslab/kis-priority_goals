﻿using System;

namespace PriorityGoals.Api.Dpo
{
    using System.Collections.Generic;

    using PriorityGoals.Api.Dpo.Idea;

    public class IssueDetailDpo : IssueDpo
    {
        public Guid? PlanId { get; set; }

        public Guid? ParentId { get; set; }

        public IList<IssueDetailDpo> SubIssues { get; set; }  

        public IList<IdeaDpo> Ideas { get; set; }
    }
}

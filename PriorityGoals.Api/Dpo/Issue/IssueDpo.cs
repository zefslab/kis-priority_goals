﻿using System;

namespace PriorityGoals.Api.Dpo
{
    using PriorityGoals.Api.Constants;

    public class IssueDpo : BaseDpo
    {
        public string Name { get; set; }

        public States State { get; set; }

        public Priorities Priority { get; set; }

        public DateTime? EndDate { get; set; }

        public DateTime StartDate { get; set; }


    }
}

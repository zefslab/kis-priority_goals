﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Dpo.Interests
{
    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo.Dream;

    public class GoalDetailDpo : GoalDpo
    {

        public Guid? DreamId { get; set; }

    }
}

﻿using System;

namespace PriorityGoals.Api.Dpo
{
    using PriorityGoals.Api.Constants;

    public class GoalDpo : BaseDpo
    {
        public string Name { get; set; }

        public DateTime GoalDate { get; set; }

        public Priorities Priority { get; set; }

    }
}

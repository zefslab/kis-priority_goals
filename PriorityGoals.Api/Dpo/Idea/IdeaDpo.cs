﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Dpo.Idea
{
    public class IdeaDpo : BaseDpo
    {
        public string Name { get; set; }

        public Guid? IssueId { get; set; }

        public Guid? PlanId { get; set; }

    }
}

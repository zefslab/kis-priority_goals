﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Dpo.Interests
{
    using PriorityGoals.Api.Constants;

    public class InterestDetailDpo : InterestDpo
    {

        public Guid? PrincipleId { get; set; }

    }
}

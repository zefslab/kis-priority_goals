﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Dpo.Interests
{
    using PriorityGoals.Api.Constants;

    public class InterestDpo : BaseDpo
    {
        public string Name { get; set; }

        public DateTime InterestDate { get; set; }

        public Priorities Priority { get; set; }
    }
}

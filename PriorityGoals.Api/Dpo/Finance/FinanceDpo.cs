﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Dpo.Finance
{
    public class FinanceDpo : BaseDpo
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public DateTime PaymentDay { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Dpo.Interests
{
    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo.Dream;

    public class DreamDetailDpo : DreamDpo
    {

        public Guid? InterestId { get; set; }

    }
}

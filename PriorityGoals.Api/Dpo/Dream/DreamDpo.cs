﻿using System;


namespace PriorityGoals.Api.Dpo.Dream
{
    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo.Interests;

    public class DreamDpo : BaseDpo
    {
        public string Name { get; set; }

        public DateTime DreamDate { get; set; } = DateTime.Now;

        public Priorities Priority { get; set; }

        
    }
}

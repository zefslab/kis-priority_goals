﻿namespace PriorityGoals.Api.Constants
{
    /// <summary>
    /// Критерии выбора записей
    /// </summary>
    public enum SelectCriterion
    {
        All = 0,

        WithoutDeleted = 1,

        OnlyDeleted = 2,

        LastWeek = 3

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Constants
{
    public enum CrudResults
    {
        Ok = 0,

        UnAccessibleDb = 1,

        CannotDelete,

        CannotUpdate,

        NotHaveRights
    }
}

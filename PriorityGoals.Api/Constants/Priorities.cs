﻿
namespace PriorityGoals.Api.Constants
{
    using System.ComponentModel;

    public enum Priorities
    {
        [Description("A")]
        A = 0,
        [Description("B")]
        B = 1,
        [Description("C")]
        C = 2,
        [Description("D")]
        D = 3


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Api.Constants
{
    /// <summary>
    /// Список состояния задачи. При создании нового плана автоматом переводим его в new, при завершении 
    /// переводим в finished
    /// </summary>
    public enum TaskStates
    {
        New = 1,

        InWork = 2,

        Paused = 3,

        Finished = 4
    }

    public struct MyStructure
    {
        
    }

}

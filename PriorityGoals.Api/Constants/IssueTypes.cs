﻿namespace PriorityGoals.Api.Entity
{


    /// <summary>
    /// Типы задач. Индекс соответсвует приоритету.
    /// </summary>
    public enum IssueTypes
        {
            A = 1,
            B = 2,
            C = 3,
            D = 4
        }
    
}

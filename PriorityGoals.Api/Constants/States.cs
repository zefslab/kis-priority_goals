﻿namespace PriorityGoals.Api.Constants
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    using PriorityGoals.Api.Extentions;

    /// <summary>
    /// Список состояния задачи. При создании нового плана автоматом переводим его в new, при завершении 
    /// переводим в finished
    /// </summary>
    public enum States
    {
        [Display(Name = "Новая")]
        New = 1,

        [Display(Name = "В работе")]
        InWork = 2,

        [Display(Name = "Временно приостановлена")]
        Paused = 3,

        [Display(Name = "Завершена")]
        Finished = 4
    }

    public static class StateListProvider
    {
        public static IList<KeyValuePair<States, string>> GetList()
        {
            IList<KeyValuePair<States, string>> resultList = new List<KeyValuePair<States, string>>();
                                                                
            var values = Enum.GetValues(typeof(States));
            foreach (States value in values)
            {
                resultList.Add(new KeyValuePair<States, string>(value, value.GetDisplayName()));
            }

            return resultList;
        }
    }
}

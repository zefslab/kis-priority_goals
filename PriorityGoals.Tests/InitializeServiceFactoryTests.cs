﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using PriorityGoals.Api.Entity;
using PriorityGoals.Service;
using PriorityGoals.Service.Service;

namespace PriorityGoals.Tests
{
    [TestFixture]
    public class InitializeServiceFactoryTests
    {
        [Test]
        public void CollectServicesDictionaty_Successfully()
        {
            var serviceFactory = ServiceFactory.InitializeServiceFactory();

            Assert.IsTrue(serviceFactory.GetService<Issue>() is IssueCrudService);
        }
    }
}

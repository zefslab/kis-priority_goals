﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Tests
{
    using NUnit.Framework;

    using PriorityGoal.WinForm.Forms;

    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service;
    /// <summary>
    /// Класс использовался для демонстрации подмены реальной фабрики на тестовую с генерацией
    /// фихтивных сервисов
    /// </summary>
    class TestFactory : IServiceFactory
    {
        public ICrudService GetService<TEntity>() where TEntity : BaseEntity
        {
            return new PlanService();
        }
    }

    [TestFixture]
    public class UITests
    {
        [Test]
        public void frmDreamList_Test()
        {
            frmDreamList frm = new frmDreamList(new TestFactory());
            frm.ShowDialog();
        }



    }
}

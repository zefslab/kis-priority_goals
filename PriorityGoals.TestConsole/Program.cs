﻿using System.Linq;
using PriorityGoals.Api.Entity;
using PriorityGoals.Service.Service;
using B = ConsoleApplication1.Bdfadfasdfadfasdfasdfasdfasdfasdfasdfasdf;

namespace ConsoleApplication1
{
    using System;
    using System.Data;
    using System.Runtime.InteropServices;

    using AutoMapper;

    using Moq;

    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Dao.Dao.EfRepositories;
    using PriorityGoals.Service;
    using static System.Console;
   



    internal class Program
    {
        private static void Main(string[] args)
        {
           new Used().AnyMethod(new Fasade(new AnothClass()));

            var planService = ServiceFactory.InitializeServiceFactory().GetService<Plan>();
            var plan = planService.Add(new PlanDetailDpo()
                        {
                            Id = Guid.NewGuid(),
                            Name = "Сходить в театр",
                            Description = "купить попкорн",
                            BeginDate = DateTime.Parse("12.05.2016")
                        });
            var plans = planService.Get<PlanDpo>(new PlanFilter());
            var goalService = new GoalService();
            var goals = goalService.Get<GoalDpo>(null);
        }

        private static void TestA(A aaa)
        {
            if (aaa == null)
            {
                throw new ArgumentNullException(nameof(aaa));
            }
            else
            {
                throw new NullReferenceException();
            }

            WriteLine($"Имя {aaa.Name} возраст {aaa.b?.Old} ");
        }
    }

    class A
    {
        public string Name { get; set; }

        public B b;

    }

    class Bdfadfasdfadfasdfasdfasdfasdfasdfasdfasdf
    {
        public int Old;
    }

    interface IMyInterface
    {
        string DoAnyThink();

    }
    class Used
    {
        public void AnyMethod(IMyInterface d)
        {
            d.DoAnyThink();
        }
    }

    class AnothClass
    {
        public string DoAnyThink(int i)
        {
            return i.ToString();
        }
        public int Do()
        {
            return 6;
        }

    }

    class Fasade : IMyInterface
    {
        AnothClass anothe;
        public Fasade(AnothClass anothe)
        {
            this.anothe = anothe;
        }

        public string DoAnyThink()
        {
           return anothe.DoAnyThink(6);
        }
    }
}

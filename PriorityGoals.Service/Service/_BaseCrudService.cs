﻿using System;
using System.Collections.Generic;
using System.Linq;
using PriorityGoals.Api.Dao;
using PriorityGoals.Api.Entity;
using AutoMapper;
using PriorityGoals.Api.Constants;
using PriorityGoals.Api.Dpo;
using PriorityGoals.Api.Filters;
using PriorityGoals.Api.Service;


namespace PriorityGoals.Dao.DbRepositories

{ 
    public abstract class BaseCrudService<TEntity, TDao> : ICrudService
        where TEntity : BaseEntity, new()
        where TDao : IBaseRepository<TEntity>, new()
    {
        protected IBaseRepository<TEntity> repo  = new TDao();
        
        public virtual TDetailDpo Add<TDetailDpo>(TDetailDpo dpo )
          where TDetailDpo : BaseDpo, new()
        {
            var entity = Mapper.Map<TEntity>(dpo);

            entity = repo.Create(entity);

            dpo = Mapper.Map<TDetailDpo>(entity);

            return dpo;
        }

        public virtual IEnumerable<TDpo> Get<TDpo>(BaseFilter filter)
            where TDpo : BaseDpo, new()
        {
            var entities = this.repo.Read(new SelectCriterion[] { SelectCriterion.WithoutDeleted});

            //Фильтруем данные по критериям указанным в форме
           var _entities = this._doFiltration(filter, entities).ToList();

           return _entities.Select(e=> Mapper.Map<TDpo>(e)); // results;
        }

        protected abstract IQueryable<TEntity> _doFiltration(BaseFilter filter, IQueryable<TEntity> query);
       

        public virtual TDetailDpo Get<TDetailDpo>(Guid id)
            where TDetailDpo : BaseDpo, new()
        {
            return Mapper.Map<TDetailDpo>(repo.Read(id)) ;
        }

        public virtual CrudResult<bool> Remove(Guid id)
        {
            var result = new CrudResult<bool>();

            if (repo.Delete(id))
            {
                result.Data = true;
                result.Result = CrudResults.Ok;
            }
            else
            {
               result.Data = false;
               result.Result = CrudResults.UnAccessibleDb;
            }

           return result;
        }

        public virtual TDetailDpo Update <TDetailDpo>(TDetailDpo dpo)
            where TDetailDpo : BaseDpo
        {
            var entity = repo.Read(dpo.Id);

            entity = Mapper.Map<TDetailDpo, TEntity>(dpo, entity);

            entity = repo.Update(entity);

            return Mapper.Map<TDetailDpo>(entity);
        }
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PriorityGoals.Service
{
    using AutoMapper;

    using Microsoft.Practices.Unity;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dao;
    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.DbRepositories;
    using PriorityGoals.Service.Service;

    using Remotion.Data.Linq;

    public class IssueCrudService : BaseCrudService<Issue, IssueRepository>, IIssueCrudService
    {
        [Dependency]
        public IBaseRepository<Plan>  PlanRepository { get; set; }
        [Dependency]
        public IIdeaCrudService IdeaService { get; set; }

        protected override IQueryable<Issue> _doFiltration(BaseFilter filter, IQueryable<Issue> query)
        {
            if (filter == null)
            {
                return query;
            }
            var _filter = filter as IssueFilter;

            if (_filter == null)
            {
                throw new Exception("Кто-то засунул в метод фильтации задач фильтр не по задачам.");
            }

            if (!_filter.IsLoadSubissues)
            {
                query = query.Where(i => i.ParentId == null);
            }
            

            if (_filter.PlanId != null && _filter.PlanId != Guid.Empty)
            {
                query = query.Where(i=> i.PlanId == _filter.PlanId);
            }
            if (_filter.State !=null)
            {
                query = query.Where(i => i.State == _filter.State);
            }
            if (_filter.Priorities !=null && _filter.Priorities.Any())
            {
                query = query.Where(p => _filter.Priorities.Contains((int)p.Priority));
            }
            
            if (_filter.DataBegin != null)
            {
                query = query.Where(b => b.StartDate >= ((DateTime)_filter.DataBegin));
            }
            if (_filter.DataEnd != null)
            {
                query = query.Where(e => ((DateTime)e.EndDate) <= ((DateTime)_filter.DataEnd));
            }

            return query;
        }

       

        public override CrudResult<bool> Remove(Guid id)
        {
            //Выполнить условия проверки на возможность удаления задачи
            var result  = new CrudResult<bool>() {Data = true};
            var entity =  repo.Read(id);

            if (entity.State == States.Finished )
            {
                result.Data = false;
                result.Result = CrudResults.CannotDelete;
                result.Message = "Не возможно удалить завершенные задачи.";
                return result;
            }
            
            //Удаляет вложенные подзадачи если таковые имеются
            if (entity.SubIssues != null)
            {
                //TODO разобраться и доделать
                foreach (var t in entity.SubIssues)
                {
                    result.Data &= this.Remove(t.Id).Data;
                }
            }

            if (entity.Ideas != null)
            {
                foreach (var i in entity.Ideas)
                {
                    result.Data &= this.IdeaService.Remove(i.Id).Data;
                }
            }

            if (result.Data)
            {
                return base.Remove(id); 
            }

            return result;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Service.Service
{
    using Api.Service;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.DbRepositories;

    class DreamService : BaseCrudService<Dream, DreamRepository>, IDreamCrudService
    {

        protected override IQueryable<Dream> _doFiltration(BaseFilter filter, IQueryable<Dream> query)
        {
            if (filter == null)
            {
                return query;
            }

            var _filter = filter as DreamFilter;

            if (_filter == null)
            {
                throw new Exception("Кто-то засунул в метод фильтации мечт фильтр не по мечтам.");
            }

            if (_filter.InterestId!= null && _filter.InterestId != Guid.Empty)
            {
                query = query.Where(i => i.InterestId== _filter.InterestId);
            }
            
            if (_filter.Priorities != null && _filter.Priorities.Any())
            {
                query = query.Where(p => _filter.Priorities.Contains((int)p.Priority));
            }
            return query;
        }
    }
}

﻿using System.Linq;

namespace PriorityGoals.Service.Service
{
    using System;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dpo.Principle;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.DbRepositories;

    public class PrincipleService : BaseCrudService<Principle, PrincipleRepository>, IPrincipleCrudService
    {
        protected override IQueryable<Principle> _doFiltration(BaseFilter filter, IQueryable<Principle> query)
        {
            return query;
        }

        public override CrudResult<bool> Remove(Guid id)
        {
            var entity = this.repo.Read(id);
            if (entity.Interests != null && entity.Interests.Any())
            {
                return new CrudResult<bool>{ Data = false,
                    Result = CrudResults.CannotDelete,
                    Message = "Удаление не возможно. Принцып соджержит вложенные интересы."
                };
            }

            return base.Remove(id);
        }
    }
}

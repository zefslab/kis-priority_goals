﻿using System.Linq;

namespace PriorityGoals.Service
{
    using System;

    using AutoMapper;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dao;
    using PriorityGoals.Api.Dpo;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.DbRepositories;

    public class GoalService : BaseCrudService<Goal, GoalRepository>, IGoalCrudService
    {
        public override GoalDpo Update<GoalDpo>(GoalDpo dpo)
        {
            ///Наш код
            /// 
            var result = base.Update(dpo);

            ///Наш код

            return result;
        }

        protected override IQueryable<Goal> _doFiltration(BaseFilter filter, IQueryable<Goal> query)
        {
            if (filter == null)
            {
                return query;
            }

            var goalFilter = filter as GoalFilter;

            if (goalFilter == null)
            {
                throw new Exception("Фильтр не соответствует типу GoalFilter");
            }
           


            if (goalFilter.DreamId != null)
            {
                query = query.Where(x => x.Dream.Id == goalFilter.DreamId);
            }

            
            if (goalFilter.DateBegin != null)
            {
                query = query.Where(g => g.GoalDate > goalFilter.DateBegin);
            }

            if (goalFilter.DateEnd != null)
            {
                query = query.Where(g => g.GoalDate <= goalFilter.DateEnd);
            }
            return query;
        }
        public override CrudResult<bool> Remove(Guid id)
        {
            var entity = this.repo.Read(id);
            if (entity.Plans !=null && entity.Plans.Any())
            {
                return new CrudResult<bool>
                {
                    Data = false,
                    Result = CrudResults.CannotDelete,
                    Message = "Удаление не возможно.Цель соджержит вложенные планы."
                };
            }
            return base.Remove(id);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Service.Service
{
    using System.Runtime.InteropServices.ComTypes;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dao;
    using PriorityGoals.Api.Dpo.Interests;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.DbRepositories;

    class InterestService : BaseCrudService<Interest, InterestRepository>, IInterestCrudService
    {
        protected override IQueryable<Interest> _doFiltration(BaseFilter filter, IQueryable<Interest> query)
        {
            return query;
        }
        public override CrudResult<bool> Remove(Guid id)
        {
            var entity = this.repo.Read(id);
            if (entity.Dreams != null && entity.Dreams.Any())
            {
                return new CrudResult<bool>
                {
                    Data = false,
                    Result = CrudResults.CannotDelete,
                    Message = "Удаление не возможно. Интерес соджержит вложенные мечты."
                };
            }
            return base.Remove(id);
        }
    
    }
}

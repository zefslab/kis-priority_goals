﻿using System.Collections.Generic;
using System.Linq;

namespace PriorityGoals.Service
{
    using System;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.DbRepositories;

    public class PlanService : BaseCrudService<Plan, PlanRepository>, IPlanCrudService
    {
        protected override IQueryable<Plan> _doFiltration(BaseFilter filter, IQueryable<Plan> query)
        {
            if (filter == null)
            {
                return query;
            }

            var planFilter = filter as PlanFilter;

            if (planFilter == null)
            {
                throw new Exception("Фильтр не соответствует типу PlanFilter");
            }

            if (planFilter.GoalId != null)
            {
                query = query.Where(x => x.GoalId == planFilter.GoalId);
            }

            if (planFilter.DataBegin != null)
            {
                query = query.Where(p => p.BeginDate >= planFilter.DataBegin);
            }
            if (planFilter.DataEnd != null)
            {
                query = query.Where(p => p.EndDate <= planFilter.DataEnd);
            }

            if (planFilter.State != null)
            {
                query = query.Where(p => p.State == planFilter.State);
            }
            if (planFilter.Priorities != null && planFilter.Priorities.Any())
            {
                query = query.Where(x => planFilter.Priorities.Contains((int)x.Priority));
            }

            return query;
        }

        public override CrudResult<bool> Remove(Guid id)
        {
            var entity = this.repo.Read(id);
            if (entity.Issues != null && entity.Issues.Any())
            {
                return new CrudResult<bool>
                {
                    Data = false,
                    Result = CrudResults.CannotDelete,
                    Message = "Удаление не возможно. План соджержит вложенные задачи."
                };
            }
            return base.Remove(id);
        }
    }

}


    


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Service.Service
{
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.DbRepositories;

    class FinanceService : BaseCrudService<Finance, FinanceRepository>
    {
        protected override IQueryable<Finance> _doFiltration(BaseFilter filter, IQueryable<Finance> query)
        {
            return query;
        }
    }
}

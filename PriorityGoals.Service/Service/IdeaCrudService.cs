﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Service.Service
{
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.DbRepositories;

    public class IdeaCrudService : BaseCrudService<Idea, IdeaRepository>, IIdeaCrudService
    {
        protected override IQueryable<Idea> _doFiltration(BaseFilter filter, IQueryable<Idea> query)
        {
            return query;
        }
    }
}

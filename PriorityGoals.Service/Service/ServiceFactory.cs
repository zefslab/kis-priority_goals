﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Service.Service
{
    using Microsoft.Practices.Unity;

    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Service;
    using PriorityGoals.Dao.DbRepositories;

    /// <summary>
    /// Паттерн одиночка и фабрика два в одном
    /// </summary>
    public class ServiceFactory : IServiceFactory
    {

        readonly Dictionary<Type, Type> _serviceRegistr = new Dictionary<Type, Type>();

        private static  ServiceFactory _serviceFactory = null;

        public static ServiceFactory InitializeServiceFactory()
        {
            if (_serviceFactory == null)
            {
                _serviceFactory = new ServiceFactory();
            }

            return _serviceFactory;
        }

        private ServiceFactory()
        {
            var servicesTypes = this.GetType().Assembly.GetTypes()
                .Where(t => typeof (ICrudService).IsAssignableFrom(t));

            foreach (var serviceType in servicesTypes)
            {
                //взять у типа сервиса базовый тип и получить у него тип первого параметра
                //который будет ключом
                var baseGenericType = serviceType.BaseType;
                if (baseGenericType != null
                    && baseGenericType.IsGenericType
                    && baseGenericType.GetGenericTypeDefinition() == typeof (BaseCrudService<,>))
                {
                    //первый параметр указывает на 
                    var entityType = baseGenericType.GenericTypeArguments.First();
                    
                    //инстанцирование сервиса и запись его в библиотеку
                    _serviceRegistr[entityType] = serviceType;
                }
            }
        }
        /// <summary>
        /// Извлечение из реестра сервисов инстанции сервиса подходящего для указанного типа сущности
        /// </summary>
        /// <typeparam name="TEntity">тип сущности</typeparam>
        /// <returns></returns>
        public ICrudService GetService<TEntity>() where TEntity : BaseEntity
        {
            var service = Activator.CreateInstance(this._serviceRegistr[typeof(TEntity)]) as ICrudService;

            return service;
        }
    }
}

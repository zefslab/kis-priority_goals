﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriorityGoals.Service.Service
{
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Filters;
    using PriorityGoals.Dao.Dao.Repositories;
    using PriorityGoals.Dao.DbRepositories;

    class paymentService : BaseCrudService<Payment, PaymentRepository>
    {
        protected override IQueryable<Payment> _doFiltration(BaseFilter filter, IQueryable<Payment> query)
        {
            return query;
        }
    }
}

﻿namespace PriorityGoals.Dao.Dao.Repositories
{
    using System;
    using System.Linq;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dao;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Dao.Dao.EfRepositories;

    public class PlanRepository : BaseRepository<Plan>
    {
        IBaseRepository<Idea> repositoryIdea = new IdeaRepository();
        IBaseRepository<Issue> repositoryIssue = new IssueRepository(); 
        public override Plan Read(Guid id)
        {
            var plan = base.Read(id);

            plan.Issues =
                this.repositoryIssue.Read(new SelectCriterion[] { SelectCriterion.WithoutDeleted })
                    .Where(x => x.PlanId == plan.Id)
                    .ToList();
            plan.Ideas =
               repositoryIdea.Read(new SelectCriterion[] { SelectCriterion.WithoutDeleted })
                   .Where(x => x.PlanId == plan.Id)
                   .ToList();

            return plan;
        }

    }
}

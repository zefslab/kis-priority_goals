﻿using System;
using System.Linq;
using NLog;

namespace PriorityGoals.Dao.Dao.EfRepositories
{
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Dao;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Api.Utility;

    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
         where TEntity : BaseEntity, new()
    {
        protected DbConnection Db = DbConnectionSingleTone.GetContext();

        protected ILog _logger;

        protected BaseRepository()
        {
            _logger = new Log("repositories");
        }

        public virtual TEntity Create(TEntity entity, bool needSaveToDb = true)
        {
            entity.Id = GuidGenerator.Get();
            entity.CreatedDate = DateTime.Now;
            entity.ModifiedDate = entity.CreatedDate;

            
                Db.Set<TEntity>().Add(entity);

                if (needSaveToDb)
                {
                    SubmitChanges();
                }

            return entity;
        }

        public virtual TEntity Read(Guid id)
        {
            return this.Db.Set<TEntity>().SingleOrDefault(x=>x.Id == id);
        }

        public virtual IQueryable<TEntity> Read(params SelectCriterion[] criterions)
        {
            var query = this.Db.Set<TEntity>();

            if (criterions.Contains(SelectCriterion.WithoutDeleted))
            {
                return query.Where(x => x.DeletedDate == null);
            }
            if (criterions.Contains(SelectCriterion.OnlyDeleted))
            {
                return query.Where(x => x.DeletedDate != null);
            }
            if (criterions.Contains(SelectCriterion.LastWeek))
            {
                return query.Where(x => x.CreatedDate > DateTime.Now - TimeSpan.FromDays(7));
            }

            return query;
        }

        public virtual TEntity Update(TEntity entity)
        {
            entity.ModifiedDate = DateTime.Now;

            this.Db.Set<TEntity>().AddOrUpdate(entity);

            this.SubmitChanges();

            return entity;
        }

        public virtual bool Delete(Guid id, bool isPermanently = false)
        {
            var entity = Read(id);

            if (entity != null)
            {
                if (isPermanently)
                {
                    Db.Set<TEntity>().Remove(entity);
                    this.SubmitChanges();
                }
                else
                {
                    entity.DeletedDate = DateTime.Now;
                    this.Update(entity);
                }
              

                return true;
            }
            

            return false;
        }

        public virtual bool Restore(Guid id)
        {
            throw new NotImplementedException();
        }

        public virtual void SubmitChanges()
        {
            try
            {
                Db.SaveChanges();
            }
            catch (Exception ex)
            {
                _logger.Error( "Проблема с сохранением в БД", ex);
                throw;
            }
            
        }
    }
}

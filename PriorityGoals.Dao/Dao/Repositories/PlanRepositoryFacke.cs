﻿namespace PriorityGoals.Dao.Dao.Repositories
{
    using System;
    using System.Linq;

    using PriorityGoals.Api.Constants;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Dao.Dao.EfRepositories;

    public class PlanRepositoryFacke : BaseRepository<Plan>
    {
        public override IQueryable<Plan> Read(params SelectCriterion[] criterions)
        {
            return new Plan[]
                        {
                            new Plan
                                {
                                    Description = "Белеберда1", Id = Guid.NewGuid(),
                                    Name = "Барбарос"
                                },
                            new Plan {Description = "Белеберда2", Id = Guid.NewGuid(),
                                    Name = "БарбаросКапут"},
                            new Plan {Description = "Белеберда3", Id = Guid.NewGuid(),
                                    Name = "БарбаросОбделался"},
                        }.AsQueryable();
        }
    }
}

﻿using System.Linq;
using PriorityGoals.Api.Constants;

namespace PriorityGoals.Dao.Dao.Repositories
{
    using System;
    using System.Runtime.InteropServices.ComTypes;

    using PriorityGoals.Api.Dao;
    using PriorityGoals.Api.Entity;
    using PriorityGoals.Dao.Dao.EfRepositories;

    public class IssueRepository : BaseRepository<Issue>
    {
        IBaseRepository<Idea> repositoryIdea = new IdeaRepository();
        public override Issue Read(Guid id)
        {
            var issue = base.Read(id);

            issue.SubIssues =
                this.Read(new SelectCriterion[] { SelectCriterion.WithoutDeleted })
                    .Where(x => x.ParentId == issue.Id)
                    .ToList();
            issue.Ideas =
               repositoryIdea.Read(new SelectCriterion[] { SelectCriterion.WithoutDeleted })
                   .Where(x => x.IssueId == issue.Id)
                   .ToList();

            return issue;
        }
    }
}

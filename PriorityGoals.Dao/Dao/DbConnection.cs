﻿using System.Data.Entity;
using PriorityGoals.Api.Entity;
using PriorityGoals.Dao.Dao.Maps;

namespace PriorityGoals.Dao.Dao
{
   
    public class DbConnection : DbContext
    {
        
        public DbSet<Issue> Issues { get; set; }
        public DbSet<Dream> Dreams { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Idea> Ideas { get; set; }
        public DbSet<Plan> Plans { get; set; }
        public DbSet<Interest> Interests { get; set; }
        public DbSet<Principle> Principles { get; set; }
        public DbSet<Finance> Finances { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Configurations.Add(new PlanMap());
            modelBuilder.Configurations.Add(new IssueMap());
            modelBuilder.Configurations.Add(new IdeaMap());
            modelBuilder.Configurations.Add(new GoalMap());
            modelBuilder.Configurations.Add(new DreamMap());
            modelBuilder.Configurations.Add(new FinanceMap());
            modelBuilder.Configurations.Add(new InterestMap());
            modelBuilder.Configurations.Add(new PaymentMap());
            modelBuilder.Configurations.Add(new PrincipleMap());
            modelBuilder.Configurations.Add(new ResultMap());

        }
    }

    class DbConnectionSingleTone
    {
        static DbConnection _connection;

        public static DbConnection GetContext()
        {
            if (_connection == null)
            {
                _connection = new DbConnection();
            }
            return _connection;
        }

    }
}

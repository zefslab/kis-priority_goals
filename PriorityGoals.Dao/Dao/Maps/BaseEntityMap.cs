﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using System.Data.Entity.ModelConfiguration;

    using PriorityGoals.Api.Entity;

    abstract class BaseEntityMap<T> : EntityTypeConfiguration<T>
        where T : BaseEntity
    {
        public BaseEntityMap()
        {
            HasKey(x => x.Id);
            Property(x => x.Id).HasColumnName("id");
            Property(x => x.CreatedDate).HasColumnName("created_date");
            Property(x => x.DeletedDate).HasColumnName("deleted_date");
            Property(x => x.Description).HasColumnName("description");
            Property(x => x.ModifiedDate).HasColumnName("modified_date");
        }
    }
}

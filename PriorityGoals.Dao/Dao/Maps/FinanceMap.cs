﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using PriorityGoals.Api.Entity;

    class FinanceMap : BaseEntityMap<Finance>
    {
        public FinanceMap()
        {
            Property(x => x.Name).HasColumnName("name").IsRequired();

            ToTable("finance", "model");
        }
    }
}

﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using PriorityGoals.Api.Entity;

    class PaymentMap : BaseEntityMap<Payment>
    {
        /// <summary>
        /// Оплата. Для контроля расходов на достижение каких-то планов.
        /// </summary>
        public PaymentMap()
        {
            HasRequired(x => x.Issue).WithMany(x => x.Payments).Map(x => x.MapKey("issue_id"));

            HasRequired(x => x.Finance).WithMany().Map(x => x.MapKey("finance_id"));

            ToTable("payments", "model");

        }
    }
}

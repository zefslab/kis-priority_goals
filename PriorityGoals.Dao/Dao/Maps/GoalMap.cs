﻿namespace PriorityGoals.Dao.Dao.Maps
{

    using PriorityGoals.Api.Entity;

    class GoalMap : BaseEntityMap<Goal>
    {
        public GoalMap()
        {
            Property(x => x.Name).HasColumnName("name");
            Property(x => x.GoalDate).HasColumnName("goal_date");
            Property(x => x.Priority).HasColumnName("priority");
            Property(x => x.DreamId).HasColumnName("dream_id");
            HasOptional(x => x.Dream).WithMany(x => x.Goals).HasForeignKey(x=>x.DreamId);

            ToTable("goals", "model");
 
        }
    }
}

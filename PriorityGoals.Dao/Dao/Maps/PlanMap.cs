﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using PriorityGoals.Api.Entity;

    class PlanMap : BaseEntityMap<Plan>
    {
        public PlanMap()
        {
           
            Property(x => x.Name).HasColumnName("name");

            Property(x => x.BeginDate).HasColumnName("begin_date");

            Property(x => x.EndDate).HasColumnName("end_date");

            Property(x => x.State).HasColumnName("state");
            Property(x => x.Priority).HasColumnName("priority");

            HasRequired(x => x.Goal).WithMany(x=>x.Plans).HasForeignKey(x=>x.GoalId);
            Property(x => x.GoalId).HasColumnName("goal_id").IsRequired();

            Ignore(x => x.Ideas);

            ToTable("plans", "model");
        }

    }
}

﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using PriorityGoals.Api.Entity;

    class DreamMap : BaseEntityMap<Dream>
    {
        public DreamMap()
        {
            Property(x => x.DreamDate).HasColumnName("dream_date").IsRequired();
            Property(x => x.Name).HasColumnName("name").IsOptional();
            Property(x => x.Priority).HasColumnName("priority");

            Property(x => x.InterestId).HasColumnName("interest_id").IsOptional();
            HasOptional(x => x.Interest).WithMany(x => x.Dreams).HasForeignKey(x => x.InterestId);

            HasMany(x => x.Goals).WithOptional(x => x.Dream);

            ToTable("dream", "model");
            
        }
    }
}

﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using PriorityGoals.Api.Entity;

    class InterestMap : BaseEntityMap<Interest>
    {
        public InterestMap()
        {
            Property(x => x.Name).HasColumnName("name");
            Property(x => x.InterestDate).HasColumnName("interest_date");
            Property(x => x.Priority).HasColumnName("priority");
            Property(x => x.PrincipleId).HasColumnName("principle_id").IsOptional();
            HasOptional(x => x.Principle).WithMany(x => x.Interests).HasForeignKey(x=>x.PrincipleId);
            HasMany(x => x.Dreams).WithOptional(x => x.Interest);

            ToTable("interests", "model");
        }
    }
}

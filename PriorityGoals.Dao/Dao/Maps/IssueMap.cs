﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using PriorityGoals.Api.Entity;

    class IssueMap : BaseEntityMap<Issue>
    {
        public IssueMap()
        {
            Property(x => x.Name).HasColumnName("name");
            Property(x => x.EndDate).HasColumnName("end_date");
            Property(x => x.StartDate).HasColumnName("start_date");
            Property(x => x.State).HasColumnName("state");
            Property(x => x.Priority).HasColumnName("priority");

            Property(x => x.PlanId).HasColumnName("plan_id");
            HasOptional(x => x.Plan).WithMany(x=>x.Issues).HasForeignKey(x=> x.PlanId);

            Ignore(x => x.Ideas);

            Property(x => x.ParentId).HasColumnName("parant_id");
            HasOptional(x => x.Parent).WithMany(x => x.SubIssues).HasForeignKey(x=> x.ParentId);

            Ignore(x => x.SubIssues);
          

            HasMany(x => x.Payments).WithRequired(x => x.Issue);

            


            ToTable("issues", "model");

        }
    }
}

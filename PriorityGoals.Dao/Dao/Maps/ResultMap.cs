﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using PriorityGoals.Api.Entity;

    class ResultMap : BaseEntityMap<Result>
    {
        public ResultMap()
        {
            
            HasRequired(x => x.Plan).WithMany().Map(x=>x.MapKey("plan_id"));

            ToTable("results", "model");

        }
    }
}

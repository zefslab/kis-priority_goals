﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using PriorityGoals.Api.Entity;

    class PrincipleMap : BaseEntityMap<Principle>
    {
        public PrincipleMap()
        {
            Property(x => x.Name).HasColumnName("name");
            HasMany(x => x.Interests).WithOptional(x => x.Principle);
            Property(x => x.Priority).HasColumnName("priority");

            ToTable("principles", "model");
        }
    }
}

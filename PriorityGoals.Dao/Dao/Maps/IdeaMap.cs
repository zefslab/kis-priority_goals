﻿namespace PriorityGoals.Dao.Dao.Maps
{
    using PriorityGoals.Api.Entity;

    class IdeaMap : BaseEntityMap<Idea>
    {

        public IdeaMap()
        {
            Property(x => x.Name).HasColumnName("name");
            this.HasOptional(x => x.Issue).WithMany().HasForeignKey(x => x.IssueId);
            this.Property(x => x.IssueId).HasColumnName("issue_id").IsOptional();

            this.HasOptional(x => x.Plan).WithMany().HasForeignKey(x => x.PlanId);
            this.Property(x => x.PlanId).HasColumnName("plan_id").IsOptional();
            ToTable("ideas", "model");
        }
    }
}

namespace PriorityGoals.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePlanEndDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("model.plans", "end_date", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("model.plans", "end_date", c => c.DateTime(nullable: false));
        }
    }
}

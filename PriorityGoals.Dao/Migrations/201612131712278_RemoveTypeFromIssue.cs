namespace PriorityGoals.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveTypeFromIssue : DbMigration
    {
        public override void Up()
        {
            RenameColumn("model.issues", "finished_date", "end_date");
            DropColumn("model.issues", "type");
            
        }
        
        public override void Down()
        {
            AddColumn("model.issues", "type", c => c.String());
            RenameColumn("model.issues", "end_date", "finished_date");
        }
    }
}

namespace PriorityGoals.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "model.dream",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        name = c.String(),
                        dream_date = c.DateTime(nullable: false),
                        Priority = c.Int(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                        interest_id = c.Guid(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("model.interests", t => t.interest_id)
                .Index(t => t.interest_id);
            
            CreateTable(
                "model.goals",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        name = c.String(),
                        goal_date = c.DateTime(nullable: false),
                        Priority = c.Int(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                        dream_id = c.Guid(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("model.dream", t => t.dream_id)
                .Index(t => t.dream_id);
            
            CreateTable(
                "model.plans",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        name = c.String(),
                        Priority = c.Int(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                        goal_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("model.goals", t => t.goal_id, cascadeDelete: true)
                .Index(t => t.goal_id);
            
            CreateTable(
                "model.ideas",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        name = c.String(),
                        idea_date = c.DateTime(nullable: false),
                        Priority = c.Int(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                        plan_id = c.Guid(),
                        issue_id = c.Guid(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("model.plans", t => t.plan_id)
                .ForeignKey("model.issues", t => t.issue_id)
                .Index(t => t.plan_id)
                .Index(t => t.issue_id);
            
            CreateTable(
                "model.issues",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        name = c.String(),
                        type = c.String(),
                        finished_date = c.DateTime(),
                        start_date = c.DateTime(nullable: false),
                        state = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                        plan_id = c.Guid(),
                        parant_id = c.Guid(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("model.plans", t => t.plan_id)
                .ForeignKey("model.issues", t => t.parant_id)
                .Index(t => t.plan_id)
                .Index(t => t.parant_id);
            
            CreateTable(
                "model.payments",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        Priority = c.Int(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                        finance_id = c.Guid(nullable: false),
                        issue_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("model.finance", t => t.finance_id, cascadeDelete: true)
                .ForeignKey("model.issues", t => t.issue_id, cascadeDelete: true)
                .Index(t => t.finance_id)
                .Index(t => t.issue_id);
            
            CreateTable(
                "model.finance",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        name = c.String(nullable: false),
                        Priority = c.Int(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "model.interests",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        name = c.String(),
                        interest_date = c.DateTime(nullable: false),
                        principle_id = c.Guid(),
                        priority = c.Int(),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("model.principles", t => t.principle_id)
                .Index(t => t.principle_id);
            
            CreateTable(
                "model.principles",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        name = c.String(),
                        principle_date = c.DateTime(nullable: false),
                        Priority = c.Int(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "model.results",
                c => new
                    {
                        id = c.Guid(nullable: false),
                        created_date = c.DateTime(nullable: false),
                        modified_date = c.DateTime(nullable: false),
                        deleted_date = c.DateTime(),
                        description = c.String(),
                        plan_id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("model.plans", t => t.plan_id, cascadeDelete: true)
                .Index(t => t.plan_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("model.results", "plan_id", "model.plans");
            DropForeignKey("model.dream", "interest_id", "model.interests");
            DropForeignKey("model.interests", "principle_id", "model.principles");
            DropForeignKey("model.goals", "dream_id", "model.dream");
            DropForeignKey("model.issues", "parant_id", "model.issues");
            DropForeignKey("model.issues", "plan_id", "model.plans");
            DropForeignKey("model.payments", "issue_id", "model.issues");
            DropForeignKey("model.payments", "finance_id", "model.finance");
            DropForeignKey("model.ideas", "issue_id", "model.issues");
            DropForeignKey("model.ideas", "plan_id", "model.plans");
            DropForeignKey("model.plans", "goal_id", "model.goals");
            DropIndex("model.results", new[] { "plan_id" });
            DropIndex("model.interests", new[] { "principle_id" });
            DropIndex("model.payments", new[] { "issue_id" });
            DropIndex("model.payments", new[] { "finance_id" });
            DropIndex("model.issues", new[] { "parant_id" });
            DropIndex("model.issues", new[] { "plan_id" });
            DropIndex("model.ideas", new[] { "issue_id" });
            DropIndex("model.ideas", new[] { "plan_id" });
            DropIndex("model.plans", new[] { "goal_id" });
            DropIndex("model.goals", new[] { "dream_id" });
            DropIndex("model.dream", new[] { "interest_id" });
            DropTable("model.results");
            DropTable("model.principles");
            DropTable("model.interests");
            DropTable("model.finance");
            DropTable("model.payments");
            DropTable("model.issues");
            DropTable("model.ideas");
            DropTable("model.plans");
            DropTable("model.goals");
            DropTable("model.dream");
        }
    }
}

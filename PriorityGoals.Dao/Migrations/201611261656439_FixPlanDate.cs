namespace PriorityGoals.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixPlanDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("model.plans", "begin_date", c => c.DateTime(nullable: false));
            AddColumn("model.plans", "end_date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("model.plans", "end_date");
            DropColumn("model.plans", "begin_date");
        }
    }
}

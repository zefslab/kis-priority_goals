namespace PriorityGoals.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStatePropertyInPlan : DbMigration
    {
        public override void Up()
        {
            AddColumn("model.plans", "state", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("model.plans", "state");
        }
    }
}

namespace PriorityGoals.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeNameInEFMappings : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "model.dream", name: "Priority", newName: "prioriti");
            RenameColumn(table: "model.goals", name: "Priority", newName: "prioriti");
            RenameColumn(table: "model.plans", name: "Priority", newName: "prioriti");
            RenameColumn(table: "model.ideas", name: "Priority", newName: "prioriti");
            RenameColumn(table: "model.interests", name: "priority", newName: "prioriti");
            RenameColumn(table: "model.principles", name: "Priority", newName: "prioriti");
        }
        
        public override void Down()
        {
            RenameColumn(table: "model.principles", name: "prioriti", newName: "Priority");
            RenameColumn(table: "model.interests", name: "prioriti", newName: "priority");
            RenameColumn(table: "model.ideas", name: "prioriti", newName: "Priority");
            RenameColumn(table: "model.plans", name: "prioriti", newName: "Priority");
            RenameColumn(table: "model.goals", name: "prioriti", newName: "Priority");
            RenameColumn(table: "model.dream", name: "prioriti", newName: "Priority");
        }
    }
}

namespace PriorityGoals.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactorIdea : DbMigration
    {
        public override void Up()
        {
            DropColumn("model.ideas", "idea_date");
            DropColumn("model.ideas", "priority");
        }
        
        public override void Down()
        {
            AddColumn("model.ideas", "priority", c => c.Int(nullable: false));
            AddColumn("model.ideas", "idea_date", c => c.DateTime(nullable: false));
        }
    }
}

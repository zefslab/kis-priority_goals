namespace PriorityGoals.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeleteUnusedField : DbMigration
    {
        public override void Up()
        {
            DropColumn("model.principles", "principle_date");
        }
        
        public override void Down()
        {
            AddColumn("model.principles", "principle_date", c => c.DateTime(nullable: false));
        }
    }
}

namespace PriorityGoals.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixPriorityName : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "model.dream", name: "prioriti", newName: "priority");
            RenameColumn(table: "model.goals", name: "prioriti", newName: "priority");
            RenameColumn(table: "model.plans", name: "prioriti", newName: "priority");
            RenameColumn(table: "model.ideas", name: "prioriti", newName: "priority");
            RenameColumn(table: "model.interests", name: "prioriti", newName: "priority");
            RenameColumn(table: "model.principles", name: "prioriti", newName: "priority");
        }
        
        public override void Down()
        {
            RenameColumn(table: "model.principles", name: "priority", newName: "prioriti");
            RenameColumn(table: "model.interests", name: "priority", newName: "prioriti");
            RenameColumn(table: "model.ideas", name: "priority", newName: "prioriti");
            RenameColumn(table: "model.plans", name: "priority", newName: "prioriti");
            RenameColumn(table: "model.goals", name: "priority", newName: "prioriti");
            RenameColumn(table: "model.dream", name: "priority", newName: "prioriti");
        }
    }
}
